//funções para manipulação de arquivo
#include "files.hpp"
#include "alocacao_desalocar.hpp"

//ler um jogo de um arquivo texto, por exemplo: entrada.txt
void ler_arquivo(char **matriz, FILE *arquivo, int *m, int *n, Mat *matrizaux)
{   
    //m e n saõ as dimensoes da matriz
    //arquivo é onde está o arquivo para ser lido
    //printf("jkjkj");
    
    for(int i =  0; i < 26; i++)
    {
        for(int j = 0; j < 26; j++)
        {
            matrizaux->vet_lin[i][j] = 0;
            matrizaux->vet_col[i][j] = 0;
        }   
    }

    matrizaux->maior_lin = 0;
    matrizaux->maior_col = 0;

    getc(arquivo);//pega o \n
    int qunt_nro;

    //printf("hey\n");
    for(int i = 0; i < *m; i++)
    {
        fscanf(arquivo,"%d", &qunt_nro);

        matrizaux->vet_maior_lin[i] = qunt_nro;

        for(int j = 0; j < qunt_nro; j++)
        {
            if(qunt_nro > (matrizaux->maior_lin))
            {
                matrizaux->maior_lin = qunt_nro;
            }
            
            fscanf(arquivo,"%d", &matrizaux->vet_lin[i][j]);
        }
    }

    getc(arquivo);// pega o '\n'
    for(int i =0; i < *n;i++)
    {
        fscanf(arquivo,"%d",&qunt_nro);

        matrizaux->valor_coluna[i] = qunt_nro;
        matrizaux->vet_maior_col[i] = qunt_nro;

        for(int j = 0; j < qunt_nro; j++)
        {
            if(qunt_nro > (matrizaux->maior_col))
            {
                matrizaux->maior_col = qunt_nro;
            }

            fscanf(arquivo,"%d", &matrizaux->vet_col[i][j]);
        }
    }

    //printf("%d \n", matrizaux->maior_lin);
    //printf("%d \n", matrizaux->maior_col);

    getc(arquivo);
    char character;
    fflush(stdout);
    //aqui ok
    for(int i = 0; i < *m; i++)
    {
        for(int j = 0; j < *n ; j++)
        {
            fgetc(arquivo);
            character = fgetc(arquivo);
            if(character == 'x')
            {
                character = 'X';
                matriz[i][j] = character;
            }
            else if(character == '.' || character == '-')
            {
                matriz[i][j] = character;
            }
            else
            {
                exit(1);
            }
            
        }
        //printf("\n");
    }

    fclose(arquivo);
}

//salva o jogo no arquivo de saida, por exemplo: out.txt
void write_file(char **matriz, FILE *arquivo_out, int m, int n, Mat *matrizaux)
{
    //m e n são as dimenções da matriz

    //grava no arquivo de saida as dimensoes da matriz
    fprintf(arquivo_out,"%d %d",m, n);
    fputs("\n\n",arquivo_out);

    //grava no arquivo de saida os cabeçalhos das linhas e das colunas

    for(int i = 0; i < m; i++)
    {
        fprintf(arquivo_out,"%d",matrizaux->vet_maior_lin[i]);
        
        for(int j = 0; j < matrizaux->vet_maior_lin[i]; j++)
        {
            fputs(" ", arquivo_out);
            fprintf(arquivo_out, "%d", matrizaux->vet_lin[i][j]);
        }

        fputs("\n", arquivo_out);
    }

    fputs("\n", arquivo_out);

    for(int i = 0; i < n; i++)
    {
        fprintf(arquivo_out, "%d", matrizaux->vet_maior_col[i]);

        for(int j = 0; j < matrizaux->vet_maior_col[i]; j++)
        {
            fputs(" ", arquivo_out);
            fprintf(arquivo_out,"%d", matrizaux->vet_col[i][j]);
        }

        fputs("\n", arquivo_out);
    }

    //fputs("\n", arquivo_out);

    //grava os caracteres (x, - ou '.')

    for(int i = 0; i < m; i++)
    {
        fputs("\n", arquivo_out);

        for(int j = 0; j < n; j++)
        {
            if(j != 0)
                fputs(" ", arquivo_out);//nesta parte serve para nao imprimir um espaço antes do peimeiro caracter

            fprintf(arquivo_out,"%c", matriz[i][j]);/*aqui nao tem espaco, pois na ultima posição depois do caracter estaria um espaço*, que na fora da impressão do jogo salvo, ficaria tudo desconfigurado*/
        }
        
    }

    fclose(arquivo_out);
    //arquivo é onde está o arquivo onde vai ser salvo
}