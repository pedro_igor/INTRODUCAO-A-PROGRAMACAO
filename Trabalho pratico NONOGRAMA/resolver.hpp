#ifndef RESOLVER_H
#define RESOLVER_H

#include <stdio.h>
#include <stdlib.h>
#include "files.hpp"
#include "jogada.hpp"
#include "verificacao.hpp"

void resolver(char **matriz, int m, int n, Mat *matriz_aux);

#endif