#ifndef ALOCACAO_DESALOCAR_HPP
#define ALOCACAO_DESALOCAR_HPP

#include <stdio.h>
#include <stdlib.h>

char** alocar_matriz(int linha, int coluna);
char **liberar_matriz(int m, int n, char **v);

#endif