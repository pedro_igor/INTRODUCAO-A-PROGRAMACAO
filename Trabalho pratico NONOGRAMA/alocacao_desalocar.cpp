//alocação e liberação de memoria
#include "alocacao_desalocar.hpp" //chamada da função

char** alocar_matriz(int linha, int coluna)
{

    if (linha < 1 || coluna < 1 || coluna > 26 || linha > 26) // verifica parametros recebidos
    {
        printf("ERRO: Parametro invalido\n");
        return NULL;
    }

    char **matriz = (char**) malloc(linha * sizeof(char*));//aloca as linhas

    for(int i = 0; i < linha; i++){
        matriz[i] = (char*) calloc(coluna, sizeof(char));// alocas as colunas
    }
    return matriz;
}

char **liberar_matriz(int m, int n, char **v)
{
    if (v == NULL)
        return (NULL);

    if (m < 1 || n < 1) // verifica parametros recebidos
    {
        printf("ERRO: Parametro invalido\n");
        return (v);
    }

    for (int i = 0; i < m; i++) // libera a colunas
    {
        free(v[i]);
    }

    free(v); // libera as linhas

    return (NULL);
}

//int aloca_vetor_linha(int m, int *v)//m = linha, v = vetor