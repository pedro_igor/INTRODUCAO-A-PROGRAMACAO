#include<stdio.h>
#include<stdlib.h>

int main(int argc, char const *argv[])
{
	system("clear");

	printf("************************************\n" );
	printf("* Bem-Vindo ao Jogo de Adivinhação *\n");
	printf("************************************\n" );
	
	int numerosecreto;
	numerosecreto = 42;

	int chute;

	printf("Qual é o seu chute? ");
	scanf("%d", &chute);
	printf("Você chutou o número %d\n", chute );

	int acertou = chute == numerosecreto;

	if(acertou){
		printf("Parabéns! Você acertou!");
		printf("Jogue de novo, voê é um bom jogador!\n");
	}
	else{
		printf("Você errou!");

		int maior = chute > numerosecreto;
		if(maior){
			printf("Seu chute foi maior que o numero secreto\n");
		}
		else{
			printf("Seu chute foi menor do que o número secreto\n");
		}

		printf("Mas não desanime, tente novamente\n");
	}
	return 0;
}