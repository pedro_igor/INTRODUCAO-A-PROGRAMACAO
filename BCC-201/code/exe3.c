#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
    double x1, x2, x3;

    system("clear");

    printf("Digite o x1, x2, x3: ");
    scanf("%lf, %lf, %lf\n", &x1, &x2, &x3);

    double y = pow((x1 + 3), 4.0) + pow((x2 * x3), 3.0);

    printf("O resultado da expressao eh: %3lf\n", y);

    return 0;
}