//exercicio 2: bonus para cliente
#include <stdio.h>

int main()
{

    double valor_de_compras;
    double bonus;

    printf("Cliente, digite o valor de suas comprass no ano anterior:\n");
    scanf("%lf", &valor_de_compras);

    if (valor_de_compras >= 50000)
    {

        bonus = valor_de_compras - (valor_de_compras * 0.15);
        printf("Bonus do Cliente: R$ %.2lf", bonus);
    }
    else
    {
        bonus = valor_de_compras - (valor_de_compras * 0.10);
        printf("Bonus do Cliente: R$ %.2lf", bonus);
    }

    return 0;
}