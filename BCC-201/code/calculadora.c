//exercicio 1
#include <stdio.h>

int main()
{
    double x, y;
    char op;
    double resul;

    printf("DIGITE O PRIMEIRO NUMERO: ");
    scanf("%lf ", &x);

    printf("DIGITE O SEGUNDO NUMERO: ");
    scanf("%lf", &y);

    printf("Qual das operações simples deseja fazer: ");
    scanf("%c", &op);

    if (op == '+')
    {
        resul = x + y;
        printf("%.2lf %c %.2lf = %.2lf\n", x, op, y, resul);
    }
    else if (op == '-')
    {
        resul = x - y;
        printf("%.2lf %c %.2lf = %.2lf\n", x, op, y, resul);
    }
    else if (op == '*')
    {
        resul = x * y;
        printf("%.2lf %c %.2lf = %.2lf\n", x, op, y, resul);
    }
    else if (op == '/')
    {
        resul = x / y;
        printf("%.2lf %c %.2lf = %.2lf\n", x, op, y, resul);
    }
    else
    {
        printf("OPERAÇÃO INVALIDA\n");
    }

    return 0;
}