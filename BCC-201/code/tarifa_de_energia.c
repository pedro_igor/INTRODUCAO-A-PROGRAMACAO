//exercicio 3: tarifa de energia
#include <stdio.h>
#include <stdlib.h>

int main()
{
    system("clear");
    printf("CÁLCULO DA CONTA DE ENERGIA ELÉTRICA\n");
    printf("------------------------------------\n");

    const float tx_basica = 5.00;

    float consumo;
    float custo;

    printf("DIGITE O CONSUMO DE ENERGIA ELÉTRICA  (KW): ");
    scanf("%f", &consumo);

    if (consumo < 0)
    {
        printf("\nVALOR INVALIDO\n");
    }
    else
    {
        if (consumo <= 500)
        {
            custo = consumo * 0.02;
        }
        else if (consumo > 500 && consumo <= 100)
        {
            custo = 500 * 0.1 + (consumo - 500) * 0.05;
        }
        else
        {
            custo = 1000 * 0.35 + (consumo - 1000) * 0.1;
        }
    }

    printf("TAXA BÁSICA: %.2f\n", tx_basica);
    printf("CONSUMO (KW): %.2f\n", consumo);
    printf("VALOR DA CONTA (R$): %.2f\n", custo);
    
    return 0;
}