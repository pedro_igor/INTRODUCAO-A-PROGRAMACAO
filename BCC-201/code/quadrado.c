#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>

int main()
{
     system("clear");   
    
    int i, j, x, y;
    char ch = 42;

    printf("_______________________________________________\n");
    printf("Programa que imprime um QUADRADO em forma de %c\n", ch);

    printf("De o numerros de linhas e coluna do quadrado:\n");
    scanf("%i", &x);

    printf("\n");

    for(i = 1; i <= x; i++){
        for(j = 1; j <= x; j++){
            printf(" %c", ch);
        }
        printf("\n");
    }

    return 0;
}
