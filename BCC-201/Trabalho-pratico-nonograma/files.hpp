//chamada de função para manipulação de arrquivo 
#ifndef FILES_HPP
#define FILES_HPP

#include <stdio.h>
#include <stdlib.h>
#include "color.h"
//#include "impressao.hpp"

typedef struct mat{
    int vet_lin[26][26];//matriz para amarzenar os numeros da linhas
    int maior_lin;//armazena a quantidade de numeros que haverá nas linhas 
    int vet_col[26][26];//matriz para amarzenar os numeros da coluna
    int maior_col;//armazena a quantidade de numeros que haverá nas colunas
    int vet_maior_lin[26];//informação das quantidade de número nas linhas
    int vet_maior_col[26];//informação das qantidades de numeros nas colunas
    int valor_coluna[26];//armazena os números auxiliares das colunas isso ajudará na impressão
    int valor_linha[26];//armazenas os números auxiliares das linhas isso ajudará na impressão
}Mat;

void ler_arquivo(char **matriz, FILE *arquivo, int *m, int *n, Mat *matrizaux);
void write_file(char **matriz, FILE *arquivo, int m, int n, Mat *matrizaux);

#endif