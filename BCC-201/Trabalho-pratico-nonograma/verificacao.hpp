#ifndef VERIFICACAO_HPP
#define VERIFICACAO_HPP

#include <stdio.h>
#include <stdlib.h>
#include "files.hpp"
#include "jogada.hpp"

bool verifica_matriz(int m, int n, char **matriz, Mat *matriz_aux, int linha, int coluna);
void verifica_as_cocordenadas(int linha, int coluna, char **matriz, char caracter, int m, int n, Mat *matri_aux);

#endif