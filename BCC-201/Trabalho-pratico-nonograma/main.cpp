#include <stdio.h>
#include <stdlib.h>

#include "files.hpp"
#include "alocacao_desalocar.hpp"
#include "impressao.hpp"
#include "color.h"
#include "menu.hpp"
#include "jogada.hpp"

int main(int argc, char *argv[])
{
    printf("%s", CLEAR);

    if (argc != 2)
    {
        printf("%s", CLEAR);
        printf("%s%sERRO%s:Arquivo não encontrado\n", RED, NEGRITO, RESET);
        return 0;
    }

    menu(&argv[1]);

    return 0;
}