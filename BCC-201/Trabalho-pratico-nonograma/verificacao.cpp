#include "verificacao.hpp"

bool verifica_matriz(int m, int n, char **matriz, Mat *matriz_aux, int linha, int coluna)
{
    
    for(int i = 0; i < m; i++)
    {
        for(int j = 0; j < n; j++)
        {
            if(matriz[i][j] == '.')
            {
                return 0;
            }
        }
    }
    return 1;
}

//bool
void verifica_as_cocordenadas(int linha, int coluna, char **matriz, char caracter, int m, int n, Mat *matriz_aux)
{
    if(caracter == 'x' || caracter == '.' || caracter == '-' || caracter == 'X' )
    {
        muda_o_valor(linha, coluna, matriz, caracter);
        impress(matriz, m, n, matriz_aux);
    }
    else
    {
        printf("%s%s%c ERRO: Paramentros Invalidos.%s\n", RED, NEGRITO, 07, RESET);
        printf("Jogue novamente.\n\n");
    }
    
}