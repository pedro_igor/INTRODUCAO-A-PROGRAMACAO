#include "jogada.hpp"

int comandos(char **matriz, int m, int n, Mat *matrizaux, char *entrada2)
{
    char entrada[25];
    bool end_game = false;
    int cont = 1;//esse contador é para saber quantas vezes entrou dentro do 'resolver'
    
    impress(matriz, m, n, matrizaux);
    do
    {
    
        printf("Digite um comando: ");
        //variaveis
        char caracter;
        int linha, coluna;
        //fim de variavais
        getchar();
        fflush(stdout);

        cin >> entrada;// x  . -
        if(strcmp(entrada, "sair") == 0)
        {
            break;
        }
        else if(strcmp(entrada,"resolver") == 0)
        {
            resolver(matriz, m, n, matrizaux);
            impress(matriz, m ,n, matrizaux);

            if(cont == 1)
            {
                //printf(CLEAR);
                printf("DICA 1.\n");
                printf("O objetivo é descobrir quais os quadradinhos\n ");
                printf("que devem ser pintados ou deixados como vazios.\n\n");
                cont++;
            }
            else if(cont == 2)
            {
                //printf(CLEAR);
                printf("DICA 2.\n");
                printf("Note que existem números em cima de cada coluna e à \n");
                printf("de cada linha. \nElas indicam quantos quadradinhos devem ser\n");
                printf("pintados em sequência, formando, assim, blocos.\n\n");
                cont++;
            }
            else if(cont == 3)
            {
                //printf(CLEAR);
                printf("DICA 3.\n");
                printf("O ideal é começar pelos números maiores ou pela sequência de maior soma,\n");
                printf("tanto na vertical quanto na horizontal.\n\n");
                cont = 1;
            }

            printf("ESTAMOS TRABALHANDO NISSO PARA LHE ENTREGAR A MELHOR EXPERIENCIA\n"
                    "NA PROXIMA VERSÃO.\n\n");

            continue;
            //return 2;
        }
        cin >> entrada2;// coordenada

        if(strcmp(entrada,"salvar") == 0)
        {
            return 1;
        }
         
        {
            caracter = entrada[0];
            linha = converter_charToInt(entrada2, 0);
            coluna = converter_charToInt(entrada2, 1);

            verifica_as_cocordenadas(linha,coluna,matriz,caracter, m, n, matrizaux);

            end_game = verifica_matriz(m, n, matriz, matrizaux, linha, coluna);

            if(end_game == 1)
            {
                printf("PARABÉNS VOCÊ TERMINOU O JOGO.\n");
                return 3;
            }
        }


    }while(strcmp(entrada,"sair") != 0);
    return 0;
}

int converter_charToInt(char entrada[5], int position)
{
    //tanto aceita letra Maiúsculas e Minúsculas
    if(entrada[position] == 'A' || entrada[position] == 'a')
        return 0;
    else if(entrada[position] == 'B' || entrada[position] == 'b')
        return 1;
    else if(entrada[position] == 'C' || entrada[position] == 'c')
        return 2;
    else if(entrada[position] == 'D' || entrada[position] == 'd')
        return 3;
    else if(entrada[position] == 'E' || entrada[position] == 'e')
        return 4;
    else if(entrada[position] == 'F' || entrada[position] == 'f')
        return 5;
    else if(entrada[position] == 'G' || entrada[position] == 'g')
        return 6;
    else if(entrada[position] == 'H' || entrada[position] == 'h')
        return 7;
    else if(entrada[position] == 'I' || entrada[position] == 'i')
        return 8;
    else if(entrada[position] == 'J' || entrada[position] == 'j')
        return 9;
    else if(entrada[position] == 'K' || entrada[position] == 'k')
        return 10;
    else if(entrada[position] == 'L' || entrada[position] == 'l')
        return 11;
    else if(entrada[position] == 'M' || entrada[position] == 'm')
        return 12;
    else if(entrada[position] == 'N' || entrada[position] == 'n')
        return 13;
    else if(entrada[position] == 'O' || entrada[position] == 'o')
        return 14;
    else if(entrada[position] == 'P' || entrada[position] == 'p')
        return 15;
    else if(entrada[position] == 'Q' || entrada[position] == 'q')
        return 16;
    else if(entrada[position] == 'R' || entrada[position] == 'r')
        return 17;
    else if(entrada[position] == 'S' || entrada[position] == 's')
        return 18;
    else if(entrada[position] == 'T' || entrada[position] == 't')
        return 19;
    else if(entrada[position] == 'U' || entrada[position] == 'u')
        return 20;
    else if(entrada[position] == 'V' || entrada[position] == 'v')
        return 21;
    else if(entrada[position] == 'W' || entrada[position] == 'w')
        return 22;
    else if(entrada[position] == 'X' || entrada[position] == 'x')
        return 23;
    else if(entrada[position] == 'Y' || entrada[position] == 'y')
        return 24;
    else if(entrada[position] == 'Z' || entrada[position] == 'z')
        return 25;

    return (int)NULL;
}

void muda_o_valor(int linha, int coluna, char **matriz, char caracter)
{
    if(caracter == 'x')
    {
        caracter = 'X';
        matriz[linha][coluna] = caracter;
    }
    else
    {
        matriz[linha][coluna] = caracter;
    }
}

