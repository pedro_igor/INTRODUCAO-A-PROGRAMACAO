#ifndef JODADA_H
#define JOGADA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "files.hpp"
#include "color.h"
#include "verificacao.hpp"
#include "impressao.hpp"
#include "resolver.hpp"

using namespace std;

int converter_charToInt(char entrada[5], int position);
int comandos(char **matriz, int m, int n, Mat *matrizaux, char *entrada2);
void muda_o_valor(int linha, int coluna, char **matriz, char caracter);


#endif