#include "menu.hpp"

void menu(char **nome_arq)
{
    puts(CLEAR);
    printf("%s%sBEM VINDO AO NONOGRAMA!%s\n", NEGRITO, UNDERLINE, RESET);
    printf("┌─────────────────┐\n");
    printf("│     %s %sMENU%s       │\n", NEGRITO, UNDERLINE, RESET);
    printf("├─────────────────┤\n");
    printf("│ 1 - Jogar       │\n");
    printf("│ 2 - Como jogar  │\n");
    printf("│ 3 - Informaçoes │\n");
    printf("└─────────────────┘\n");
    //puts("\n");

    int opcao;// opçaõ para o menu

    //variaveis
    int m, n;//m e n, são respectivamente linhae colunas
    char entrada2[30]; //entrada para o arquivo de saida (onde o Jogo será salvo)
    int save_or_out;//verifica se vai salvar ou nao
    FILE *file_out;                              // arquivo de saida, onde será salvo o jogo
    FILE *arquivo;                              // arquivo de entrada do jogo
    FILE *continue_game;                       // caso o ususari queria continuar o jogo


    scanf("%d", &opcao);
    //getchar();

    //while (opcao != 4)
    
    switch (opcao)
    {
        case 1: //no case 1 é onde o jogo ŕa rodar de verdade
            
            puts(CLEAR);

            int op; //opção do subMenu

            printf("COMEÇAR UM NOVO JOGO (1) OU CONTINUAR (2): ");
            scanf("%d", &op);
            //getchar();

            switch (op)
            {
                case 1:
                //inicio
                    if ((arquivo = fopen(*nome_arq, "r")) == NULL)
                    {
                        printf("%s%sERRO NA ABERTURA DO ARQUIVO.%s\n", RED, NEGRITO, RESET);
                        exit(1);
                    }
                    else
                    {
                        fscanf(arquivo, "%d %d", &m, &n);

                        if(m > 0 && n > 0 && m < 27 && n < 27)
                        {
                            Mat *matrzaux = (Mat*)malloc(sizeof(Mat));

                            char** matriz = alocar_matriz(m,n);
                            char entrada2[30];
                            
                            ler_arquivo(matriz, arquivo, &m, &n, matrzaux);

                            //jogada();
                            //impress(matriz,m,n,matrzaux);
                            save_or_out = comandos(matriz, m, n, matrzaux, entrada2);// se a for 1, salva se nao sai;

                            //printf("Menu: %d",save_or_out );
                            if(save_or_out == 1)
                            {
                                if((file_out = fopen(entrada2, "w")) == NULL)
                                {
                                    printf("%s%sERRO NA ABERTURA DO ARQUIVO.%s\n", RED, NEGRITO, RESET);
                                    exit(1);
                                }
                                else
                                {   
                                    write_file(matriz, file_out, m, n, matrzaux);
                                    matriz = liberar_matriz(m, n, matriz);//libera matriz
                                    printf("NONOGAMA SALVO COM SUCESSO EM: %s \n", entrada2);
                                    //printf("Você abandonou o jogo\n");
                                //return 0;
                                }
                            }            
                            else if(save_or_out == 2)
                            {
                                printf("Volte sempre.\n");
                            }
                            else if( save_or_out == 3)
                            {
                                if((file_out = fopen("final_game.txt","w")) == NULL)
                                {
                                    printf("%s%sERRO NA ARBERTURA DO ARQUIVO%s\n", RED, NEGRITO, RESET);
                                    exit(1);
                                }
                                else
                                {
                                    write_file(matriz, file_out, m, n, matrzaux); //salva o ultimo jogo terminado.
                                    matriz = liberar_matriz(m, n, matriz);//libera matriz
                                    printf("© PEDRO.DEVELOPER\n");
                                }
                            }
                            else if(save_or_out == 0)
                            {
                                matriz = liberar_matriz(m, n, matriz);//libera matriz
                                printf("Você abandonou o jogo\n");
                            }
                            
                        }//fim do if
                        else
                        {
                            printf("ERRO: PARAMENTROS INVALIDOS.\n");
                            fclose(arquivo);
                            //fclose(file_out);
                        }//fim do else
                    }//fim do else
                //fim
                break;

                case 2://caso o usuario queira coninuaar o jogo que esteja salvo

                    char name_file[20];

                    printf(CLEAR);
                    printf("%sDigite o Nome do arquivo: %s", NEGRITO, RESET);
                    scanf("%s", name_file);

                    if((continue_game = fopen(name_file,"r")) == NULL)
                    {
                        printf("%s%sERRO NA ABERTURA DO ARQUIVO.%s\n", RED, NEGRITO, RESET);
                        exit(1);
                    }
                    else
                    {
                        fscanf(continue_game, "%d %d", &m, &n);

                        if(m > 0 && n > 0 && m < 27 && n < 27)
                        {
                            Mat *matrzaux = (Mat*)malloc(sizeof(Mat));

                            char** matriz = alocar_matriz(m,n);

                            ler_arquivo(matriz, continue_game, &m, &n, matrzaux);

                            save_or_out = comandos(matriz, m, n, matrzaux, entrada2);// se a for 1, salva se nao sai;

                            if(save_or_out == 1)
                            {
                                if((file_out = fopen(entrada2, "w")) == NULL)
                                {
                                    printf("%s%sERRO NA ABERTURA DO ARQUIVO.%s\n", RED, NEGRITO, RESET);
                                    exit(1);
                                }
                                else
                                {
                                    write_file(matriz, file_out, m, n, matrzaux);
                                    matriz = liberar_matriz(m, n, matriz);// liberar matriz
                                    printf("NONOGAMA SALVO COM SUCESSO EM: %s \n", entrada2);
                                    //return 0;
                                }
                                
                            }
                            else if(save_or_out == 2)
                            {
                                printf("Volte sempre.\n");
                            }
                            else if(save_or_out == 3)
                            {
                                matriz = liberar_matriz(m, n, matriz);//libera matriz
                                printf("© PEDRO.DEVELOPER\n");
                            }
                            else
                            {
                                matriz = liberar_matriz(m, n, matriz);//libera matriz
                                printf("Você abandonou o jogo\n");
                            }//fim do else save_or_out
                            
                        }
                        else
                        {
                            printf("ERRO: PARAMENTROS INVALIDOS.\n");
                            fclose(continue_game);
                        }
                        
                    }//fim do primeiro else
                    

                    break;//fim do continue, ou seja, o jogo salvo

                default:
                    printf("Erro: opção invalida.\n");
                    break;
            }

            break;

        case 2:
            //informações como jogar
            printf("%s", CLEAR);
            printf("┌─────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐\n");
            printf("│                                             %s%sCOMO JOGAR:%s                                                         │\n", NEGRITO, UNDERLINE, RESET);
            printf("├──────────┬──────────────────────────────────────────────────────────────────────────────────────────────────────┤\n");
            printf("│    x     │ Preenche a cédula da linha e coluna com um 'x'                                                       │\n");
            printf("│    -     │ Preenche a cédula da linha e coluna com um '-'                                                       │\n");
            printf("│    .     │ Limpa a cédula da linha e coluna                                                                     │\n");
            printf("│ Resolver │ Resolve o nonograma e marca automaticamente as cédulas com 'x' ou '-'                                │\n");
            printf("│  Salvar  │ Salva o nonograma tal como está no momento no arquivo 'out.txt', ou outro arquivo de sua preferencia │\n");
            printf("│   Sair   │ Encerra o programa (sem salvar as últimas alterações)                                                │\n");
            printf("└──────────┴──────────────────────────────────────────────────────────────────────────────────────────────────────┘\n");

            printf("\n\nAperte ENTER para continuar...\n");
            getchar();
            getchar();
            menu(nome_arq);

        break;

        case 3:
            //informações na internet
            printf("%s", CLEAR);
            printf(" MAIS INFORMAÇÕES DO JOGO NOS SEGUINTES SITES:\n");
            puts("\n");
            printf("http://www.nonograms.org/instructions  -> Instruções do jogo\n");
            printf("https://en.wikipedia.org/wiki/Nonogram -> Um pouco da história\n");

            printf("\nAperte ENTER para continuar...\n");
            getchar();
            getchar();
            menu(nome_arq);

        break;

        default:
            printf("%s%sComando invalido%s\n", RED, NEGRITO, RESET);
        break;
    }//fim do primeiro switch 
    

} // fim da função

