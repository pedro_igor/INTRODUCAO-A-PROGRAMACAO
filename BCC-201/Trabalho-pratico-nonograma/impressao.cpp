#include "impressao.hpp"

void impress(char **matriz, int m, int n, Mat *matriz_aux)
{
    char a = 65, b = 65; //a e b são respectivamente coluna e linha

    //imprimir on numeros da colunas
    int maior_linha = matriz_aux->maior_lin;
    int maior_coluna = matriz_aux->maior_col;  

    int* vetor_tamanho = (int*) calloc(n, sizeof(int));
    int* vetor_tam = (int*) malloc(n * sizeof(int));

    for(int i = 0; i < n; i++)
    {
        vetor_tam[i] = matriz_aux->valor_coluna[i];
    }
    printf(CLEAR);
    printf("\n");

    int aux = maior_coluna;

    for(int i = 0; i < maior_coluna ; i++)
    {
        for(int k = 0; k < maior_coluna + 1; k++)
        {
            printf("  ");
        }
    
        for(int j = 0; j < n; j++)
        {
            if(aux == vetor_tam[j])
            {
                printf("%d ", matriz_aux->vet_col[j][vetor_tamanho[j]]);//adiciona o número à coluna
                vetor_tamanho[j]++;
                vetor_tam[j]--;
            }
            else
                printf("  ");
            
        }
        aux--;
        printf("\n");
    }

    free(vetor_tamanho);
    free(vetor_tam);

    for(int j = 0; j < maior_linha + 1; j++ )
    {
        printf("  ");
    }
    
    //imprimir os numeros das linhas
    for (int i = 0; i < n; i++, a++)
    {
        printf("%c ", a); //imprime a letra das colunas
    }

    a -= n;

    puts("\n");

    for (int i = 0; i < m; i++, b++) //or i < m
    {  
        for(int w = 0; w < matriz_aux->maior_lin - matriz_aux->vet_maior_lin[i]; w++)
        {
            printf("  ");
        }

        for(int k = 0; k < maior_linha ; k++)
        {  
            if(matriz_aux->vet_lin[i][k]) // se for 0 nao imprime
            {
                printf("%d ", matriz_aux->vet_lin[i][k]);//adiciona número à linhas sem os zero
            }
        
        }
        
        printf("%c ", b); //imprime as letras das linhas

        for (int j = 0; j < n; j++)
        {
            if(matriz[i][j] == 'x' || matriz[i][j] == 'X')
                printf("%s%s%c %s", NEGRITO,RED, matriz[i][j], RESET);
            else if(matriz[i][j] == '.' )
                printf("%c ",matriz[i][j]);
            else
                printf("%s%c %s",ANSI_COLOR_GREEN, matriz[i][j], RESET);
        }
        
        puts("\n");

    }

    a = 65;
    for (int i = 0; i < n; i++, a++)
    {
        if (a < 10)
            printf(" ");
    }
    puts("\n");

}
