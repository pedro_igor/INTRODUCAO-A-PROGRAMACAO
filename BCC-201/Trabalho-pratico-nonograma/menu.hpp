#ifndef MENU_H
#define MENU_H

#include <stdio.h>
#include <stdlib.h>
#include "color.h"
#include "files.hpp"
#include "alocacao_desalocar.hpp"
#include "impressao.hpp"
#include "jogada.hpp"

void menu(char **nome_arq);

#endif