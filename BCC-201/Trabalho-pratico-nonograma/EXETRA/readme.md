O pratico prarico constite em desenvolver um programa que o usuario possa jogar um jogo de origem japonesa, que é uma matriz MxN onde pelo arquivo esta as dimençoes da matriz.

Instruções do Jogo
------------------------------------------------------------------------------------------------
| x           | Preenche a celula da linha M e a coluna N com um 'x'                           |
| -           | Preenche a celula da linha M e a coluna N com um '-'                           |
| .           | Limpa a celula da linha M e a coluna N                                         |
| resolver    | Resolve o nonograma e marca automaticamente  as celulas com um 'x' ou '-'      |
| salvar      | Salva o nonograma tal como está no momento no arquivo "out.txt"                |
| sair        | Encerra o programa (sem salvar as ultimas alteraçoes);                         |
------------------------------------------------------------------------------------------------

Comecei a desenvolver o jogo pela matriz dinamica pois, achei mais facil.