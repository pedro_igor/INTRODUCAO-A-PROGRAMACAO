#include <stdio.h>
#define TAM 50

int main()
{
    int vet[TAM];

    for(int i = 0; i < TAM; i ++ )
    {
        vet[i] = (i) * 2;   
    }

    for(int i = 0; i < TAM; i++)
    {
        printf("%d ", vet[i]);
    }

    printf("\n");
    return 0;
}