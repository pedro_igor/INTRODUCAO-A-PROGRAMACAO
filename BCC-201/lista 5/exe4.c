#include <stdio.h>
#define TAM 5

int main()
{
    int vetA[TAM];
    int vetB[TAM];
    int vetC[TAM];

    int nro;

    for(int i = 0; i < TAM; i++)
    {
        printf("Usuario digite o vetor A na posiçao %d: ", i + 1);
        scanf("%d", &nro);

        vetA[i] = nro;
    }

    printf("\n");
    for(int i = 0; i < TAM; i++)
    {
        printf("Usuario digite o vetor B na posiçao %d: ", i + 1);
        scanf("%d", &nro);

        vetB[i] = nro;
    }

    for(int i = 0; i < TAM; i++)
    {
        vetC[i] = vetA[i] + vetB[i];
    }

    for(int i = 0; i < TAM; i++)
    {
        printf("%d ", vetC[i]);
    }

    return 0;
}'
