#include <stdio.h>
#include <math.h>

#define TAM 10

void vetor(int x, double *vet);
void troca(double *a, double *b);

int main()
{
    int x; 
    printf("Usuario digite o valor de x: ");
    scanf("%d", &x);

    double vet[TAM] = {1,2,3,4,5,6,7,8,9,10};

    vetor(x,vet);

    for(int i = 0; i < TAM; i++)
    {
        printf("%.0lf ", vet[i]);
    }

    printf("\n");

    return 0;
}
void vetor(int x, double *vet)
{
    if (x > 30 && x % 2 == 0)
    {
        for (int i = 0; i < TAM; i++)
        {
            vet[i] += x;
        }
    }
    else if (x < 2 && x % 2 != 0)
    {
        for (int a = 0, i = TAM - 1; i >= TAM / 2; i--, a++)
        {
            troca(&vet[i], &vet[a]);
        }
    }
    else
    {
        for (int i = 0; i < TAM; i++)
        {
            vet[i] = sqrt(vet[i]);
        }
    }
}

void troca(double *a, double *b)
{
    int aux = *a;
    *a = *b;
    *b = aux;
}