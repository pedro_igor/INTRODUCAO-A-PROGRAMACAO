#include <stdio.h>

#define TAM 10

int main()
{
    int vet[TAM];
    int soma = 0;
    int nro;

    for (int i = 0; i < TAM; i++)
    {
        printf("Usuario Digite o valor na %d posição: ", i + 1);
        scanf("%d", &nro);

        vet[i] = nro;
    }

    for (int i = 0; i < TAM; i++)
    {
        soma += vet[i];
    }

    printf("Soma: %d\n", soma);

    return 0;
}