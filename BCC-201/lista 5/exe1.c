#include <stdio.h>

#define TAM 15

int main()
{
    int vet[TAM];

    for (int i = 0; i < TAM; i++)
    {
        vet[i] = (3 * i + 3);
    }

    for (int i = 0; i < TAM; i++)
    {
        printf("%d ", vet[i]);
    }

    printf("\n");

    /*for (int i = 0; i < 100; i++)
    {
        vet[i] = i + 1;
    }

    for (int i = 0; i < 100; i++)
    {
        printf("%d ", vet[i]);
    }

    printf("\n");*/

    return 0;
}