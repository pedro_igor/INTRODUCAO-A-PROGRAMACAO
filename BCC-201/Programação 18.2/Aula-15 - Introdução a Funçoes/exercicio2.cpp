//Exercicio 2, area do triangulo
#include<iostream>
#include<cmath>
using namespace std;

double perimetroTriangulo(double a, double b, double c)
{
    double perimetro = a + b + c;

    return perimetro;
}

double area(double a, double b, double c)
{
    double s = perimetroTriangulo(a, b, c)/2;

    double area = sqrt(s * (s - a) * (s - b) * (s - c));

    return area;
}

int main()
{
    double a, b, c;

    cout << "Digite os Lados dos Triangulo: ";
    cin >> a >> b >> c;

    cout << "O perimetro do Triangulo é: " << perimetroTriangulo(a, b, c);
    cout << "\nA area do Triangulo é: " << area(a, b, c) << endl;

    return 0; 
}