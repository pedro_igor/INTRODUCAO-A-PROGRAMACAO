//exemplo I - converção de temperatura usando função
#include<iostream>
using namespace std;

//prototipo da função
double celsiusToFahrenheit(double tempCels);

int main()
{
    double tempC, tempF;

    cout << "Conveersão Celsius para Fahrenheit "
         << " (Valor menor do que -273.15 encerra o programa)" << endl;
    cout << "\nTemperatura em Celsius: ";
    cin << temC;

    while (tempC >= -273.15)
    {
        tempF = celsiusToFahrenheit(temC);
        cout << tempC << " graus em Celsius é equivalente a "
             << tempF << " graus Fahrenheit." << endl;
        cout << "\nTemperatura em Celsisus: ";
        cin >> tempC;
    }
    return 0;
}

//definição da função 
double celsiusToFahrenheit(double tempCels)
{
    double f;
    f = 1.8 * tempCels + 32;
    return f;
}