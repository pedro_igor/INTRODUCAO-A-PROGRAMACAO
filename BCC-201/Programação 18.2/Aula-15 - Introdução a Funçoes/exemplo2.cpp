//Exemplo II - Calculo do Fatorial
#include<iostream>
using namespace std;

//prototipo da funçao 
unsigned fatoreal(unsigned n);

int main()
{
    unsigned numero;

    cout << "Digite um número positivo: ";
    cin >> numero;

    cout << numero << "! = " << fatoreal(numero) << endl;

    return 0;
}

//definiçao da função 
unsigned fatoreal( unsigned n)
{
    unsigned fat = 1;

    for (unsigned i = 1; i <= n; i++)
        fat *= i;

    return fat;
}