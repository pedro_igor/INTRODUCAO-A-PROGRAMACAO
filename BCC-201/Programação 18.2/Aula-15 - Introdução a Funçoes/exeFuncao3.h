//funçao que imprime um numero como moeda
#include<iostream>
#include<iomanip>
using namespace std;

void printAsMoney(double n)
{
    cout << fixed << showpoint << setw(8)
        << right << setprecision(2)
        << "R$ " << n;
}