//exercicio/exemplo perimetro de um triangulo
#include<iostream>
using namespace std;

double perimetroTriangulo(double a, double b, double c)
{
    double perimetro = a + b + c;

    return perimetro;
}

int main()
{
    double a, b, c;

    cout << "Digite os lados do Trianngulo: ";
    cin >> a >> b >> c;

    cout << "O perimetro do Triangulo é: " << perimetroTriangulo(a, b, c) << endl;

    return 0;
}