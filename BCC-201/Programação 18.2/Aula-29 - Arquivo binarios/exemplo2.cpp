//exemplo 2 binario leitura
#include<iostream>
#include<fstream>
using namespace std;

typedef struct Ponto
{
	float x, y;
} ponto;

int main(int argc, char const *argv[])
{
	ifstream nfp;
	ponto P1;
	nfp.open("arquivoStruct.txt", ios::binary);
	if(nfp.fail())
	{
		cout << "Erro na abertura do arquivo.\n";
		exit(1);
	}
	while (nfp.read((char*)&P1, sizeof(ponto)))
	{
		cout << P1.x << " " << P1.y << endl;
	}

	return 0;
}