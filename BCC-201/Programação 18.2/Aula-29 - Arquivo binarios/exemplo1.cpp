//exemplo arquivo binario
#include<iostream>
#include<fstream>

using namespace std;

typedef struct Ponto
{
	float x, y;
} ponto;

int main(int argc, char const *argv[])
{
	int i, n;
	ponto p;
	ofstream fp;
	fp.open("arquivoStruct.txt", ios::binary);
	if(fp.fail())
	{
		cout << "Erro na abertura do arquivo.\n";
		exit(1);
	}

	cout << "Digite numero de pontos a gravar:\n";
	cin >> n;

	for(i = 0; i < n; i++)
	{
		cin >> p.x >> p.y;
		fp.write((char*)&p, sizeof(ponto));
	}
	fp.close();
	return 0;
}