//exemplo 3
#include<iostream>
#include<fstream>
using namespace std;

int main()
{
	int n, *v;
	ofstream txt, bin;

	cin >> n;
	v = new int[n];
	for(int i = 0; i < n; i++)
	{
		cin >> v[i];
	}

	txt.open("vetor.txt");
	txt << n << endl;
	for(int i = 0; i < n; i++)
	{
		txt << v[i] << " ";
	}
	txt.close();

	bin.open("vetor.bin", ios::binary);
	bin.write((char*) &n, sizeof(int));
	bin.write((char*)v, n * sizeof(int));
	bin.close();

	delete[] v;
	return 0;
}