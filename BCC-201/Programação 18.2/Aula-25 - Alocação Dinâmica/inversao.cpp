/*
**crie um função que:
**1ºrecebe um vetor v e seu tamanho n por parametro;
**2ºcria um novo vetor por alocação dinâmica, preenchendo-o com o
conteúdo de v em ordem inversa;
**3ºretorna este novo vetor.
*/
#include <iostream>
using namespace std;

int *inverso(int *v, int n)
{
	int *inv = new int[n];
	for (int i = 0; i < n; i++)
	{
		inv[i] = v[n - 1 - i];
	}
	return inv;
}

int main()
{
	int v[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

	//obtendo o inverso e  imprimindo o resultado
	int *inv = inverso(v, 10);
	for (int i = 0; i < 10; i++)
	{
		cout << inv[i] << " " << endl;
	}

	//liberando memoria
	delete[] inv;

	return 0;
}