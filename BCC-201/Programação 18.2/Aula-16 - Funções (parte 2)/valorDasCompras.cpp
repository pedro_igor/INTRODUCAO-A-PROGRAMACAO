//exercicio 1
#include <iostream>
using namespace std;

int main()
{
    int dinheiro; //não é double porque no exercicio nao à exemplo de ponto flutuante e nem moedas abaixo e  1
    int cont50 = 0, cont20 = 0, cont10 = 0;
    int cont5 = 0;
    int cont1 = 0; //isto é respectivamente as notas/a moeda citada no exercico
    
    cout << "VALOR DA COMPRA: ";
    cin >> dinheiro;

    while (dinheiro - 50 > -1)
    {
        dinheiro = dinheiro - 50;
        cont50++;
    }
    while (dinheiro - 20 > -1)
    {
        dinheiro = dinheiro - 20;
        cont20++;
    }
    while (dinheiro - 10 > -1)
    {
        dinheiro = dinheiro - 10;
        cont10++;
    }
    while (dinheiro - 5 > -1)
    {
        dinheiro = dinheiro - 5;
        cont5++;
    }
    while (dinheiro - 1 > -1)
    {
        dinheiro = dinheiro - 1;
        cont1++;
    }

    cout << "O VALOR DA COMPRA SERÁ PAGO COM: \n";
    if (cont50 > 0)
    {
        cout << cont50 << " NOTA(S) DE CINQUENTA\n";
    }
    if (cont20 > 0)
    {
        cout << cont20 << " NOTAS(S) DE VINTE\n";
    }
    if (cont10 > 0)
    {
        cout << cont10 << " NOTAS(S) DE DEZ\n";
    }
    if (cont5 > 0)
    {
        cout << cont5 << " NOTA(S) DE CINCO\n";
    }
    if (cont1 > 0)
    {
        cout << cont1 << " MOEDA(S) DE UM\n";
    }

    return 0;

} 