//numero de combinaçoes de n elementos tomados k a k
#include<iostream>
using namespace std;

unsigned combinacao( int n, int k);
unsigned fatoreal( unsigned n);

int main()
{
	int n, k;

	cout << "Digite o 'n' e o 'k': ";
	cin >> n >> k;

	
	cout << combinacao(n, k) << " Combinaçoes posiveis " << endl;

	return 0;
}

unsigned combinacao( int n, int k)
{
	unsigned c = fatoreal(n) / (fatoreal(k) * fatoreal(n - k));

	return c;
}

unsigned fatoreal( unsigned n)
{
    unsigned fat = 1;

    for (unsigned i = 1; i <= n; i++)
        fat *= i;

    return fat;
}