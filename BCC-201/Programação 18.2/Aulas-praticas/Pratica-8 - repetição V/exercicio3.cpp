//comando de repetição V: exercicio 3
#include<iostream>
using namespace std;

int main()
{
    int tamanho = 12;

    for(int i = 1; i <= tamanho; i++){
        for(int j = 1; j <= tamanho; j++){
            if(i==j || j==(tamanho +1) - i)
                cout << "xx";
            else
                cout << " ";
        }
        cout << endl;
    }
    return 0;
}
