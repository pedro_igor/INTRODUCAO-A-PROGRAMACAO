//comando de repetição V : exercicio 2
#include<iostream>
#include<cmath>
using namespace std;

int main()
{
    int a, b, r = 1, m;//m que paga  o resto

    do
    {
        cout << "Digite dois Números inteiros: ";
        cin >> a >> b;
        while (r <= b)
        {
            r = r * 10;
        }

        m = a % r;

        if(m == b)
        {
            cout << b << " correspende o final de " << a << endl;
        }
        else
            cout << b << " não corresponde o final de " << a << endl;

    }
    while(a != 0 && b != 0);

    return 0;
}
