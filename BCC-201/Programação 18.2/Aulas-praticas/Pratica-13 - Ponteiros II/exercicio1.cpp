//Aula pratica 13 - Ponteiros II
#include <iostream>
using namespace std;

void readVetor(int *vet, int n)
{
    for (int i = 0; i < n; i++)
    {
        cin >> vet[i];
    }
}

void printVetor(int *vet, int n)
{
    for (int i = 0; i < n; i++)
    {
        cout << vet[i] << " ";
    }
}

double calcMedia(int *vet, int n)
{
    int soma = 0;

    for (int i = 0; i < n; i++)
    {
        soma += vet[i];
    }

    double media = soma / n;

    return media;
}

int *maiorMedia(int *vet, int n, int media, int &maior)
{
    int *vet2;

    for (int i = 0; i < n; i++)
    {
        if (vet[i] >= media)
        {
            maior++;
        }
    }

    vet2 = new int[maior];

    int j = 0;
    for (int i = 0; i < n; i++)
    {
        if (vet[i] >= media)
        {
            vet2[j] = vet[i];
            j++;
        }
    }

    return vet2;
}

int *menorMedia(int *vet, int n, int media, int &menor)
{
    int *vet3;

    for (int i = 0; i < n; i++)
    {
        if (vet[i] < media)
        {
            menor++;
        }
    }

    vet3 = new int[menor];

    int j = 0;
    for (int i = 0; i < n; i++)
    {
        if (vet[i] < media)
        {
            vet3[j] = vet[i];
            j++;
        }
    }

    return vet3;
}

int main()
{
    int tamMaior = 0;
    int tamMenor = 0;
    int *vet = NULL;
    int n;
    cout << "Entre com o tamanho do seu vetor: ";
    cin >> n;

    vet = new int[n];

    cout << "Entre o conteudo do seu vetor: ";
    readVetor(vet, n);
    cout << "\nVet1 = ";
    printVetor(vet, n);

    double media = calcMedia(vet, n);

    int *vet2 = maiorMedia(vet, n, media, tamMaior);
    cout << "\nVet2 = ";
    printVetor(vet2, tamMaior);

    int *vet3 = menorMedia(vet, n, media, tamMenor);
    cout << "\nVet3 = ";
    printVetor(vet3, tamMenor);

    delete[] vet3;
    delete[] vet2;
    delete[] vet;
    cout << endl;

    return 0;
}
