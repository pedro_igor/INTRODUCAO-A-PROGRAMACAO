//pratica 13 - Ponteiros II / exercicio 2
#include <iostream>
using namespace std;

double **criaMatriz(int m, int n) //linha; coluna
{
    double **t = new double *[m];
    for (int i = 0; i < m; i++)
    {
        t[i] = new double[n];
    }
    return t;
}

double **transporta(double **A, int m, int n) //linha ;coluna
{
    double **t = criaMatriz(n, m);
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            t[i][j] = A[j][i];
        }
    }
    return t;
}

void liberaMatriz(double **A, int m) //linha
{
    if (A != NULL)
    {
        for (int i = 0; i < m; i++)
        {
            if (A != NULL)
                delete[] A[i];
        }
        delete[] A;
    }
}

void readMatrix(double **A, int lin, int col)
{
    for (int i = 0; i < lin; i++)
    {

        for (int j = 0; j < col; j++)
        {
            cin >> A[i][j];
        }
    }
}

void printMatrix(double **A, int lin, int col)
{
    for (int i = 0; i < lin; i++)
    {
        for (int j = 0; j < col; j++)
        {
            cout << A[i][j] << " ";
        }
        cout << endl;
    }
}

int main()
{
    double **A = NULL;
    int linha, coluna;

    cout << "Entre com as dimençoes de sua matriz: ";
    cin >> linha >> coluna;

    A = criaMatriz(linha, coluna);

    cout << "Entre com o conteudo do seu vetor: ";
    readMatrix(A, linha, coluna);

    double **matriz2 = transporta(A, linha, coluna);

    cout << "\nMatriz A:\n";
    printMatrix(A, linha, coluna);

    cout << "\nMatriz Transporta:\n";
    printMatrix(matriz2, coluna, linha);

    liberaMatriz(A, linha);
    liberaMatriz(matriz2, coluna);

    cout << endl;

    return 0;
}
