//exercicio 3 - ponteiros I
#include<iostream>
using namespace std;

void troca(int *a, int *b)
{
    int axu = *a;
    *a = *b;
    *b = axu;

}

int main()
{
    int numeros[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    
    cout << "Entre com um valor: ";
    cin >> numeros[10 - 1];

    for(int i = 9; i > 0; i--)
    {
        if(*(numeros + i) < *(numeros +(i - 1)))
        {
            troca(numeros + i, numeros + (i - 1));
        }
    }

    for(int i = 0; i < 10; i++)
    {
        cout << *(numeros + i) << " "; 
    }

    cout << endl;

    return 0;
}