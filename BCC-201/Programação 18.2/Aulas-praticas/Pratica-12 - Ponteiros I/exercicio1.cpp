// exercicio 1 - aula pratica ponteiros
#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;

//função
char* procuraUltimo( char *text, char find)
{
    int tam = strlen(text);
    int pos = 0;

    for(int i = 0; i <= tam; i ++)
    {
        if(text[i] == find)
        {
            pos = i;
        }
    }
    return pos + text;
}

int main()
{
    char text[256 + 1];
    char caracter;

    cout << "Digite um texto: ";
    fgets(text,267,stdin);

    cout << "Digite o carácter para prtocurar: ";
    cin >> caracter;

    char *retorno = procuraUltimo(text,caracter);

    cout << "\nÚltima posição que ele aparece: " << (retorno - text) + 1 << endl;

    return 0;
}
