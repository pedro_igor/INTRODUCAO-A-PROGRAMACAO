//exercicio 2 : pratica 6
#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int max, cateto1, cateto2, hipotenusa;
    int found;

    cout << "Digite o compremento maximo da hipotenusa: ";
    cin >> max;

    for (hipotenusa = 1; hipotenusa <= max; hipotenusa++)
    {
        found = false;
        //testa todos os valores possiveis para o cateto

        for (cateto1 = 1; cateto1 < hipotenusa && !found; cateto1++)
        {
            cateto2 = cateto1; //cateto 1 é o menor cateto

            while (pow(cateto1, 2) + pow(cateto2, 2) < pow(hipotenusa, 2))
                cateto2++;

            if (pow(cateto1, 2) + pow(cateto2, 2) == pow(hipotenusa, 2))
            {

                cout << "Hipotenusa = " << hipotenusa << ", catetos " << cateto1 << " e " << cateto2;
                cout << endl;
                found = true;
            }
        }
    }
    return 0;
}
