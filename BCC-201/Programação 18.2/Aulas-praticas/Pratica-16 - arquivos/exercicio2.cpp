//aula pratica 16 - arquivos
//exercicio 2
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstring>

using namespace std;

void ler_arquivo();
double media(double nota1, double nota2);
double frequencia(int faltas);
struct Aluno
{
    char matricula[10];
    char nome[100];
    double nota1, nota2;
    int faltas;
    double media;
};

int main(int argc, char const *argv[])
{
    system("clear");
    int m;
    cout << "Qual é o número de alunos:\n";
    cin >> m;
    Aluno *aluno = new Aluno[m];
    char op; //opção
    double media_menor;
    double media_maior;

    ofstream out_file;
    ofstream file_out;

    cout << "1 - Entrar com os dados dos alunos.\n"
         << "2 - Exibir o Aluno com a maior nota acima da media.\n"
         << "3 - Exibir o Aluno com a menor nota acima da media.\n"
         << "4 - SAIR\n";

    cout << "Digite a  opção:\n";
    cin >> op;

    do
    {

        switch (op)
        {
        case '1':
            cout << "ENTRAR COM OS DADOS DOS ALUNOS\n";

            for (int i = 0; i < m; i++)
            {
                cout << "Digite o número da matricula:\n";
                cin >> aluno[i].matricula;

                cin.ignore();
                cout << "Digite o nome do aluno:\n";
                fgets(aluno[i].nome, 100, stdin);

                cout << "Digite a nota da primeira prova:\n";
                cin >> aluno[i].nota1;

                cout << "Digite a nota da segunda prova:\n";
                cin >> aluno[i].nota2;

                cout << "Digite o números de faltas:\n";
                cin >> aluno[i].faltas;

                cout << endl;
            }
            ler_arquivo();

            break;

        case '2':
            cout << "EXIBIR NOTA ACIMA DA MEDIA:\n";

            for (int i = 0; i < m; i++)
            {
                media_maior = media(aluno[i].nota1, aluno[i].nota2);
            }

            out_file.open("MaiorMedia.txt");

            out_file << "<matr>;<cod_disc>;<situação>" << endl;

            if (media_maior >= 6.0)
            {
                for (int i = 0; i < m; i++)
                {
                    out_file << aluno[i].matricula
                             << ";BCC201" << endl;
                }
            }
            out_file.close();
            break;

        case '3':
            cout << "EXIBIR NOTA ABAIXO DA MEDIA:\n";

            for (int i = 0; i < m; i++)
            {
                media_menor = media(aluno[i].nota1, aluno[i].nota2);
            }

            file_out.open("MenorMedia.txt");

            file_out << "<matr>;<cod_disc>;<situação>" << endl;

            if (media_maior < 6.0)
            {
                for (int i = 0; i < m; i++)
                {
                    file_out << aluno[i].matricula
                             << ";BCC201" << endl;
                }
            }
            file_out.close();
            break;
        }

        cout << "1 - Entrar com os dados dos alunos.\n"
             << "2 - Exibir o Aluno com a maior nota acima da media.\n"
             << "3 - Exibir o Aluno com a menor nota acima da media.\n"
             << "4 - SAIR\n";

        cout << "Digite a  opção:\n";
        cin >> op;

    } while (op != '4');

    delete aluno;
    return 0;
}

void ler_arquivo()
{
    Aluno aluno;
    ifstream file_in;
    char file[20];
    cout << "Digite o Nome do arquivo onde contem os dados dos alunos:\n";
    cin >> file;

    file_in.open(file);

    do
    {
        file_in >> aluno.matricula;
        file_in >> aluno.nome;
        file_in >> aluno.nota1;
        file_in >> aluno.nota2;
        file_in >> aluno.faltas;

    } while (!file_in.eof());

    cout << "Arquivo lido com sucesso\n";

    file_in.close();
}

double media(double nota1, double nota2)
{
    Aluno aluno;

    aluno.media = (nota1 + nota2) / 2;

    return aluno.media;
}

double frequencia(int faltas) //para ser aprovado tem que ter 81 pesença pra cima
{
    double frequente = (faltas * 100) / 108;

    return frequente;
}
