//aula pratica 16 - arquivos
//exercicio 1
#include <iostream>
#include <cmath>
#include <fstream>
#include <cstdlib>

using namespace std;

int main()
{
    system("clear");
    double x, y, z = 0;
    char name_file[20];

    cout << "Digite o nome do arquivo:\n";
    cin >> name_file;

    ifstream file_in;
    ofstream file_out;
    file_out.open("resulta.txt");
    file_in.open(name_file);

    file_out << "X\tY\tZ"<<endl; 
    do
    {
        file_in >> x >> y;
        z = sqrt(pow(x,2)+pow(y,2));
        file_out << x << "\t" << y << "\t" << z << endl;

    } while (!file_in.eof());

    cout << "Arquivo lido\n";

    file_out << x << '\t' << y << '\t' << z << endl; 

    cout << "Arquivo salvo com sucesso em: resuta.txt" << endl; 

    file_in.close();
    file_out.close();

    return 0;
}