//exercicio3
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int n;
    cout << "Digite um numero positivo não-nulo: \n";
    cin >> n;

    for (int i = 1; i <= n; i++)
    {
        for (int j = 0; j < (n - i) ; j++)
				cout << "  " ;
        
        for (int j = 1; j <= i ; j++)
        {
            cout << setw(2) << i << "  ";
            
        }

        cout << endl;
    }
    return 0;
}
