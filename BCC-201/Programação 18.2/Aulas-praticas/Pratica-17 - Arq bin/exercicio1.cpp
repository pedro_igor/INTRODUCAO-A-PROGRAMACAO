//aula pretica binario
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
using namespace std;

struct pontoIn
{
    float x, y;
};

struct pontoOut
{
    float x, y, z;
};

int main()
{
    //pontoIn inPonto;
    int pos = 0;

    ifstream bin_in;
    ofstream bin_out;

    system("clear");

    bin_in.open("pontos.bin", ios::binary);
    if (bin_in.fail())
    {
        cout << "Erro na abertura do arquivo";
        exit(1);
    }
    bin_in.seekg(0, ios::end);
    streampos tam = bin_in.tellg() / sizeof(pontoIn);
    bin_in.seekg(0, ios::beg);
    //cout << "Tamanho " << tam << endl;



    bin_out.open("resultado.bin", ios::binary);
    if (bin_out.fail())
    {
        cout << "Erro na abertura do arquivo";
        exit(1);
    }
    pontoIn *v = new pontoIn[tam];
    bin_in.read((char*)v, sizeof(pontoIn) * tam );


    for(int i = 0; i < tam; i++)
    {
        cout << v[i].x << " " << v[i].y << endl;
    }
    while (pos != -1)
    {
        system("clear");
        cout << "Qual é a  possição que quer calcular"
             << "\n digite -1 para sair" << endl;

        cin >> pos;
        pontoOut outPonto;
        outPonto.x = v[pos].x;
        outPonto.y = v[pos].y;

        outPonto.z = sqrt(pow(v[pos].x, 2) + pow(v[pos].y, 2));
        //cout << v[pos].x << " " << v[pos].y <<endl;
        bin_out.write((char *)&outPonto, sizeof(pontoOut));
    }

    bin_in.close();
    bin_out.close();

    return 0;
}