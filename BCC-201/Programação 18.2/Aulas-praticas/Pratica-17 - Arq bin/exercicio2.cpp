//aula pretica binario
#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

struct Dados
{
    int dia, mes, ano;
    double temp_min, temp_max;
};

int main()
{
    system("clear");
    Dados *dados;
    double temp_max = 0;
    double temp_min = 0;
    double media_max = 0;
    double media_min = 0;
    ifstream bin;
    bin.open("temperaturas.dat", ios::binary);
    if (bin.fail())
    {
        cout << "Erro na abertura do arquivo";
        exit(1);
    }
    bin.seekg(0, ios::end);
    streampos tam = bin.tellg() / sizeof(dados);
    bin.seekg(0, ios::beg);
    dados = new Dados[tam];
    bin.read((char *)dados, sizeof(Dados) * tam);

    for (int i = 0; i < tam; i++)
    {
        temp_max += dados[i].temp_max;
        temp_min += dados[i].temp_min;
    }

    media_max = temp_max / tam;
    media_min = temp_min / tam;

    cout << media_max << endl;
    cout << media_min << endl;

    bin.close();
    return 0;
}
