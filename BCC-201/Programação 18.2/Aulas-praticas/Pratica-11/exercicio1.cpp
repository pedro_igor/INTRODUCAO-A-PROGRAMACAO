//aula pratica matrizes exercicio 1
#include<iostream>
using namespace std;

#define NUM 10

//função ler matriz
void lermatriz(int linha,int coluna,int matriz[NUM][NUM])
{

  for(int i = 0; i < linha; i++)
  {
    for(int j = 0; j < coluna; j++)
    {
      cin >> matriz[i][j];
    }
  }
}//fim da função

//funçao para imprimir matriz
void printmatriz(int linha, int coluna, int matriz[NUM][NUM])
{
    for(int i = 0; i < linha; i++)
    {
        for(int j = 0; j < coluna; j++)
        {
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
}//fim da função 

//função para multiplicação das matrizes A x B
void multiMatizes(int linha, int coluna, int a[NUM][NUM], int b[NUM][NUM], int c[NUM][NUM],int p)
{

    for(int i = 0; i < linha; i++)
    {
        for(int j = 0; j < coluna; j++)
        {
            c[i][j] = 0;
            for(int k = 0; k < p; k++)
            {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    
    }
}//fim da função 

int main()

{
    int a[NUM][NUM], b[NUM][NUM], c[NUM][NUM];
    int linhaA, colunaA, linhaB, colunaB;

    //ler e  imprime a matriz A
    cout << "DIGITE AS DIMENÇÕES DA MATRIZ A: ";
    cin >> linhaA >> colunaA;
    lermatriz(linhaA, colunaA, a);
    printmatriz(linhaA, colunaA, a);

    cout << endl;

    //ler e imprime a matriz B
    cout << "DIGITE A DIMENÇÕES DA MATRIZ B: ";
    cin >> linhaB >> colunaB;
    lermatriz(linhaB, colunaB, b);
    printmatriz(linhaB, colunaB, b);

    cout << endl;

    if(colunaA == linhaB)
    {
        multiMatizes(linhaA, colunaB, a, b, c, linhaB);//linhaB seria o p
        printmatriz(linhaA, colunaB, c);
    }
    
    cout << endl; 
    return 0;
}