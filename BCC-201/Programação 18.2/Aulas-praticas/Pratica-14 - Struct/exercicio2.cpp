//aula pratica de struct - exercicio 2
#include <iostream>
#include <cmath>
using namespace std;


struct Contato
{
    int ddd;
    char telefone[10];
};

struct Dados
{
    char cpf[12];
    double altura;
    double peso;
    char sexo;
    double imc;
    Contato tel;

};

int main()
{
    
    int n;
    cout << "Digite o numero de pessoas:\n";
    cin >> n;

    Dados *data = new Dados[n];
    
    int j;

    for (int i = 0; i < n; i++)
    {
        cout << "Digite o seu CPF:\n";
        cin >> data[i].cpf;

        cout << "Digite a Altura:\n";
        cin >> data[i].altura;

        cout << "Digite o peso:\n";
        cin >> data[i].peso;

        cout << "Digite o sexo M (para masculino) ou F (para feminino):\n";
        cin >> data[i].sexo;

        cout << "Digite o DDD:\n";
        cin >> data[i].tel.ddd;

        cout << "Digite o  numero telefone ou o celular:\n";
        cin >> data[i].tel.telefone;

        cout << endl;

        data[i].imc = data[i].peso / pow(data[i].altura, 2);
    }

    for (j = 0; j < n; j++)
    {
        if (data[j].imc >= 25)
        {
            cout << "Telefone: "
                 << "( " << data[j].tel.ddd << " ) " << data[j].tel.telefone << endl;
        }
    }
    delete data;

    return 0;
}