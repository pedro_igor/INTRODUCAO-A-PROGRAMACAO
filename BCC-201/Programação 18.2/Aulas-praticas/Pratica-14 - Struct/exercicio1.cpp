//aula pratica de struct - exercicio 1
#include <iostream>
#include <cmath>
#include <cstring>

#define NUM_PESSOA 20
using namespace std;

struct Dados
{
    char cpf[12];
    double altura;
    double peso;
    char sexo;
    double imc;
};

int main()
{
    Dados data[NUM_PESSOA];
    int j;

    for (int i = 0; i < NUM_PESSOA; i++)
    {
        cout << "Digite o seu CPF:\n";
        cin >> data[i].cpf;

        cout << "Digite a Altura:\n";
        cin >> data[i].altura;

        cout << "Digite o peso:\n";
        cin >> data[i].peso;

        cout << "Digite o sexo M (para masculino) ou F (para feminino):\n";
        cin >> data[i].sexo;
        cout << endl;

        data[i].imc = data[i].peso / pow(data[i].altura, 2);
    }

    cout << "Digite o CPF no qual quer calcular o IMC:\n";
    char cpf[12];
    cin >> cpf;

    for (j = 0; j < NUM_PESSOA; j++)
    {
        if (strcmp(cpf, data[j].cpf) == 0)
        {
            cout << "O IMC (Indice de Massa Corporal) é de: " << data[j].imc << endl;
            break;
        }
    }

    return 0;
}