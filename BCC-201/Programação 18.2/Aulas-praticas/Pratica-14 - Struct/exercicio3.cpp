//aula pratica de struct - exercicio 3
#include <iostream>
using namespace std;

struct hab
{
    int idade;
    char sexo;
    int num_filhos;
    double salario;
};

double media(double salarios, int num_pessoas);

int main()
{
    int j;
    int num;
    double soma = 0;
    cout << "Quantas pessoas:\n";
    cin >> num;

    hab *dados = new hab[num];

    for (int i = 0; i < num; i++)
    {
        cout << "\nEntrevistado : " << i + 1 << endl;
        cout << "Digite a idade: \n";
        cin >> dados[i].idade;

        cout << "Digite o sexo M (para masculino) ou F (para feminino):\n";
        cin >> dados[i].sexo;

        cout << "Digite o numero de filhos:\n";
        cin >> dados[i].num_filhos;

        cout << "Digite o Salario:\n";
        cin >> dados[i].salario;
    }

    for (j = 0; j < num; j++)
    {
        soma += dados[j].salario;
    }

    cout << "\nA media dos salarios dos habitantes é: " << media(soma, num) << endl;

    delete dados;

    return 0;
}

double media(double salarios, int num_pessoas)
{
    return (salarios / num_pessoas);
}
