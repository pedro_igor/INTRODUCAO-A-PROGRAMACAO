//conjectura de Collatz
#include <iostream>
using namespace std;

int main()
{
    int n, cont;

    cout << "SEQUENCIA DE COLLATZ\n"
         << "--------------------\n";

    cout << "Digite um numero nao negativo: ";
    cin >> n;

    while (n < 0)
    {
        cout << "Numero Invalido!";
        break;
    }
    while (n != 1)
    {
        if (n % 2 == 0)
        {

            n = n / 2;
        }
        else
        {

            n = 3 * n + 1;
        }
        cout << n << " ";
        cont++;
    }
    cout << "Interaçoes: " << cont << endl;

    return 0;
}