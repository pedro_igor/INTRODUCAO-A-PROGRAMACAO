//exercicio 1
#include <iostream>
using namespace std;

int main()
{

    double km, resul;
    int cout100 = 1.00, cout200 = 0.80, cout300 = 0.70;

    cout << "Digite quantos km  pecorrido:\n";
    cin >> km;

    if (km <= 100.0)
    {
        resul = cout100 * km;
    }
    if (km > 100.0 && km <= 300.0)
    {
        resul = cout200 * (100 - km) + 100 * cout100;
    }
    if (km > 300.0)
    {
        resul = cout300 * ((100 - km) + (200 - km)) + (100 * cout100 + 200 * cout200);
    }

    cout << "Total é de: " << resul << endl;

    return 0;
}