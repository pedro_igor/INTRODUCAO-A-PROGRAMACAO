//aula prtica 9- funçoes I
#include<iostream>
#include<cmath>
using namespace std;

int digitoFinais(int a, int b)
{
	int m;

	m = a % (int) pow(10, b);

	return m;
}

int contaDigito (int i)
{
	int count = 0;
	int mod = i;
	
	while(mod != 0 )
	{
		mod = mod / 10;
		count ++; 
	}  

	return count;
}

int main()
{
	int a, b, c;//'c' retorna o numero
	int n;//é o numero em que o usuaria quee descubrir quantos numero  há;

	do
	{
		cout << "Digite dois numeros: ";
		cin >> a >> b;

		n = contaDigito(b);
		c = digitoFinais(a,n);

		if(c == b)
		{
			cout << b << " Corresponde ao final de: " << a << endl;
		}
		else 
			cout << b << " Não corresponde ao final de: " << a << endl;
	}while(a != 0 && b != 0);

		
	return 0;
}

