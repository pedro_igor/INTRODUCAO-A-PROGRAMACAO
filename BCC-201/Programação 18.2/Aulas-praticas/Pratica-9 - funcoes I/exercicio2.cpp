//exercicio 2: funcoes
#include<iostream>
#include<cmath>
using namespace std;

double hipotenusa(double a)//a de aresta
{
	double d = a * sqrt(2);

	double hip = sqrt(pow(a, 2) + pow(d, 2));

	return hip;
}

int main()
{
	double a; //aresta

	cout << "Entre com a aresta do cubo: ";
	cin >> a;

	

	cout << "A HIPOTENUSA DO INTERIOR DO CUBO É: " << hipotenusa(a) << endl;
	

	return 0;
}