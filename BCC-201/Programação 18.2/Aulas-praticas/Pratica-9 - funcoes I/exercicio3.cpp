//exercicio 3 : funçoes I
#include<iostream>
#include<iomanip>
using namespace std; 

void converteHora(int totalSegundos,  int &horas, int &minutos, int &segundos)
{
	horas = totalSegundos / 3600;
	minutos = horas / 60;
	segundos = minutos;

}

int main()
{
	int segInicio, h, m, s;
	cout << "ENTRE COM OS SEGUNDOS: ";
	cin >> segInicio;

	converteHora(segInicio, h, m, s);

	cout << setfill('0') << setw(2) << h << " : " << setw(2) << m << " : " << setw(2)  << s << endl; 

	return 0;
}


