//exercicio 2
#include <iostream>
using namespace std;

int main()
{
	double nota = 1, soma = 0, media;
	double maior = 0, menor = 0;

	int cont = 0;
	while (nota > 0)
	{

		cout << "DIGITE UMA NOTA (ou uma nota negativa para sair): ";
		cin >> nota;
		if (nota >= 0 && nota <= 10.0)
		{
			soma = soma + nota;
			cont++;

			if (nota > maior)
				maior = nota;
			else if (nota < menor)
				menor = nota;
		}
	}
	media = soma / cont;
	cout << "A MEDIA DAS " << cont << " NOTAS É: " << media;
	cout << "\nA MAIOR NOTA: " << maior;
	cout << "\nA MENOR NOTA: " << menor;
	cout << endl;
	return 0;
}