// exercicio 1 
#include<iostream>
using namespace std;

int main()
{
	double numero;

	int positive = 0, negative = 0;
	while (numero != 0) {
		cout << "DIGTE UM N�MERO REAL (PARAR = 0): ";
		cin >> numero;
		
		if (numero > 0)
			positive++;
		else if (numero < 0)
			negative++;
	}
	cout << "QUANTIDADE DE POSITIVOS DIGITADOS: " << positive << endl;
	cout << "QUANTIDADE DE NEGATIVOS DIGITADOS: " << negative << endl;

	return 0;
}