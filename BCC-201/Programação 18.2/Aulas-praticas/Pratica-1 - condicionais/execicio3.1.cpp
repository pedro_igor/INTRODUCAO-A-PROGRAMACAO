//exercico 3
#include <iostream>
using namespace std;

int main()
{
    int cod; //código do cargo
    double salario, reajuste;

    cout << "DIGITE O CÓDIGO DO CARGO DO FUNCIONÁRIO:\n";
    cin >> cod;

    cout << "DIGITE O SALARIO DO FUNCIONÁRIO:\n";
    cin >> salario;

    switch (cod)
    {
    case 100:
        reajuste = salario + (salario * 0.03);
        cout << "\nO NOVO SALARIO DO FUNCIONÁRIO É DE: " << reajuste
             << "\n ANTIGO SALARIO:  " << salario;
        break;

    case 101:
        reajuste = salario + (salario * 0.05);
        cout << "\nO NOVO SALARIO DO FUNCIONÁRIO É DE: " << reajuste
             << "\nO ANTIGO SALARIO DO FUNCIONÁRIO É DE: " << salario;
        break;

    case 102:
        reajuste = salario + (salario * 0.075);
        cout << "\nNOVO SALARIO DO FUNCIONARIO É DE: " << reajuste
             << "\nO ANTIGO SALARIO DO FUNCIONARIO É DE: " << salario;
        break;

    case 201:
        reajuste = salario + (salario * 0.1);
        cout << "\nNOVO SALARIO DO FUNCIONARIO É DE: " << reajuste;
        cout << "\nO ANTIGO SALARIO DO FUNCIONARIO É DE: " << salario;
        break;

    default:
        reajuste = salario + (salario * 0.15);
        cout << "\nNOVO  SALARIO DO FUNCIONARIO É DE: " << reajuste;
        cout << "\nO ANTIGO SALARIO DO FUNCIONARIO É DE:" << salario;
    }
    cout << endl;.
    return 0;
}