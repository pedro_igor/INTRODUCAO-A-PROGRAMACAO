//exercico 2
#include<iostream>
using namespace std;

#define MAX 100

int main()
{
    int vet1[MAX];
    int vet2[MAX];
    int vet3[MAX * 2];
    
    int vetTam1;
    int vetTam2;

    int menor;
    int maior;

    cout << "Entre com o Tamanho do primeiro Vetor: ";
    cin >> vetTam1;

    cout << "vet1 = {";
    for(int i = 0; i < vetTam1; i++)
    {
        cin >> vet1[i];
    }
    cout << "} " << endl;

    cout << "Entre com o Tamanho do segundo Vetor: ";
    cin >> vetTam2;

    cout << "vet2 = {";
    for(int i = 0; i < vetTam2; i++)
    {
        cin >> vet2[i];
    }
    cout << "} " << endl;

    if(vetTam1 > vetTam2)
    {
        menor = vetTam2;
    }
    else
    {
        menor = vetTam1;
    }

    int i = 0;

    for(int j = 0; j < menor; j++)
    {
        vet3[i] = vet1[j];
        i++;
        vet3[i] = vet2[j];
        i++;
    }

    if(vetTam1 > vetTam2)
    {
        maior = vetTam1;

        for(int j = menor; j < maior; j++)
        {
            vet3[i] = vet1[j];
            i++;
        }

    }
    else 
    {
        maior = vetTam2;

        for(int j = menor; j < maior; j++)
        {
            vet3[i] = vet2[j];
            i++;
        }

    }


    cout << "vet3 = { ";
    for(int u = 0; u < vetTam1 + vetTam2 ; u++)
    {
        cout << vet3[u] << " ";
    }
    cout << " } " << endl;

    cout << endl;

    return 0;
}