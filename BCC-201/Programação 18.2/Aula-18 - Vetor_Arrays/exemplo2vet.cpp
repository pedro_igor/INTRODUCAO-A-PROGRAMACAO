//exemplo 2
//lerr 10 nmueros inteiros do teclados para depois impreimir os numeros na ordem
//inversa de leitura 
#include<iostream>
using namespace std;
#define TAM 10

int main()
{
    int numeros[TAM];

    //lendo os numeros
    for(int i = 0; i < TAM; i++)
    {
        cout << "Digite o " << i + 1 << " numero: ";
        cin >> numeros[i];
    }

    //imprimindo em ordem inversa
    for(int i = TAM - 1; i >= 0; i--)
    {
        cout << numeros[i] << " ";
    }

    cout << endl;
    return 0;
}
