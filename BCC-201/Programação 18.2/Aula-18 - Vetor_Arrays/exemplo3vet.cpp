//exemplo 3
//Ler 10 notas de alunos e imprimir quantas tem valor superior à média
#include<iostream>
using namespace std;
#define TAM 10

int main()
{
    double notas[TAM], soma = 0;

    //lendo as  notas 9 (e acumulando os valores para calcular a media)
    for(int i = 0; i < TAM; i++)
    {
        cout << "Digite a " << i + 1 << " nota: ";
        cin >> notas[i];
        soma += notas[i];
    }

    // calculando quantas notas sao maiores do que a media
    double media = soma / TAM;
    int contador = 0;
    for(int i = 0; i < TAM; i++)
    {
        if (notas[i] > media)
            contador ++;
    }

    cout << endl << contador << " notas superam a média " << endl;

    return 0;
}