//exercicio da ultima aula
#include<iostream>
using namespace std;

#define N 15

void troca(int &a, int &b)
{
    int aux = a;
    a = b;
    b = aux;
}

void ordena(int array[], int tamanho)
{
    bool trocou;
    do
    {
        trocou = false;
        for(int i = 0; i < tamanho - 1; i ++){
            if(array[i] > array[i+1]){
                troca(array[i], array[i + 1]);
                trocou = true;
            }
        }
    } while (trocou);
}

int main()
{
    int data[N] = {0, 9, 8, 21, 33, 1, -1, 10, 7, 11, 6, 2, 5, 4, 3 };
    ordena(data, N);

    for(int i = 0; i < N; i++)
        cout << data[i] << " ";
    cout << endl;

    return 0;
}