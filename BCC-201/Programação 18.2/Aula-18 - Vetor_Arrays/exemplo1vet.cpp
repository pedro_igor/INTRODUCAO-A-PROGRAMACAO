//exemplo 1 vetor
#include<iostream>
using namespace std;
#define TAM 5

int main()
{
    double notas[TAM];

    //lendo as notas
    for(int i = 0; i < TAM; i++)
    {
        cout << "Digite a " << i + 1 << " a nota: ";
        cin >> notas[i];
    }

    //obitendo  maior nota
    double maiorNota = 0;
    for(int i = 0; i < TAM; i++)
    {
        if(notas[i] > maiorNota)
        {
            maiorNota = notas[i];
        }
    }

    cout << "Maior nota: " << maiorNota << endl;

    return 0;
}