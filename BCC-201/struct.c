#include <stdio.h>
#include <stdlib.h>

typedef struct prod
{
	char nome[100];
	double preco;
	int cod;
}Prod;

void leitura_dados(Prod *dados, int *nro)
{
	dados = malloc(*nro * sizeof(int));

	for(int i = 0; i < *nro; i++)
	{
		puts("\n");
		printf("Nome do produto: ");
		fgets(dados[i].nome, 101, stdin);
		getchar();
		printf("Preço do produto: ");
		scanf("%lf", &dados[i].preco);
		//getchar();
		printf("Codigo do produto: ");
		scanf("%d", &dados[i].cod);

	}
}

int main()
{
	Prod *dados;
	int *nro;
	printf("Qual é quantidade de produto: ");
	scanf("%d", nro);

	leitura_dados(dados, nro);

	free(dados);
	return 0;	
}