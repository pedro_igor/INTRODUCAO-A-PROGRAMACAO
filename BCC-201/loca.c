#include <stdio.h>
#include <stdlib.h>


void ler_vetor(int *v, int tamanho);
int *inverso(int *v, int n);

int main()
{
	int tam;
	printf("Qual é o tamanho do vetor: ");
	scanf("%d", &tam);

	int *v;

	v = malloc(tam * sizeof(int)); 

	ler_vetor(v,tam);

	int *vet = inverso(v,tam);

	for(int i = 0; i < tam; i++)
	{
		printf("%d ", vet[i]);
	}

	puts("\n");

	free(vet);
	free(v);

	return 0;
}

void ler_vetor(int *v, int tamanho)
{
	for(int i =0; i < tamanho; i++)
	{
		scanf("%d", &v[i]);
	}
}

int *inverso(int *v, int n)
{
	int *inverso;
	inverso = malloc(n * sizeof(int));
	
	for(int i = n - 1, j = 0; i >= 0; i--, j++)
	{
		inverso[j] = v[i]; 
	}

	return inverso;
}