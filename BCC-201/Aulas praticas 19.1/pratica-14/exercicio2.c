#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    char nome[50];
    double nota;
    int frequencia;
} Alunos;

Alunos* read_file(FILE *arquivo, int *nro_alunos);

int main()
{
    int nro_alunos;             //numeros de alunos
    Alunos *alunos = NULL;                //vetor do tipo alunos
    double media_notas = 0;      // media da notas
    double media_frequencia = 0; //media da frequencia
    double percentual_apro = 0;  //percentual de alunos aprovados
    char name_file[100];
    FILE *arquivo;   

    printf("Digite o nome do arquivo: ");
    scanf("%s", name_file);

    arquivo = fopen(name_file, "rb");
    if(arquivo == NULL)
    {
        printf("Erro na abertura do arquivo.\n");
        exit(1);
    }
    else
    {
        alunos = read_file(arquivo, &nro_alunos);
        printf("%d", nro_alunos);
        printf("\n");
        fflush(stdout);
        
        for (int i = 0; i < nro_alunos; i++)
        {    
            media_notas += alunos[i].nota;
        }

        printf("nro"); //teste
        fflush(stdout);
        for (int i = 0; i < nro_alunos; i++)
        {
            media_frequencia += alunos[i].frequencia;
        }

        printf("Nota média: %.1lf\n", media_notas / nro_alunos);
        printf("Frequencia média: %.0lf%%\n", (media_frequencia / nro_alunos) * 100);

        fflush(stdout);
        for (int i = 0; i < nro_alunos; i++)
        {
            if (alunos[i].frequencia > 75 && alunos[i].nota > 6.0)
            {
                percentual_apro++;
            }
        }

        printf("Pecentual de aprovação: %.0lf\n\n", (percentual_apro / nro_alunos) * 100);

        printf("Nomes dos alunos com notas acima da media: \n");
        for (int i = 0; i < nro_alunos; i++)
        {
            if (alunos[i].nota > media_notas)
            {
                printf("%s\n", alunos[i].nome);
            }
        }
    }

    free(alunos);
    fclose(arquivo);
    return 0;
}

Alunos* read_file(FILE *arquivo, int *nro_alunos)
{
    Alunos *alunos = NULL;
    fread(nro_alunos, sizeof(int), 1, arquivo);
    fflush(stdout);

    alunos = calloc(*nro_alunos, sizeof(Alunos));
    fflush(stdout);

    fread(alunos, sizeof(Alunos), *nro_alunos, arquivo);

    for(int i = 0; i < *nro_alunos; i++)
    {
        printf("Nome: ");
        printf("%s", alunos[i].nome);
        printf("\n");
        //getchar(); 
        fflush(stdout);       
    }

    fflush(stdout);
    return alunos;
}
