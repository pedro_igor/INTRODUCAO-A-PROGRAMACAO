#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct 
{
    char nome[50];
    double nota;
    int frequencia;
}Aluno;

void write_file(FILE *arquivo, Aluno *alunos, int nro_alunos);
void ler_dados(Aluno *dados, int nro_alunos);

int main()
{
    Aluno *alunos;
    FILE *arquivo;
    int nro_alunos;
    char name_file[50];

    printf("Qual é o nome do arquivo: ");
    scanf("%s", name_file);

    printf("\n Qual é a quantidade de alunos: ");
    scanf("%d", &nro_alunos);

    getchar();

    arquivo = fopen(name_file, "wb");
    if(arquivo == NULL)
    {
        printf("Erro na abertura do arquivo.\n");
        exit(1);
    }
    else
    {
        alunos = calloc(nro_alunos, sizeof(Aluno));

        puts("\n");

        ler_dados(alunos, nro_alunos);
        write_file(arquivo, alunos, nro_alunos);
    }
    
    free(alunos);
    fclose(arquivo);

    return 0;
}

void write_file(FILE *arquivo, Aluno *alunos, int nro_alunos)
{
    fwrite(&nro_alunos, sizeof(int),1, arquivo);

    fwrite(alunos, sizeof(Aluno), nro_alunos, arquivo);
}

void ler_dados(Aluno *dados, int nro_alunos)
{
    for(int i = 0; i < nro_alunos; i++)
    {
        printf("Digite o nome do aluno: ");
        fgets(dados[i].nome, 51, stdin);
        dados[i].nome[strlen(dados[i].nome) - 1] = '\0';
        getc(stdout);
        //fflush(stdin);
        printf("Digite a nota de %s: ", dados[i].nome);
        scanf("%lf", &dados[i].nota);

        printf("Digite a frequencia de %s em (%%): ", dados[i].nome);
        scanf("%d", &dados[i].frequencia);
        getchar();        
    }
}

