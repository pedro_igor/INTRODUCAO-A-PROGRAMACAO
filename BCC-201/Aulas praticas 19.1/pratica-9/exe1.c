/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1

#include <stdio.h>
#define TAM 10

int main()
{
    int vet[10];

    vet[0] = 1;
    vet[1] = 1;

    for(int i = 2; i < TAM; i++)
    {
        vet[i] = vet[i -1] + vet[i -2];
    }

    for(int j = 0; j < TAM; j++)
    {
        printf("%d ", vet[j]);
    }

    printf("\n");

    return 0;
}

