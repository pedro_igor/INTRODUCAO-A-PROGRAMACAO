/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 4
#include <stdio.h>
#include <stdlib.h>

#define MAX 5

int main()
{
    double vetA[MAX];
    double vetB[MAX];

    system("reset");

    for(int i = 0; i < MAX; i++)
    {
        printf("Entre com a 1ª nota do aluno %d: ", i + 1);
        scanf("%lf", &vetA[i]);
    }

    for(int i = 0; i < MAX; i++)
    {
        printf("Entre com a 2ª nota do aluno %d: ", i + 1);
        scanf("%lf", &vetB[i]);
    }

    for(int i = 0; i < MAX; i++)
    {
        if(((vetA[i] + vetB[i])/2) >= 60)
        {
            printf("Aluno: %d foi com nota: %.2lf\n",i +1,(vetA[i] + vetB[i])/2);
        }
        else
        {
            printf("Aluno %d nao foi Aprovado\n", i + 1);
        }
    }

    printf("\n");

    return 0;
}