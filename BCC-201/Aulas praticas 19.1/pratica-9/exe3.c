/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 3
#include <stdio.h>

#define TAM 50

int main()
{
    int vet[TAM];
    int nro;
    int n;

    printf("Usuario digite o numro de posiçoes do vetor: ");
    scanf("%d", &nro);


    for (int i = 0; i < nro; i++)
    {
        printf("Digite o numero na posição %d: ", i + 1);
        scanf("%d", &n);
        vet[i] = n;
    }

    //imprime a barra de asteristico
    for(int i =0; i < nro; i++)
    {
        for(int j = 0; j < vet[i];  j++)
        {
            printf("* ");
        }
        printf("\n");
    }

    return 0;
}