/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
    system("clear");
    printf("nro\tquad\tcubo\n");
    printf("%d\t%.0lf\t%.0lf\n", 0, pow(0, 2), pow(0, 3));
    printf("%d\t%.0lf\t%.0lf\n", 1, pow(1, 2), pow(1, 3));
    printf("%d\t%.0lf\t%.0lf\n", 2, pow(2, 2), pow(2, 3));
    printf("%d\t%.0lf\t%.0lf\n", 3, pow(3, 2), pow(3, 3));
    printf("%d\t%.0lf\t%.0lf\n", 4, pow(4, 2), pow(4, 3));
    printf("%d\t%.0lf\t%.0lf\n", 5, pow(5, 2), pow(6, 3));
    printf("%d\t%.0lf\t%.0lf\n", 7, pow(7, 2), pow(7, 3));
    printf("%d\t%.0lf\t%.0lf\n", 8, pow(8, 2), pow(8, 3));
    printf("%d\t%.0lf\t%.0lf\n", 9, pow(9, 2), pow(9, 3));
    printf("%d\t%.0lf\t%.0lf\n", 10, pow(10, 2), pow(10, 3));

    return 0;
}
