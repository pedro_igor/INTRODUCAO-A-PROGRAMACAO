/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include <stdio.h>
#include <math.h>

int main()
{
    int nro;
    double solucao;
    printf("Digite o valor de n (inteiro entre 1 e 5): ");
    scanf("%d", &nro);

    if (nro < 1 || nro > 5)
    {
        printf("ERRO\n");
    }
    else
    {
        if (nro == 1)
        {
            solucao = 1 / pow(2, 0);
        }
        else if (nro == 2)
        {
            solucao = 1 / pow(2, 0) + 1 / pow(2, 1);
        }
        else if (nro == 3)
        {
            solucao = (1 / pow(2, 0)) + (1 / pow(2, 1)) + (1 / pow(2, 2));
        }
        else if (nro == 4)
        {
            solucao = (1 / pow(2, 0)) + (1 / pow(2, 1)) + (1 / pow(2, 2)) + (1 / pow(2, 3));
        }
        else if (nro == 5)
        {
            solucao = (1 / pow(2, 0)) + (1 / pow(2, 1)) + (1 / pow(2, 2)) + (1 / pow(2, 3)) + (1 / pow(2, 4));
        }
    }

    printf("Resultado: %lf\n", solucao);

    return 0;
}