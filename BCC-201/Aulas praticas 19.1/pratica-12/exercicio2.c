/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct
{
    char nome[100];
    double nota;
    int frequencia;
} Dados;

void write_file(FILE *arquivo, Dados *dados, int nro_alunos);

int main()
{
    Dados *dados;
    FILE *arquivo;
    int nro_aluno;
    char name_file[100];

    printf("Qual é o nome do arquivo: ");
    scanf("%s", name_file);

    printf("\nQual é o numero de alunos: ");
    scanf("%d", &nro_aluno);

    getchar();
    arquivo = fopen(name_file, "w");
    if (arquivo == NULL)
    {
        printf("ERRO NA ABERTURA DO ARQUIVO!\n");
        exit(1);
    }
    else
    {
        dados = malloc(nro_aluno * sizeof(dados));

        puts("\n");
        for (int i = 0; i < nro_aluno; i++)
        {
            printf("Digite o nome do aluno: ");
            fgets(dados[i].nome, 101, stdin);
            dados[i].nome[strlen(dados[i].nome) - 1] = '\0';
            getc(stdout);
            //fflush(stdin);
            printf("Digite a nota de %s: ", dados[i].nome);
            scanf("%lf", &dados[i].nota);

            printf("Digite a frequencia de %s em (%%): ", dados[i].nome);
            scanf("%d", &dados[i].frequencia);
            getchar();
        }

        write_file(arquivo,dados,nro_aluno);
    }

    fclose(arquivo);
    return 0;
}

void write_file(FILE *arquivo, Dados *dados, int nro_alunos)
{
    fprintf(arquivo, "%d\n", nro_alunos);

    for (int i = 0; i < nro_alunos; i++)
    {
        fprintf(arquivo, "%s %.2lf %d \n", dados[i].nome, dados[i].nota, dados[i].frequencia);
    }
}