/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    char nome_food[100]; //nome da comida
    double peso;         //em grams
    double calorias;     //quantidade de caloria que está contido no alimento
    double cal_gra;      //calorias por gramas
} Dieta;

Dieta *read_file(Dieta *v, FILE *arquivo, int *m);

int main()
{
    Dieta *dieta = NULL;         //vetor do tipo Dieta
    double cal_gra = 0.0; // variavel do dipo dieta para amarzenar cal/g
    FILE *arquivo;        //pontero para o arquivo
    char name_file[100];  // nome do arquivo
    int *tam = 0;              // onde vai alocar dentro da funçao 'read_file'

    printf("Digite o nome do arquivo: ");
    scanf("%s", name_file);

    //printf("-1\n");
    arquivo = fopen(name_file, "r");
    if ( arquivo== NULL)
    {
        printf("ERRO NA ABERTURA DO ARQUIVO!\n");
        exit(1);
    }
    else
    {
        printf("0");//teste
        fflush(stdout);
        dieta = read_file(dieta,arquivo,tam);
        printf("10");//teste
        fflush(stdout);
        for (int i = 0; i < *tam; i++)
        {
            dieta[i].cal_gra = dieta[i].calorias / dieta[i].peso;
        }

        printf("1");//teste
        fflush(stdout);
        for (int i = 0; i < *tam; i++)
        {
            if (dieta[i].cal_gra > cal_gra)
            {
                cal_gra = dieta[i].cal_gra;
            }
        }

        printf("2");//teste

        printf("Alimento        cal/grama\n");
        printf("--------------------------\n");
        for (int i = 0; i < *tam; i++)
        {
            printf("%s\t%lf\n", dieta[i].nome_food, dieta[i].cal_gra);
        }
    }

    free(dieta); //libera o vetor do tipo 'Dieta'
    fclose(arquivo); //fecha o arquivo
    return 0;
}

Dieta *read_file(Dieta *v, FILE *arquivo, int *m)
{
    printf("20");//teste
    fflush(stdout);
    fscanf(arquivo, "%d", m);
    printf("%d",*m);//teste

    printf("3");//teste
    fflush(stdout);
    v = malloc((*m )* sizeof(Dieta));

    for (int i = 0; i < *m; i++)
    {
        fscanf(arquivo, "%s", v[i].nome_food);
        fscanf(arquivo, "%lf", &v[i].peso);
        fscanf(arquivo, "%lf", &v[i].calorias);
    }

    return v;
}