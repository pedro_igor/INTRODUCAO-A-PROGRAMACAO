/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 3
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    char nome[100];
    double nota;
    int frequencia;
} Dados;

void read_file(FILE *arquivo, Dados *dados, int *nro_alunos);

int main()
{
    int nro_alunos;             //numeros de alunos
    Dados *dados;                //vetor do tipo dados
    double media_notas = 0;      // media da notas
    double media_frequencia = 0; //media da frequencia
    double percentual_apro = 0;  //percentual de alunos aprovados
    char name_file[100];
    FILE *arquivo;

    printf("Digite o nome do arquivo: ");
    scanf("%s", name_file);

    arquivo = fopen(name_file, "r");
    if (arquivo == NULL)
    {
        printf("ERRO NA ABERTURA DO ARQUIVO!\n");
        exit(1);
    }
    else
    {
        //printf("nro"); teste
        read_file(arquivo, dados, &nro_alunos);
        printf("%d", nro_alunos);
        fflush(stdout);
        
        for (int i = 0; i < nro_alunos; i++)
        {
            media_notas += dados[i].nota;
        }

        printf("nro"); //teste
        fflush(stdout);
        for (int i = 0; i < nro_alunos; i++)
        {
            media_frequencia += dados[i].frequencia;
        }

        printf("Nota média: %.1lf\n", media_notas / nro_alunos);
        printf("Frequencia média: %.0lf%%\n", (media_frequencia / nro_alunos) * 100);

        fflush(stdout);
        for (int i = 0; i < nro_alunos; i++)
        {
            if (dados[i].frequencia > 75 && dados[i].nota > 6.0)
            {
                percentual_apro++;
            }
        }

        printf("Pecentual de aprovação: %.0lf\n\n", (percentual_apro / nro_alunos) * 100);

        printf("Nomes dos alunos com notas acima da media: \n");
        for (int i = 0; i < nro_alunos; i++)
        {
            if (dados[i].nota > media_notas)
            {
                printf("%s\n", dados[i].nome);
            }
        }
    }
    free(dados);
    fclose(arquivo);

    return 0;
}

void read_file(FILE *arquivo, Dados *dados, int *nro_alunos)
{

    fscanf(arquivo, "%d", nro_alunos);
    printf("%d", *nro_alunos);//teste
    fflush(stdout);

    dados = malloc((*nro_alunos) * sizeof(Dados));
    printf("hi\n");//teste
    fflush(stdout);
    for (int i = 0; i < *nro_alunos; i++)
    {
        fscanf(arquivo, "%s %lf %d", dados[i].nome, &dados[i].nota, &dados[i].frequencia);
    }

    printf("hi2\n");//teste
    fflush(stdout);
}