#include<stdio.h>

int main()
{
    printf("Movimento de um Bispo no Xadrez!\n\n");
    int linha, coluna;

    printf("Digite a linha em que o bisbo se encontra: ");
    scanf("%d", &linha);

    printf("Digite a coluna em que o Bispo se encontra: ");
    scanf("%d", &coluna);

    printf("\nMovimentos possiveis: \n");

    printf("    1  2  3  4  5  6  7  8 \n");
    printf("   ------------------------\n");

    int n = 1;

    for(int l = 1; l <= 8; l++ )
    {
        printf("%d |", l+1);
        for(int c = 1; c <= 8; c++)
        {
            if(linha - l == coluna - c)
            {
                printf(" x ");
            }
            else if(-(linha -l) == coluna -c)
            {
                printf(" x ");
            }
            else
            {
                printf(" - ");
            }
            
        }
        printf("\n");
    }

    return 0;
}