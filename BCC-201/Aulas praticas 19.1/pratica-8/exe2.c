/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include <stdio.h>

//primos : 2, 3, 5, 7, 11, 13, 17, 19

int ehprimo(int n);

int main()
{
    int n;

    printf("Digite um numero: ");
    scanf("%d", &n);

    int resul = ehprimo(n);

    if (resul == 0)
    {
        printf("%d não é um número primo\n", n);
    }
    else
    {
        printf("%d  é um número primo\n", n);
    }

    return 0;
}

int ehprimo(int n)
{
    int i;
    if (n % 2 == 0)
    {
        return 0;
    }
    else
    {
        for (i = 2; i < n; i++)
        {
            if (n % i == 0)
            {
                return 0;
            }
        }
    }
    return 1;
}