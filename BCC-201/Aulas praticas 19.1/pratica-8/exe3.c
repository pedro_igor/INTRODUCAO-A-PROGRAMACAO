/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 3
#include <stdio.h>

int main()
{
    int x, y;
    int i;
    printf("Digite os valores de x e y: ");
    scanf("%d %d", &x, &y);

    if (x % 2 != 0 || y % 2 != 0)
    {
        printf("Erro: Apenas números pares são aceitos.\n");
    }
    else if (y < x)
    {
        printf("Erro: x deve ser menor que y.\n");
    }
    else
    {
        for (i = 0; i < (y - x) / 2 + 1; i++)
        {
            for (int j = i; j < (y - x) / 2; j++)
                printf(" ");

            for (int j = 0; j <= x + (i * 2); j++)
            {
                printf("*");
            }

            printf("\n");
        }
    }

    return 0;
}