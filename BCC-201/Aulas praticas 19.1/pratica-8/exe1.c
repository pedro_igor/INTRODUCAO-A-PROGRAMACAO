/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1
#include <stdio.h>

long long fat(int n);

int main()
{
    int n;
    printf("Digite o valor de n: ");
    scanf("%d", &n);

    printf("%d! = %lld\n", n, fat(n));//%lld long long

    return 0;
}

long long fat(int n)
{
    long long resul = 1;
    for (long long i = n; i >= 1; i--)
        resul *= i;

    return resul;
}