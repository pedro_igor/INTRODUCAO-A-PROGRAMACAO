/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include <stdio.h>

typedef struct Racional
{
    int numerador;
    int denominador;
} Mdc;

int mdc(int x, int y);
void troca(int *x, int *y);
int equal(int x, int x2, Mdc mdc1, Mdc mdc2);

int main()
{
    Mdc mdc1;
    Mdc mdc2;

    printf("Digite numerador e denominador de r1: ");
    scanf("%d %d", &mdc1.numerador, &mdc1.denominador);

    printf("Digite numeredor e denominador de r2: ");
    scanf("%d %d", &mdc2.numerador, &mdc2.denominador);

    int x = mdc(mdc1.numerador, mdc1.denominador);
    int x2 = mdc(mdc2.numerador, mdc2.denominador);

    
    if (equal(x,x2, mdc1, mdc2))
    {
        printf("r1 e r2 são iguais!\n");
    }
    else
    {
        printf("r1 e r2 são diferentes!\n");
    }

    return 0;
}

int mdc(int x, int y)
{
    if (x < y)
        troca(&x, &y);

    int resto;

    do
    {
        resto = x % y;
        x = y;
        y = resto;
    } while (resto != 0);

    return x;
}

void troca(int *x, int *y)
{
    int aux = *x;
    *x = *y;
    *y = aux;
}

int equal(int x, int x2, Mdc mdc1, Mdc mdc2)
{
    int y, y1;
    int z, z1;

    y = mdc1.numerador / x;
    z = mdc1.denominador / x;
    y1 = mdc2.numerador / x2;
    z1 = mdc2.denominador / x2;
    if (y == y1 && z == z1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}