#include <stdio.h>

typedef struct
{
  double real;
  double imaginario;
}Complexo;

//funçao que retorna x + y
Complexo somaComplexo(Complexo x, Complexo y);

//função que retorna x - y
Complexo subComplexo(Complexo x, Complexo y);

//funçao que retorna  x * y
Complexo multComplexo(Complexo x, Complexo y);

int main()
{
    Complexo x;
    Complexo y;
    Complexo resultado;
    char operacao;

    printf("Calculadora de dois nímeros complexos!\n\n");

    printf("Digite os valore de a e b  (x = a + bi): ");
    scanf("%lf %lf", &x.real, &x.imaginario);

    printf("Digite os valores de c e d (y = c + di): ");
    scanf("%lf %lf", &y.real, &y.imaginario);

    getchar();

    printf("Digite as operações (+, - ou *): ");
    scanf("%c", &operacao);

    switch(operacao)
    {
        case '+':
               resultado = somaComplexo(x, y);
            break;

        case '-':
                resultado = subComplexo(x, y);
            break;

        case '*':
                resultado = multComplexo(x, y);
            break;

        default:
            printf("Operação invalida\n");
            break;
    }

    printf("Resultado: %lf\n", resultado);

    return 0;
};

Complexo somaComplexo(Complexo x, Complexo y)
{
    printf("%lf\n", ((x.real + y.real) +(x.imaginario +y.imaginario)));
    //return  Complexo = ;
}

Complexo subComplexo(Complexo x, Complexo y)
{
    printf("%lf\n", ((x.real - y.real) + (x.imaginario + y.imaginario)));
    //return Complexo = ((x.real - y.real) + (x.imaginario + y.imaginario));
}

Complexo multComplexo(Complexo x, Complexo y)
{
    printf("%lf",((x.real * x.imaginario - y.real * y.imaginario) + (x.real * x.imaginario + y.real * y.imaginario)));
    //return  Complexo = ((x.real * x.imaginario - y.real * y.imaginario) + (x.real * x.imaginario + y.real * y.imaginario));
}