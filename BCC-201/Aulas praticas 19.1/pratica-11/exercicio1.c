/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1
#include <stdio.h>
#include <stdlib.h>

#define M 10
#define N 100

typedef struct dados
{
    char nome[100];
    int matricula;
    double notas[M];
} Aluno;

Aluno encontraAluno(Aluno aluno[], int nAluno, int matricula)
{
    Aluno a;
    a.matricula = -1;
    for (int i = 0; i < nAluno; i++)
    {
        if (matricula == aluno[i].matricula)
        {
            return aluno[i];
        }
    }

    return a;
}

int main()
{
    Aluno aluno[N];
    double media;
    int k = 0;

    int matricula;
    int nro_alunos;
    int nro_notas;

    do
    {
        printf("Digite o Número de Alunos: ");
        scanf("%d", &nro_alunos);
    } while (nro_alunos > 100 || nro_alunos < 1);

    do
    {
        printf("Digite o Números de notas:");
        scanf("%d", &nro_notas);
    } while (nro_notas < 1 || nro_notas > 10);

    puts("\n");

    for (int i = 0; i < nro_alunos; i++)
    {
        getchar();
        printf("Digite o nome do aluno %d: ", i + 1);
        fgets(aluno[i].nome, 101, stdin);

        printf("Qual é a matricula: ");
        scanf("%d", &aluno[i].matricula);

        printf("Quais são as notas:\n");
        for (int j = 0; j < nro_notas; j++)
        {
            printf(" %dª nota do aluno: ", j + 1);
            scanf("%lf", &aluno[i].notas[j]);

            media += aluno[i].notas[j];
        }
        k++;
        puts("\n");
    }


    printf("AS medias:\n");
    printf("MEDIA: %.2lf\n", media / (nro_alunos * nro_notas));

    puts("\n");

    printf("Digite a matricula: ");
    scanf("%d", &matricula);
    puts("\n");

    Aluno aluno1 = encontraAluno(aluno, nro_alunos, matricula);//chamada de função
    
    double media2;
    if (aluno1.matricula == -1)
    {
        printf("Aluno não encontrado\n");
    }
    else
    {
        printf("Nome: %s\n", aluno1.nome);
        for (int i = 0; i < nro_notas; i++)
        {
            media2 += aluno1.notas[i];
        }
        printf("A media desse aluno é: %.2lf.\n", media2 / nro_alunos);
        puts("\n");
    }

    return 0;
}