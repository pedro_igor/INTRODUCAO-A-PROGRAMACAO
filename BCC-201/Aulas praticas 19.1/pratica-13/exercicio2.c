/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include <stdio.h>
#include <stdlib.h>

int multiplicaMatrizes(int *** R, int ** A, int m, int n, int ** B, int p, int q);
void read_matriz(int ***matriz, int linha, int coluna);
void printmatriz(int linha, int coluna, int **matriz);
int **desalocar(int m, int n, int **matriz);

int main()
{
    int **R, **A, **B;
    int linhaA, colunaA, linhaB, colunaB;
    int resultado;

    system("clear");
    //ler e imprime a matriz A
    printf("DIGITE AS DIMENSÕES DA MATRIZ A: ");
    scanf("%d %d", &linhaA, &colunaA);
    read_matriz(&A, linhaA, colunaA);
    printmatriz(linhaA, colunaA, A);  
    
    printf("\n");

    //ler e imprime a matriz B
    printf("DIGITE AS DIMENSÕES DA MATRIZ B: ");
    scanf("%d %d", &linhaB, &colunaB);
    read_matriz(&B, linhaB, colunaB);
    printmatriz(linhaB, colunaB, B); 


    printf("\n");
    fflush(stdout);
    resultado = multiplicaMatrizes(&R, A, linhaA, colunaA, B, linhaB, colunaB);
    if(resultado == 1)
    {
        printmatriz(linhaA, colunaB, R);
    }
    else
    {
        printf("Não foi possivel fazer a multiplicação da matriz.\n");
    }

    A = desalocar(linhaA, colunaA, A);
    B = desalocar(linhaB, colunaB, B);
    R = desalocar(linhaA, colunaB, R);

    return 0;
}
int multiplicaMatrizes(int *** R, int ** A, int m, int n, int ** B, int p, int q)
{
    if(n == p)// coluna da a, e linha da b
    {
        *R = malloc(n * sizeof(int*));

        for(int i = 0; i < n; i++)
        {
            (*R)[i] = calloc(q,sizeof(int));
        }

        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < q; j++)
            {
                for(int k = 0; k < p; k++)
                {
                    (*R)[i][j] +=  A[i][k] * B[k][j];
                }
            }
        }
        return 1;
    }
    return 0;
}

void read_matriz(int ***matriz, int linha, int coluna)
{
    int x;
    *matriz = calloc(linha , sizeof(int*));
    for(int i = 0; i < linha; i++)
    {
        (*matriz)[i] = calloc(coluna,sizeof(int));
    }

    for(int i = 0; i < linha; i++)
    {
        for(int j = 0; j < coluna; j++)
        {
            printf("Digite o valor da matriz na posição[%d][%d]: ", i + 1, j + 1);
            scanf("%d", &x);
            (*matriz)[i][j] = x;
            //printf("2");
        }
        //printf("1");
    }
}

//funçao para imprimir matriz
void printmatriz(int linha, int coluna, int **matriz)
{
    for (int i = 0; i < linha; i++)
    {
        for (int j = 0; j < coluna; j++)
        {
            printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
} //fim da função

int **desalocar(int m, int n, int **matriz)
{
    if(matriz == NULL)
    {
        return (NULL);
    }

    for(int i = 0; i < m; i++)
    {
        free(matriz[i]);
    }

    free(matriz);

    return (NULL);
}
