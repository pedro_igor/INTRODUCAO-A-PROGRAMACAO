//exercicio 1
#include <stdio.h>

int main()
{
    double x, y, resultado;
    char op;

    printf("\nDigite a operação: ");
    scanf("%c", &op);

    printf("\nDigite 1 numero que deseja fazer a operação: ");
    scanf("%lf", &x);

    printf("\nDigite 2 numero que deseja fazer a operação: ");
    scanf("%lf", &y);

    switch (op)
    {
        case '+':
            resultado = x + y;
            break;

        case '-':
            resultado = x - y;
            break;

        case '*':
            resultado = x * y;
            break;

        case '/':
            resultado = x / y;
            break;

        default:
            printf("ERRO!\n");
            break;
    }

    printf("Resultado: %.2lf %c %.2lf = %.2lf\n", x, op, y, resultado);

    return 0;
}