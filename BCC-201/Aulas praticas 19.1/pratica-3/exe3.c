/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 3
#include <stdio.h>

int main()
{
    char conceito;

    printf("Digite o conceito na diciplina BCC-201: ");
    scanf("%c", &conceito);

    if (conceito == 'A')
    {
        printf("Exelente\n");
    }
    else if (conceito == 'B')
    {
        printf("Otimo\n");
    }
    else if (conceito == 'C')
    {
        printf("Bom\n");
    }
    else if (conceito == 'D')
    {
        printf("Regular\n");
    }
    else if (conceito == 'E')
    {
        printf("Ruim\n");
    }
    else if (conceito == 'F')
    {
        printf("Nos vemos de novo no ano que vem...\n");
    }
    else
    {
        printf("ERRO: %cConceito invalido\n", 07);
    }

    return 0;
}