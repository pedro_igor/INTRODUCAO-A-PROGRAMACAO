/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 3
#include <stdio.h>
#include <math.h>

int main()
{
    double altura, peso;

    printf("Digite a altura: ");
    scanf("%lf", &altura);

    printf("Digite o peso: ");
    scanf("%lf", &peso);

    double imc = peso / pow(altura, 2);

    if (imc <= 18.5)
    {
        printf("Abaixo do peso\n");
    }
    else if (imc > 18.5 && imc <= 25)
    {
        printf("Peso Normal\n");
    }
    else if (imc > 25 && imc <= 30)
    {
        printf("Acima do peso\n");
    }
    else
    {
        printf("Obeso\n");
    }

    return 0;
}