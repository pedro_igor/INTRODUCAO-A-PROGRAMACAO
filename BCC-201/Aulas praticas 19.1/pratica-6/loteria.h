/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1
#ifndef LOTERIA_H
#define LOTERIA_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define PREMIO 100000.00

int sorteio();
void read(int *nro1, int *nro2, int *nro3);
int acertos(int *nro1, int *nro2, int *nro3);


#endif