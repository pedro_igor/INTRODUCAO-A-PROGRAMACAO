/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include "func_x_y.h"

int main()
{
    int x, y, f;

    system("clear");

    printf("DIGITE X E Y: ");
    scanf("%d %d", &x, &y);

    f = fXanadY(&x, &y);

    system("clear");

    printf("Valor de x: %d\n", x);
    printf("Valor de y: %d\n", y);
    printf("Valor de f: %d\n", f);

    return 0;
}

int fXanadY(int *x, int *y)
{
    int resultado;

    if (*x >= 0 && *y >= 0)
    {
        resultado = *x + *y;
    }
    else if (*x >= 0 && *y < 0)
    {
        resultado = *x + pow(*y, 2);
    }
    else if(*x < 0 && *y >= 0)
    {
        resultado = pow(*x,2) + *y;
    }
    else if(*x < 0 && *y < 0)
    {
        resultado = pow(*x, 2) + pow(*y, 2);
    }

    return resultado;
}