/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1
#include "loteria.h"

int main()
{
    int nro1, nro2, nro3;
    int acertos_nro;
    double premio;

    srand(time(NULL));

    system("clear");

    printf("SORTEIO DA LOTERIA\n\n");
    printf("Usuario digite três numeros: \n");

    read(&nro1, &nro2, &nro3);
    sorteio();

    acertos_nro = acertos(&nro1, &nro2, &nro3);

    switch (acertos_nro)
    {
        case 3:

            premio = PREMIO;

            break;

        case 2:

            premio = PREMIO * 0.01;

            break;

        case 1:

            premio = 1.00;
            break;

        case 0:

            premio = 0;

            break;
    }

    if (acertos_nro == 0)
    {
        printf("Tente novamente, Voce acertou nada\n");
    }
    else
    {
        printf("\nO SEU PREMIO FOI DE: %.2lf\n", premio);
    }

    return 0;
}

int sorteio()
{
    int nroAleatorio = (rand() % 30) + 1;
    return nroAleatorio;
}

void read(int *nro1, int *nro2, int *nro3)
{
    scanf("%d %d %d", nro1, nro2, nro3);
}

int acertos(int *nro1, int *nro2, int *nro3)
{
    int numero1 = sorteio();
    int numero2 = sorteio();
    int numero3 = sorteio();
    
    int cont = 0;

    if (*nro1 == numero1 || *nro1 == numero2 || *nro1 == numero3)
    {
        cont += 1;
    }

    if (*nro2 == numero1 || *nro2 == numero2 || *nro2 == numero3)
    {
        cont += 1;
    }

    if (*nro3 == numero1 || *nro3 == numero2 || *nro3 == numero3)
    {
        cont += 1;
    }

    return cont;
}

