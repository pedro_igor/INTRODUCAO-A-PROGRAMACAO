/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#ifndef FUNC_X_Y_C
#define FUNC_X_Y_C

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int fXanadY(int *x, int *y);

#endif