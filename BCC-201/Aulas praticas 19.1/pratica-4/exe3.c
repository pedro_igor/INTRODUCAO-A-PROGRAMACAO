/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercio 3
#include <stdio.h>

void consoante(char letter);

char letra;

int main()
{
    printf("Digite um Letra: ");
    scanf("%c", &letra);

    consoante(letra);

    return 0;
}

void consoante(char letter)
{
    if (letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u' ||
        letter == 'A' || letter == 'E' || letter == 'I' || letter == 'O' || letter == 'U')
    {
        printf("\nNão é consoante\n");
    }
    else if (letter >= 66 && letter <= 68 || letter >= 70 && letter <= 72 || letter >= 74 && letter <= 78 || letter >= 80 && letter <= 84 || letter >= 86 && letter <= 90 ||
             letter >= 98 && letter <= 100 || letter >= 102 && letter <= 104 || letter >= 106 && letter <= 110 || letter >= 112 && letter <= 116 || letter >= 118 && letter <= 122) //nesta parte usei a tabela ascii
    {
        printf("É uma consoante\n");
    }
    else if (letter <= 64 || letter >= 91 && letter <= 96 || letter >= 123)//aqui tambem usei a tabela ascii
    {
        printf("ERRO!!\n");
    }
}