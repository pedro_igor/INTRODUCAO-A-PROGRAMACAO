/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include <stdio.h>
#include <math.h>

double distancia(int x1, int y1, int x2, int y2);

int main()
{
    double x1, x2, y1, y2;

    printf("Usuario digite as coordernadas do primeiro ponto: ");
    scanf("%lf %lf", &x1, &y1);

    printf("\nDigite as coordernadas do segundo ponto: ");
    scanf("%lf %lf", &x2, &y2);

    double dist = distancia(x1, y1, x2, y2);

    printf("A distancia entre os dois pontos é: %.2lf\n", dist);

    return 0;
}

double distancia(int x1, int y1, int x2, int y2)
{
    double dist = sqrt(pow(x2 - x1, 2) + (y2 - y1));

    return dist;
}