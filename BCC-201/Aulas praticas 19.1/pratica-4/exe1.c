/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercio 1
#include <stdio.h>
#include <stdlib.h>
#include "/home/pedro/Documentos/code/color.h"
#define PI 3.141592

double celsius_to_kelvin(double tempC);
double celsius_to_farenheint(double tempC);
double farenheint_to_celsius(double tempF);
double farenheint_to_kelvin(double tempF);
double kelvin_to_celsius(double tempK);
double kelvin_to_farenheint(double tempk);
double degrees_to_radian(double graus);
double radian_to_degrees(double rad);

int main()
{
    system("clear");
    double temperatura, angulo;
    double farehinheit, celsius, kelvin;
    int op1, op2;

    printf("\t%s## CONVERSOR DE UNIDADES ##%s\n\n", NEGRITO,RESET);

    printf("1) Angulo\n");
    printf("2) Temperatura\n");

    printf("Selecione uma opção: ");
    scanf("%d", &op1);

    switch (op1)
    {
        case 1:
            printf("Qual é a unidade de origem?\n");
            
            printf("\n1) Graus");
            printf("\n2) Radianos\n");

            printf("Selecione uma opção: ");
            scanf("%d", &op2);

            switch (op2)
            {
                case 1:
                    printf("\nDigite o valor em graus: ");
                    scanf("%lf", &angulo);

                    double radiano = degrees_to_radian(angulo);

                    printf("Valor em Radianos: %lf\n", radiano);
                    break;

                case 2:
                    printf("\nDigite o valor em radianos: ");
                    scanf("%lf", &angulo);

                    double graus = radian_to_degrees(angulo);

                    printf("Valor em Graus: %lf\n", graus);
                    break;
            
                default:
                    printf("ERRO: opçao invalida\n");
                    break;
            }

            break;

        case 2:
            printf("Qual é a unidade de origem?\n");
            
            printf("\n1) Celsius");
            printf("\n2) Farehinheit");
            printf("\n3) Kelvin\n");

            printf("Selecione uma opção: ");
            scanf("%d", &op2);     

            switch (op2)
            {
                case 1:

                    printf("\nDigite o valor em celsius: ");
                    scanf("%lf", &temperatura);

                    farehinheit = celsius_to_farenheint(temperatura);
                    kelvin = celsius_to_kelvin(temperatura);

                    printf("Valor em Farehinheit: %.2lf\n", farehinheit);
                    printf("Valor em kelvin: %.2lf\n", kelvin);
                    
                    break;

                case 2:

                    printf("\nDigite a temperatura em Farehinheit: ");
                    scanf("%lf", &temperatura);

                    celsius = farenheint_to_celsius(temperatura);
                    kelvin = farenheint_to_kelvin(temperatura);

                    printf("Valor em Celsius: %.2lf\n", celsius);
                    printf("Valor em Kelvin: %.2lf\n", kelvin);

                    break;

                case 3:

                    printf("\nDigite a temperatura em Kelvin: ");
                    scanf("%lf", &temperatura);

                    farehinheit = kelvin_to_farenheint(temperatura);
                    celsius = kelvin_to_celsius(temperatura);

                    printf("Valor em Celsius: %.2lf\n", celsius);
                    printf("Temperatura em Farehinheit: %.2lf\n", farehinheit);
                    break;
            
                default:

                    printf("ERRO: Opção invalida\n");
                    break;
            }       

            break;
    
        default:
            printf("ERRO: opção invalida\n");
            break;
    }

    return 0;
}

double celsius_to_kelvin(double tempC)
{
    double kelvin = tempC + 273;
    return kelvin;
}

double celsius_to_farenheint(double tempC)
{
    double farenheint = (1.8 * tempC) + 32;
    return farenheint;
}

double farenheint_to_celsius(double tempF)
{
    double celsius = (tempF - 32) / 1.8;
    return celsius;
}

double farenheint_to_kelvin(double tempF)
{
    double kelvin = (tempF + 459.67) * (5.0 / 9.0);
    return kelvin;
}

double kelvin_to_celsius(double tempK)
{
    double celsius = tempK - 273;
    return celsius;
}

double kelvin_to_farenheint(double tempk)
{
    double farenheint = tempk * (9.0 / 5.0) - 459.67;
    return farenheint;
}

double degrees_to_radian(double graus)
{
    double rad = graus * (PI / 180);
    return rad;
}

double radian_to_degrees(double rad)
{
    double graus = rad * (180 / PI);
}
