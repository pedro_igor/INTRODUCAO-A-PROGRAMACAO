/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1
#include <stdio.h>

int main()
{
    int cont_neg = 0, cont_pos = 0;
    int i;
    double nro;

    do
    {
        printf("DIGITE UM NÚMERO REAL (PARAR = 0): ");
        scanf("%lf", &nro);

        if (nro < 0)
            cont_neg++;
        else if(nro > 0)
            cont_pos++;

        i++;

    } while (nro != 0);

    printf("\n");
    printf("QANTIDADE DE POSITIVOS DIGITADOS: %d\n", cont_pos);
    printf("QUATIDADE DE NEGATIVOS DIGITADOS: %d\n", cont_neg);

    return 0;
}