/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 3
#include <stdio.h>
#include <math.h>

int main()
{
    int n;
    double soma = 0;
    double x;

    printf("DIGITE O VALOR DE N: ");
    scanf("%d", &n);

    int i = 0;
    for (; i < n; i++)
    {
        printf("DIGITE O VALOR DE X%d: ", i + 1);
        scanf("%lf", &x);

        soma += pow(x, 2);
    }

    printf("\nMÉDIA QUADRÁTICA: %.0lf\n", sqrt(soma / n));

    return 0;
}