/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include <stdio.h>

int main()
{
    double nota;
    double media;
    double maior = 0, menor = 10, soma = 0;
    int i = 0;

    do
    {
        printf("DIGITE UMA NOTA (ou uma nota negativa para sair): ");
        scanf("%lf", &nota);
        if (nota >= 0)
        {
            soma += nota;

            if (nota > maior)
            {
                maior = nota;
            }

            if (nota < menor)
            {
                menor = nota;
            }

            i++;
        }
        
    } while (nota != -1);

    media = soma / i;

    printf("A MÉDIAS DAS %d NOTAS É: %.2lf\n", i, media);
    printf("A MAIOR NOTA É: %.1lf\n", maior);
    printf("A MENOR NOTA É: %.1lf\n", menor);

    return 0;
}