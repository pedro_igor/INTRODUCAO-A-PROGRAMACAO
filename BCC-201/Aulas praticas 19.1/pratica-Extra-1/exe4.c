/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 4
#include <stdio.h>

int main()
{
    int n;
    printf("DIGITE O VALOR DE N: ");
    scanf("%d", &n);

    int nro = n;

    int fac;

    for (fac = 1; n > 1; n -= 1)
    {
        fac *= n;
    }
    printf("%d! = %d\n", nro, fac);

    return 0;
}