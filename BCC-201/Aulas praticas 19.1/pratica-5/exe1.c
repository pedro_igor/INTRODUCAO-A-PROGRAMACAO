/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1
#include <stdio.h>

void converter_temp(float celsius, float *fahrenheit, float *kelvin);

int main()
{
    float temp_c, temp_f, temp_k;

    printf("DIGITE A TEMPERATURA EM CELSIUS: ");
    scanf("%f", &temp_c);

    converter_temp(temp_c, &temp_f, &temp_k);

    printf("Temperatura em Celsius: %.2f°C\n", temp_c);
    printf("Temperatura em Fahrenheit: %.2f°F\n", temp_f);
    printf("Temperatura em Kelvin: %.2fK\n", temp_k);

    return 0;
}

void converter_temp(float celsius, float *fahrenheit, float *kelvin)
{
    *fahrenheit = (celsius * 1.8) + 32;
    *kelvin = celsius + 273.17;
}