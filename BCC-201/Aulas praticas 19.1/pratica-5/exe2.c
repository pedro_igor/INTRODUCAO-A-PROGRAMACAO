/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include <stdio.h>
#include <math.h>

int calculaRaizes(int a, int b, int c, double *x1, double *x2);

int main()
{
    int a, b, c, delta;
    double x1, x2;
    
    printf("Digite os coeficientes da equação ");
    scanf("%d %d %d", &a, &b, &c);

    delta = calculaRaizes(a, b, c, &x1, &x2);

    if(delta == 0 )
    {
        printf("A equação nao posuem raizes reais\n");
    }
    else 
    {
        printf("x1 = %.2lf\n", x1);
        printf("x2 = %.2lf\n", x2);
    }

    return 0;
}

int calculaRaizes(int a, int b, int c, double *x1, double *x2)
{
    int delta = pow(b, 2) - (4 * a * c);

    if(delta < 0)
    {
        return 0;
    }
    else
    {
        *x1 = (-b + sqrt(delta)) / (2 * a);
        *x2 = (-b - sqrt(delta)) / (2 * a);
        return 1;
    }
    
}