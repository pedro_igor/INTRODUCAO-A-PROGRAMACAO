#include <stdio.h>
#include <string.h>

#define TAM 100

int main()
{
    char frase[TAM];

    printf("Digite uma frase: ");
    fgets(frase, TAM - 1, stdin);

    int tamanho = strlen(frase) - 1;

    char aux_frase[tamanho];

    int j = 0;

    for (int i = 0; i < tamanho; i++, j++)
    {
        if (frase[j] == 'a')
        {
            aux_frase[i] = '@';
        }
        else if (frase[j] == 'e')
        {
            aux_frase[i] = '_';
        }
        else if (frase[j] == 'i')
        {
            aux_frase[i] = '|';
        }
        else if (frase[j] == 'o')
        {
            aux_frase[i] = '0';
        }
        else if (frase[j] == 'u')
        {
            aux_frase[i] = '#';
        }
        else if (frase[j] == 's')
        {
            aux_frase[i] = '$';
        }
        else
        {
            aux_frase[i] = frase[j];
        }   
    }

    printf("%s", aux_frase);
    printf("\n");

    return 0;
}