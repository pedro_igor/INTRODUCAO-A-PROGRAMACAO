#include <stdio.h>
#include <string.h>

#define TAM 100

int main()
{
    char frase[TAM];

    printf("Digite uma frase: ");
    fgets(frase, TAM - 1, stdin);

    int tamanho = strlen(frase) - 1;

    //char aux_frase[tamanho];

    //int j = 0;

    for (int i = 0; i < tamanho; i++)
    {
        if(frase[i] == ' ')
        {
           printf("%c", frase[i]); 
        }
        else if(frase[i] >= 65 && frase[i] < 91 || frase[i] >= 33 && frase[i] < 65 && frase[i] >= 91 && frase[i] < 96)
        {
            printf("%c", frase[i]);
        }
        else
        {
            printf("%c", frase[i] - 32);
        }
    }

    printf("\n");

    return 0;
}