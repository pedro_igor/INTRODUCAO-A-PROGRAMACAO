/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 1
#include <stdio.h>
#include <stdlib.h>

#define NUM 10

void lermatriz(int linha, int coluna, int matriz[NUM][NUM]);
void printmatriz(int linha, int coluna, int matriz[NUM][NUM]);
void multiMatizes(int linha, int coluna, int a[NUM][NUM], int b[NUM][NUM], int c[NUM][NUM], int p);

int main()
{
    int a[NUM][NUM], b[NUM][NUM], c[NUM][NUM];
    int linhaA, colunaA, linhaB, colunaB;

    system("clear");
    //ler e imprime a matriz A
    printf("DIGITE AS DIMENSÕES DA MATRIZ A: ");
    scanf("%d %d", &linhaA, &colunaA);
    lermatriz(linhaA, colunaA, a);
    printmatriz(linhaA, colunaA, a);

    printf("\n");

    //ler e imprime a matriz B
    printf("DIGITE AS DIMENSÕES DA MATRIZ B: ");
    scanf("%d %d", &linhaB, &colunaB);
    lermatriz(linhaB, colunaB, b);
    printmatriz(linhaB, colunaB, b);

    printf("\n");

    if (colunaA == linhaB)
    {
        multiMatizes(linhaA, colunaB, a, b, c, linhaB); //linhaB seria o p
        printmatriz(linhaA, colunaB, c);
    }

    return 0;
}

//função ler matriz
void lermatriz(int linha, int coluna, int matriz[NUM][NUM])
{

    for (int i = 0; i < linha; i++)
    {
        for (int j = 0; j < coluna; j++)
        {
            printf("Digite o valor da matriz na posição [%d][%d]: ", i+1, j+1);
            scanf("%d", &matriz[i][j]);
        }
    }
} //fim da função

//funçao para imprimir matriz
void printmatriz(int linha, int coluna, int matriz[NUM][NUM])
{
    for (int i = 0; i < linha; i++)
    {
        for (int j = 0; j < coluna; j++)
        {
            printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
} //fim da função

//função para multiplicação das matrizes A x B
void multiMatizes(int linha, int coluna, int a[NUM][NUM], int b[NUM][NUM], int c[NUM][NUM], int p)
{

    for (int i = 0; i < linha; i++)
    {
        for (int j = 0; j < coluna; j++)
        {
            c[i][j] = 0;
            for (int k = 0; k < p; k++)
            {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
} //fim da função