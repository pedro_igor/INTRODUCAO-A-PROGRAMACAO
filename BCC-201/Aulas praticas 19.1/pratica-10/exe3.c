/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 3
#include <stdio.h>
#include <string.h>

#define TAM 70

int main()
{
    char nome[TAM], nome_aux[TAM];
    char sobrenome[TAM];
    int k = 0;

    printf("Digite o nome completo: ");
    fgets(nome, TAM - 1, stdin);

    int tamanho = strlen(nome) - 1;
    nome[tamanho] = '\0';

    int j = 0;

    printf("\n");

    for (int i = 0; i < tamanho; i++)
    {
        if (nome[i] != ' ')
        {
            nome_aux[j] = nome[i];
            k++;
        }
    }

    for (int i = tamanho - 1; nome[i] != ' '; i--)
    {
        sobrenome[j] = nome[i];
        j++;
    }

    for (int i = (tamanho - j); i < tamanho; i++)
    {
        printf("%c", nome[i]);
    }

    printf(", ");

    for (int i = 0; i < (tamanho - j); i++)
    {
        printf("%c", nome[i]);
    }

    printf("\n");

    tamanho = strlen(nome_aux) - 1;

    int tamanho_sobre = strlen(sobrenome);

    printf("Total de letras: %d\n", k);
    printf("Total de letras do último sobrenome: %d\n", j);

    return 0;
}