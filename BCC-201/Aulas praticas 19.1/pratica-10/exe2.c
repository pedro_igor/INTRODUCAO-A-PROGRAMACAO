/**************************************
*Nome: Pedro Igor de Souza Malaquias  *
*Matricula: 18.1.4076                 *
*Turma:                               *
***************************************/
//exercicio 2
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TAM 100

int main()
{
    char palavra1[TAM];
    char palavra2[TAM];
    char fim[4] = {'F', 'I', 'M'};

    system("clear");

    printf("Digite uma palavra ou FIM para sair: ");
    fgets(palavra1, TAM - 1, stdin);

    int tamanho = strlen(palavra1) - 1;
    palavra1[tamanho] = '\0';
    int j; //para calcular todas as posiçoes da palavra2

    for (int i = 0; i < tamanho; i++)
    {
        palavra2[i] = palavra1[tamanho - 1 - i];
        j++;
    }

    palavra2[j] = '\0';

    printf("%s\n", palavra2);

    do
    {
        if (strcmp(palavra1, palavra2) == 0)
        {
            printf("%s é um palindromo \n", palavra1);
        }
        else
        {
            printf("%s não é um palindromo \n", palavra1);
        }

        printf("Digite uma palavra ou FIM para sair: ");
        fgets(palavra1, TAM - 1, stdin);

        int tamanho = strlen(palavra1) - 1;
        palavra1[tamanho] = '\0';
        int j; //para calcular todas as posiçoes da palavra2

        for (int i = 0; i < tamanho; i++)
        {
            palavra2[i] = palavra1[tamanho - 1 - i];
            j++;
        }

        palavra2[j] = '\0';

    } while (strcmp(palavra1, fim) != 0);

    return 0;
}
