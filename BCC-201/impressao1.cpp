#include <iostream>
#include <stdio.h>

#define TAMH 26
#define TAMV 10

using namespace std;

void impress(char **m)
{
	char b = 65; //char b = 'H'
	char a = 65; //int a = 1

	/*for (int i = 0; i < TAM; i++)
		for (int j = 0; j < TAM; j++)
			txt >> m[i][j];*/

	cout << "┌───┬";
	for(int i = 0; i < TAMH; i++)
		cout << "───┬";
	cout << "───┐" << endl << "|   | ";

	for (int i = 0; i < TAMH; i++, a++)
	{
		printf("%c ", a);//cout << a;
		if(a < 10)
			cout << " ";
		cout << "| ";
	}

	cout << "  |\n├───";

	for(int i = 0; i <= TAMH; i++)
		cout << "┼───";
	cout << "┤";

	a -= TAMH;

	for (int i = 0; i < TAMV; i++, b++)
	{
		cout << endl
			 << "| " << b << " | ";

		for (int j = 0; j < TAMH; j++)
			if (m[i][j] == '-')
				cout << "  | ";
			else
				cout << m[i][j] << " | ";

		cout << b << " |" << endl;
		cout << "├───";
		
		for(int i = 0; i <= TAMH; i++)
			cout << "┼───";

		cout << "┤";
	}

	cout << endl
		 << "|   | ";
	for (int i = 0; i < TAMH; i++, a++)
	{
		printf("%c ", a);//cout << a;
		if(a < 10)
			cout << " ";
		cout << "| ";
	}

	cout << "  |" << endl << "└";

	for(int i = 0; i <= TAMH; i++, a++)
		cout << "───┴";
	cout << "───┘" << endl;

}

int main ()
{
	char **matriz;

	matriz = new char*[TAMV];
	for(int i = 0; i < TAMV; i++)
		matriz[i] = new char[TAMH];

	for(int i = 0; i < TAMV; i++)
		for(int j = 0; j < TAMH; j++)
			matriz[i][j] = ' ';

	impress(matriz);
}