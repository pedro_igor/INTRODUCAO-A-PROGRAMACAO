#include <stdlib.h>
#include <stdio.h>

int main()
{
    int nro;
    double *v_notas;
    double media = 0;

    printf("Qual é a quantidade de notas: ");
    scanf("%d", &nro);

    v_notas = malloc(nro * sizeof(double));

    for (int i = 0; i < nro; i++)
    {
        printf("Digite a %dª notas: ", i + 1);
        scanf("%lf", &v_notas[i]);

        media += v_notas[i];
    }

    printf("A media da notas é: %.2lf.\n", media / nro);

    free(v_notas);
    return 0;
}