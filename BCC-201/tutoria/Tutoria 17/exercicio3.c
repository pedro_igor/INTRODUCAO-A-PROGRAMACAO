#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *file;

    file = fopen("arq.txt", "w+");

    char carac;

    while (carac != '0')
    {
        scanf("%c", &carac);
        if (carac != '0')
            fprintf(file, "%c", carac);
    }

    fclose(file);
    file = fopen("arq.txt", "r");

    while (!feof(file))
    {
        fscanf(file, "%c", &carac);
        printf("%c ", carac);
    }

    fclose(file);

    return 0;
}