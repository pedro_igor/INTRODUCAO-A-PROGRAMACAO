#include <stdio.h>
#include <stdlib.h>

int *cria_vetor(int n)
{
    int *vetor = malloc(n * sizeof(int));

    return vetor; 
}

void print(int *vet ,int n)
{
    for(int i = 0; i < n; i++)
    {
        printf("%d ", vet[i]);
    }
}

int main()
{
    int n; 
    int *vet;
    printf("Digite o tamanho do vetor: ");
    scanf("%d", &n);

    vet = cria_vetor(n);
    //vet = malloc(n * sizeof(int));

    //printf("%d", vet[2]);

    for(int i = 0; i < n; i++)
    {
        printf("%d: ", i + 1);
        scanf("%d", &vet[i]);
    }

    puts("\n");

    print(vet, n);

    puts("\n");

    free(vet);
    return 0;
}