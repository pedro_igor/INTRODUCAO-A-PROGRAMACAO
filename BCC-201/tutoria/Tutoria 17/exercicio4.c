#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *aqruivo = fopen("arq.txt", "r");

    char caracter;
    int cont = 0;

    printf("Digite o caracter: ");
    scanf("%c", &caracter);

    while (!feof(aqruivo))
    {
        char c = fgetc(aqruivo);
        if (c == caracter)
            cont++;
    }

    printf("%c repete %d.\n", caracter, cont);

    return 0;
}