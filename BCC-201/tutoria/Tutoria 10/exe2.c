#include <stdio.h>
#include <stdlib.h>

#define CLIENTS 15

int main()
{
    double valor1, valor2, valor3;
    double bonus;

    system("reset");

    printf("\nLOJÃO\n");

    for (int i = 0; i < CLIENTS; i++)
    {
        printf("\nCliente: %d\n", i + 1);
        printf("Por Favor ddigite o valor das ultimas 3 compras no ultimo ano:\n");
        scanf("%lf %lf %lf", &valor1, &valor2, &valor3);

        double soma = 0;

        soma = valor1 + valor2, valor3;

        while (soma < 0)
        {
            if (soma < 1000)
            {
                bonus = soma * 0.10;
                printf("O bonus foi de 10%%\n");
            }
            else
            {
                bonus = soma * 0.15;
                printf("O bonus foi de 15%%\n");
            }
        }

        printf("BONUS PARA O CLIENTE %d: %.2lf\n\n", i + 1, bonus);
    }

    return 0;
}
