#include <stdio.h>
#include <stdlib.h>

#define QUANT_PESSOAS 15.0

int main()
{
    system("reset");
    int idade;
    int cont1 = 0, cont2 = 0, cont3 = 0, cont4 = 0, cont5 = 0;

    for (int i = 0; i < QUANT_PESSOAS; i++)
    {
        printf("\nDIGITE A IDADE DA %dª PESSOA: ", i + 1);
        scanf("%d", &idade);

        if (idade >= 0 && idade <= 15)
        {
            cont1++;
        }
        else if (idade > 15 && idade <= 30)
        {
            cont2++;
        }
        else if (idade > 30 && idade <= 45)
        {
            cont3++;
        }
        else if (idade > 45 && idade <= 60)
        {
            cont4++;
        }
        else if (idade > 60)
        {
            cont5++;
        }
    }

    printf("\nPORCENTAGEM DAS IDADE EM CADA FAIXA ETARIA\n");
    printf("1ª| Até 15 anos: %.2lf%%\n", (cont1 / QUANT_PESSOAS) * 100);
    printf("2ª| De 16 a 30 anos: %.2lf%%\n", (cont2 / QUANT_PESSOAS) * 100);
    printf("3ª| De 31 a 45 anos: %.2lf%%\n", (cont3 / QUANT_PESSOAS) * 100);
    printf("4ª| De 46 a 60 anos: %.2lf%%\n", (cont4 / QUANT_PESSOAS) * 100);
    printf("5ª| Acima de 61 anos: %.2lf%%\n", (cont5 / QUANT_PESSOAS) * 100);
    printf("\n");

    return 0;
}