#include <stdio.h>
#include <stdlib.h>
int main()
{
    int idade;
    char sexo;
    double altura, peso;

    double soma_ault_h = 0, soma_ault_m = 0, soma_altura_10_and_20 = 0;
    double soma_age_h = 0, soma_age_m = 0, soma_age_group = 0;
    int quant_50_age;
    int porcem_25_mulher;
    int quant_people = 0, quant_mulheres = 0, quant_homens = 0, quant_10_20 = 0;

    int i = 0;

    system("reset");

    for (i; i < 7; i++)
    {
        printf("\n\nDIGTE A IDADE DA PESSOA %d: ", i + 1);
        scanf("%d", &idade);

        printf("DIGITE O PESO: ");
        scanf("%lf", &peso);

        printf("DIGITE A ALTURA: ");
        scanf("%lf", &altura);

        getchar();
        printf("DIGITE O SEXO DA PESSOA (h pra homens e m para  mulheres): ");
        scanf("%c", &sexo);

        if (sexo == 'h' || sexo == 'H')
            quant_homens++;
        else if (sexo == 'm' || sexo == 'M')
            quant_mulheres++;

        if (idade >= 50)
            quant_50_age++;

        if (idade >= 10 && idade <= 20)
        {
            soma_altura_10_and_20 += altura;
            quant_10_20++;
        }
        if (sexo == 'm' || sexo == 'M' && idade >= 25)
            porcem_25_mulher++;

        if (sexo == 'h' || sexo == 'H')
            soma_ault_h += altura;

        if (sexo == 'm' || sexo == 'M')
            soma_ault_m += altura;

        if (sexo == 'h' || sexo == 'H')
            soma_age_h += idade;

        if (sexo == 'm' || sexo == 'M')
            soma_age_m += idade;

        soma_age_group += idade;

        quant_people++;
    }

    printf("QUANTDADE DA PESSOAS COM IDADE SUPERIOR A 50 ANOS: %d\n", quant_50_age);
    printf("MÉDIA DAS ALTURAS DAS PESSOAS COM IDADE ENTRES 10 E 20 ANOS: %.2lf%%\n", soma_altura_10_and_20 / quant_10_20);
    printf("A PORCENTADE DE MULHERES DO GUPO COM IDADE ACIMA DE 25 ANOS: %.2f\n", (porcem_25_mulher / quant_mulheres * 1.0) * 100);
    printf("A ALTURA MEDIA DOS HOMENS: %.2lf\n", soma_ault_h / quant_homens);
    printf("A ALTURA MEDIA DAS MULHERES: %lf\n", soma_ault_m / quant_mulheres);
    printf("A IDADE MEDIA DO GRUPOS %.2lf\n", soma_age_group / quant_people);
    printf("A IDADE MÉDIA DAS MULHERES: %.2lf\n", soma_age_m / quant_mulheres);
    printf("A IDADE MÉDIA DOS HOMENS: %.2lf\n", soma_age_h / quant_homens);

    return 0;
}