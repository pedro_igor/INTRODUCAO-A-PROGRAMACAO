#include <stdio.h>

void troca(int *px, int *py);

int main()
{
    int a, b;
    printf("digite os valores a e b: ");
    scanf("%d %d", &a, &b);

    troca(&a, &b);

    printf("OS VALORES TROCADOS SERÃO:\n");
    printf("a = %d\n", a);
    printf("b = %d\n", b);

    return 0;
}

void troca(int *px, int *py)
{
    int aux;
    aux = *px;
    *px = *py;
    *py = aux;
}