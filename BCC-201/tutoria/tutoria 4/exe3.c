#include <stdio.h>

float media1(float p1, float p2, float p3);
char situacao(float p1, float p2, float p3, float faltas, float aulas, float *media);

int main()
{
    float p1, p2, p3, media;
    float faltas, nro_aulas;

    printf("Qual foi o total de aulas dadas: ");
    scanf("%f", &nro_aulas);

    printf("Numeros de faltas do aluno: ");
    scanf("%f", &faltas);

    printf("Digite as notas da 3 provas: ");
    scanf("%f %f %f", &p1, &p2, &p3);

    media = media1(p1, p2, p3);

    printf("media = %.2f\n", media);

    char situ = situacao(p1, p2, p3, faltas, nro_aulas, &media);

    if (situ == 'F')
    {
        printf("Reprvado por Falta\n");
    }
    else if (situ == "R")
    {
        printf("Reprvado por Nota\n");
    }
    else
    {
        printf("Aprovado\n");
    }
    return 0;
}

float media1(float p1, float p2, float p3)
{
    float med = (p1 + p2 + p3) / 3;
    return med;
}

char situacao(float p1, float p2, float p3, float faltas, float aulas, float *media)
{
    if ((faltas / aulas) >= 0.25)
    {
        return 'F';
    }
    else
    {
        if (*media >= 6.0)
        {
            return 'A';
        }
        else
        {
            return 'R';
        }
    }
}
