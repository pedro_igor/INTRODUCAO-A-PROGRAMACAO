#include <stdio.h>
#include <math.h>

float area_hex(float l);
float perime(float l);
void calcula_hex(float l, float *area, float *perimetro);

int main()
{
    float l, area, perimetro;
    printf("Digite o lado do hexagono: ");
    scanf("%f", &l);

    //area = area_hex(l);
    //perimetro = perime(l);

    calcula_hex(l, &area, &perimetro);

    printf("area = %.2f\n", area);
    printf("perimetro = %.2f\n", perimetro);

    return 0;
}

float area_hex(float l)
{
    float area = (pow((3 * l), 2) * sqrt(3)) / 2;
    return area;
}

float perime(float l)
{
    float perimetro = 6 * l;
    return perimetro;
}

void calcula_hex(float l, float *area, float *perimetro)
{
    *area = (pow((3 * l), 2) * sqrt(3)) / 2;
    *perimetro = 6 * l;
}