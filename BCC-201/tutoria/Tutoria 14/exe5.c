#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TAM 100

int main()
{
    char str1[TAM];
    char str2[TAM];

    system("reset");
    printf("Digite a primeira string: ");
    fgets(str1, TAM, stdin);

    printf("Digite a segunda string: ");
    fgets(str2, TAM, stdin);

    if(strcmp(str1, str2) == 0)
    {
        printf("As String são iguais.\n");
        return 0;
    }

    printf("As String não são iguais.\n");

    return 0;
}