#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TAM 100
int main()
{
    char str[TAM];

    printf("Digite uma palavra: ");
    fgets(str, TAM, stdin);

    int tamanho = strlen(str) - 1;

    for(int i = 0; i < tamanho; i++)
    {
        printf("%c",str[i] - 32);
    }

    printf("\n");
    return 0;
}