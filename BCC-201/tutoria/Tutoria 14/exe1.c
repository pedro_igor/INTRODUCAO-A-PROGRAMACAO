#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
    char nome[100];

    system("clear");

    printf("Digite o nome: ");
    fgets(nome, 100, stdin);

    int tamanho = strlen(nome) - 1;

    printf("O nome tem %d de tamanho\n", tamanho);

    return 0;
}
