#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 100

int main()
{
    char str[MAX];
    char nro_one = '1';

    system("reset");

    int cont = 0;

    printf("Digite um número: ");
    fgets(str, MAX, stdin);

    int tamanho = strlen(str);

    for (int i = 0; i < tamanho; i++)
    {
        if(str[i] == nro_one)
        {
            cont++;
        }
    }

    printf("%s -> %d\n", str, cont);

    return 0;
}