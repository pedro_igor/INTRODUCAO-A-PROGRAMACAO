//QUANTIDADE DE MAÇAS E O PREÇO
#include <stdio.h>
#include <stdlib.h>
#include "apple.h"

int main()
{
    int quant_macas;
    double preco;
    double valor_unitario;

    printf("Digite a quantidades de maças: ");
    scanf("%d", &quant_macas);

    preco = apple(quant_macas);

    system("clear");

    if (quant_macas < 12)
    {
        valor_unitario = 0.30;
    }
    else
    {
        valor_unitario = 0.25;
    }

    printf("-----------------------------\n");
    printf("Quandidades de maçãs: %d\n", quant_macas);
    printf("Valor unitario      : %.2lfR$\n", valor_unitario);
    printf("Total da compra     : %.2lfR$\n", preco);
    printf("-----------------------------\n");

    return 0;
}