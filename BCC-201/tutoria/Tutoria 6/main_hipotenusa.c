// main do calculo da hipotenusa
#include <stdio.h>
#include "hipotenusa.h"

int main()
{
    double cat_1, cat_2, hipotenusa;

    printf("DIGITE OS CATETOS DOS TRIANGULO (Em cm): ");
    scanf("%lf %lf", &cat_1, &cat_2);

    calc_hipo(cat_1, cat_2, &hipotenusa);

    printf("CATETO 1: %.2lf cm\n", cat_1);
    printf("CATETO 2: %.2lf cm\n", cat_2);
    printf("HIPOTENUSA: %.2lf cm\n", hipotenusa);

    return 0;
}