//varios calculos
#include "varias_calc.h"

double area_quadrado(double lado)
{
    double area = pow(lado, 2);
    return area;
}

double area_circulo(double raio)
{
    double area = PI * pow(raio, 2);
    return area;
}

double area_triangulo(double base, double altura)
{
    double area = (base * altura) / 2;
    return area;
}
