//calculo da hipotenusa
#ifndef HIPOTENUSA_H
#define HIPOTENUSA_H

#include <math.h>

void calc_hipo(double cateto_1, double cateto_2, double *hipotenusa);

#endif