//main varios calculos
#include <stdio.h>
#include "varias_calc.h"

int main()
{
    double lado, raio, base, altura;
    double area;
    int opcao;

    printf("\nAREA DO TRIANGULO, CIRCULO, QUADRADO\n\n");

    printf("1 - Area do Quadrado\n");
    printf("2 - Area do Circulo\n");
    printf("3 - Area do Triangulo\n");

    printf("\nQual opção voce quer: ");
    scanf("%d", &opcao);

    switch (opcao)
    {
        case 1:
            printf("AREA DO QUADRADO\n\n");
            
            printf("Digite o lado do Quadrado: ");
            scanf("%lf", &lado);

            area = area_quadrado(lado);

            printf("A area do quadrado é: %.2lf\n", area);
            break;

        case 2:
            printf("AREA DO CIRCULO\n\n");

            printf("Digite o Raio do circulo: ");
            scanf("%lf", &raio);

            area = area_circulo(raio);

            printf("A area do circulo é: %.2lf\n",area);
            break;

        case 3:
            printf("AREA DO TRIANGULO\n\n");

            printf("Digite a base do triangulo: ");
            scanf("%lf", &base);

            printf("Digite a altura do triangulo: ");
            scanf("%lf", &altura);

            area = area_triangulo(base, altura);

            printf("A area do triangulo é: %.2lf\n", area);
            break;
    
        default:
            printf("ERRO\n");
            break;
    }

    return 0;
}