//QUANTIDADE DE MAÇAS E O PREÇO
#include "apple.h"

double apple(int quant_apple)
{
    double preco;
    if(quant_apple < 12)
    {
        preco = quant_apple * ABAIXO_DE_12;
        return preco;
    }
    else
    {
        preco = quant_apple * UMA_DUZIA;
        return preco;
    }
    
}