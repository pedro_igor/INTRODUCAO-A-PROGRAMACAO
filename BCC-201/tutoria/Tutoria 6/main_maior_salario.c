//MAIOR SALARIO
#include <stdio.h>
#include "maior_salario.h"

int main()
{
    double salario_1, salario_2;
    double maior;

    printf("Digite o primeiro salario: ");
    scanf("%lf", &salario_1);

    printf("Digite o segundo salario: ");
    scanf("%lf", &salario_2);

    maior = maior_salario(salario_1, salario_2);

    printf("\nSalario 1: %.2lf", salario_1);
    printf("\nSalario 2: %.2lf", salario_2);
    printf("\nMAIOR SALARIO: %.2lf\n", maior);

    return 0;
}