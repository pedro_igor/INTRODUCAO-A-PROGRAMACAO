//varios calculos
#ifndef VARIAS_CALC_H
#define VARIAS_CALC_H

#include <math.h>
#define PI 3.14159265359

double area_quadrado(double lado);
double area_circulo(double raio);
double area_triangulo(double base, double altura);

#endif