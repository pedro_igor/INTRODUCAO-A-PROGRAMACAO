//calculo da hipotenusa
#include "hipotenusa.h"

void calc_hipo(double cateto_1, double cateto_2, double *hipotenusa)
{
    *hipotenusa = sqrt(pow(cateto_1, 2) + pow(cateto_2, 2));
}