//exercicio 2
#include <stdio.h>

int main()
{
    int x, y;
    char op;

    printf("Digite os numeros no qual voce quer fazer a operação: ");
    scanf("%d%d", &x, &y);

    printf("Que operação deseja realizar: ");
    getchar();
    scanf("%c", &op);

    if (op == '+')
    {
        printf("Resltado de %d %c %d = %d\n", x, op, y, x + y);
    }
    else if (op == '-')
    {
        printf("Resltado de %d %c %d = %d\n", x, op, y, x - y);
    }
    else if (op == '*')
    {
        printf("Resltado de %d %c %d = %d\n", x, op, y, x * y);
    }
    else if (op == '/')
    {
        printf("Resltado de %d %c %d = %d\n", x, op, y, x / y);
    }
    else
        printf("Operação invalida \n");

    return 0;
}