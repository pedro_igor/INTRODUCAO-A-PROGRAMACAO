//exercicio 4
#include <stdio.h>
#include <math.h>

int main()
{
    double peso, altura;
    double imc;

    printf("Digite a Altura (m): ");
    scanf("%lf", &altura);

    printf("Digite o peso (kg): ");
    scanf("%lf", &peso);

    imc = peso / pow(altura, 2);

    if (imc < 20)
    {
        printf("Abaixo do peso ideal\n");
    }
    else if (imc >= 20 && imc <= 25)
    {
        printf("Peso ideal\n");
    }
    else if (imc > 25 && imc <= 30)
    {
        printf("Acima do peso ideal\n ");
    }
    else if (imc > 30)
    {
        printf("Obesidade ");
    }

    return 0;
}
