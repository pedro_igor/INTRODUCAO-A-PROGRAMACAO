//exercicio 3
#include <stdio.h>

int main()
{
    double nota1, nota2, nota3;
    double media, soma;
    int matricula;

    printf("Matricula: ");
    scanf("%d", &matricula);

    printf("Nota 1: ");
    scanf("%lf", &nota1);

    printf("Nota 2: ");
    scanf("%lf", &nota2);

    printf("Nota 3: ");
    scanf("%lf", &nota3);

    soma = nota1 + nota2 + nota3;
    media = soma / 3;

    if (soma < 0 && soma > 30)
    {
        printf("Soma Invalida!\n");
    }
    else
    {
        if (media >= 6 && media <= 10)
        {
            printf("\nMatricula: %d", matricula);
            printf("\nMedia da notas: %.1lf", media);
            printf("\nO aluno foi Aprovado!\n");
        }
        else if (media < 6)
        {
            printf("\nMatricula: %d", matricula);
            printf("\nMedia da notas: %.1lf", media);
            printf("\nO aluno foi Reprovado!\n");
        }
    }
}