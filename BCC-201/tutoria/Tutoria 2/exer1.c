//exercicio 1
#include <stdio.h>

int main()
{
    double salario, reajuste;

    printf("DIGITE O SALARIO DO FUNCIONARIO: ");
    scanf("%lf", &salario);

    if (salario < 1000.00)
    {
        reajuste = salario + (salario * 0.15);
        printf("O percentual do seu salario é de 15%% ");
        printf("\nO novo salario é de: %.2lf\n", reajuste);
    }
    else if (salario >= 1000.00 && salario < 2000.00)
    {
        reajuste = salario + (salario * 0.1);
        printf("O percentual do seu salario é de 10%% ");
        printf("\nO novo salario é de: %.2lf\n", reajuste);
    }
    else if (salario >= 2000.00)
    {
        reajuste = salario + (salario * 0.05);
        printf("O percentual do seu salario é de 5%% ");
        printf( "\nO novo salario é de: %.2lf\n",reajuste);
    }

    return 0;
} 