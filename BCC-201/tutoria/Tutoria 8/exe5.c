#include <stdio.h>

int main()
{
    int x, i = 0;

    printf("Digite um numero: ");
    scanf("%d", &x);

    while (i <= x)
    {
        if (i % 2 != 0)
        {
            printf("i = %d\n", i);
        }
        i++;
    }

    return 0;
}