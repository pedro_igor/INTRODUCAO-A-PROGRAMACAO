#include <stdio.h>

int main()
{
    int x;
    printf("Digite um numero entre 1 e 4: ");
    scanf("%d", &x);

    while (x != 1 && x != 2 && x != 3 && x != 4)
    {
        printf("Numero invalido: Digite novamente!\n");

        printf("Digite um numero entre 1 e 4: ");
        scanf("%d", &x);
    }

    printf("Numero correto Digitado: %d\n", x);

    return 0;
}