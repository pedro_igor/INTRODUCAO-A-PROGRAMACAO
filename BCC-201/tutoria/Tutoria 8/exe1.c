#include <stdio.h>

int main()
{
    int x;

    printf("Digite um numero inteiro: ");
    scanf("%d", &x);

    int i = 0;

    while (i <= x)
    {
        printf("i = %d\n", i);
        i++;
    }

    return 0;
}