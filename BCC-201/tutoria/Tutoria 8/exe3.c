#include <stdio.h>

int main()
{
    int x, i = 0;
    printf("Digite um numero: ");
    scanf("%d", &x);

    while (i <= 10)
    {
        printf("%d x %d = %d\n", x, i, x * i);
        i++;
    }

    return 0;
}