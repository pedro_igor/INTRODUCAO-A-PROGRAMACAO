//exercicio 4
#include <stdio.h>

int maior_end(int *a, int *b);

int main()
{
    int a,b;
    printf("digite dois valores: ");
    scanf("%d %d", &a, &b);

    maior_end(&a, &b);

    return 0;
}

int maior_end(int *a, int *b)
{
    if(a > b)
    {
        printf("a = %d = %p\n",*a, a);
    }
    else
    {
        printf("b = %d = %p\n",*b, b);
    }
}