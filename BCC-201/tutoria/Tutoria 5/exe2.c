#include <stdio.h>

void maior_menor(int *a, int *b, int *c);

int main()
{
    int a, b, c;

    printf("Digite os valoes de  a, b, c: ");
    scanf("%d %d %d", &a, &b, &c);

    maior_menor(&a, &b, &c);

    return 0;
}

void maior_menor(int *a, int *b, int *c)
{
    int maior_1, maior_2, maior_3;
    int menor_1, menor_2, menor_3;

    if (*a > *b && *a > *c && *b > *c)
    {
        maior_1 = *a;
        maior_2 = *b;
        maior_3 = *c;

        menor_1 = *c;
        menor_2 = *b;
        menor_3 = *a;
    }
    else if (*b > *a && *a > *c && *b > *c)
    {
        maior_1 = *b;
        maior_2 = *a;
        maior_3 = *c;

        menor_1 = *c;
        menor_2 = *a;
        menor_3 = *b;
    }
    else if (*a > *c && *c > *b && *a > *b)
    {
        maior_1 = *a;
        maior_2 = *c;
        maior_3 = *b;

        menor_1 = *b;
        menor_2 = *c;
        menor_3 = *a;
    }
    else if (*b > *c && *c > *a && *b > *a)
    {
        maior_1 = *b;
        maior_2 = *c;
        maior_3 = *a;

        menor_1 = *a;
        menor_2 = *c;
        menor_3 = *b;
    }
    else if (*c > *b && *b > *a && *c > *a)
    {
        maior_1 = *c;
        maior_2 = *b;
        maior_3 = *a;

        menor_1 = *a;
        menor_2 = *b;
        menor_3 = *c;
    }
    else if (*c > *a && *a > *b && *c > *b)
    {
        maior_1 = *c;
        maior_2 = *a;
        maior_3 = *b;

        menor_1 = *b;
        menor_2 = *a;
        menor_3 = *c;
    }

    printf("%d %d %d\n", menor_1, menor_2, menor_3);
    printf("%d %d %d\n", maior_1, maior_2, maior_3);

}