#include <stdio.h>

int main()
{
    int a;
    float b;
    char c;

    int *pa;
    float *pb;
    char*pc;

    printf("digite os valores de a, b, c: ");
    scanf("%d %f %c", &a, &b, &c);

    printf("\nAntes\n");
    printf("%d\n", a);
    printf("%.2f\n", b);
    printf("%c\n\n", c);  

    pa = &a; 
    pb = &b;
    pc = &c;

    *pa += 5;
    *pb += 6.0;
    *pc += 4;

    printf("Depois");
    printf("%d\n", a);
    printf("%.2f\n", b);
    printf("%c\n", c);

    return 0;
}