#include <stdio.h>

void func1(int *a, int *b, int *c, int *d);

int main()
{
    int w, x, y, z;

    printf("Digite os 4 valores: ");

    scanf("%d %d %d %d", &w, &x, &y, &z);

    func1(&w, &x, &y, &z);

    return 0;
}
void func1(int *a, int *b, int *c, int *d)
{
    int maior_1, maior_2, menor_1, menor_2;
    int soma, multplica, subtrai;

    //inicio do maior
    if (*a > *c && *b > *d)
    {
        if (*a > *b)
        {
            maior_1 = *a;
            maior_2 = *b;
        }
        else
        {
            maior_1 = *b;
            maior_2 = *a;
        }
    }

    if (*a > *b && *c > *d)
    {
        if (*a > *c)
        {
            maior_1 = *a;
            maior_2 = *c;
        }
        else
        {
            maior_1 = *c;
            maior_2 = *a;
        }
    }

    if (*a > *b && *d > *c)
    {
        if (*a > *d)
        {
            maior_1 = *a;
            maior_2 = *d;
        }
        else
        {
            maior_1 = *d;
            maior_2 = *a;
        }
    }

    if (*b > *a && *c > *d)
    {
        if (*b > *c)
        {
            maior_1 = *b;
            maior_2 = *c;
        }
        else
        {
            maior_1 = *c;
            maior_2 = *b;
        }
    }

    if (*d > *a && *c > *b)
    {
        if (*d > *c)
        {
            maior_1 = *d;
            maior_2 = *c;
        }
        else
        {
            maior_1 = *c;
            maior_2 = *d;
        }
    } //fim do maior

    //inicio do menor
    if (*a < *c && *b < *d && *b < *c)
    {
        if (*a < *b)
        {
            menor_1 = *a;
            menor_2 = *b;
        }
        else
        {
            menor_1 = *b;
            menor_2 = *a;
        }
    }

    if (*a < *b && *c < *d && *c < *b)
    {
        if (*a < *c)
        {
            menor_1 = *a;
            menor_2 = *c;
        }
        else
        {
            menor_1 = *c;
            menor_2 = *a;
        }
    }

    if (*a < *b && *d < *c && *d < *b)
    {
        if (*a < *d)
        {
            menor_1 = *a;
            menor_2 = *d;
        }
        else
        {
            menor_1 = *d;
            menor_2 = *a;
        }
    }

    if (*b < *a && *c < *d && *c < *a)
    {
        if (*b < *c)
        {
            menor_1 = *b;
            menor_2 = *c;
        }
        else
        {
            menor_1 = *c;
            menor_2 = *b;
        }
    }

    if (*d < *a && *c < *b && *c < *a)
    {
        if (*d < *c)
        {
            menor_1 = *d;
            menor_2 = *c;
        }
        else
        {
            menor_1 = *c;
            menor_2 = *d;
        }
    } //fim do menor

    soma = maior_1 + maior_2;
    subtrai = menor_1 - menor_2;
    multplica = soma * subtrai;

    printf("%d + %d = %d\n", maior_1, maior_2, soma);
    printf("%d - %d = %d\n", menor_1, menor_2, subtrai);
    printf("%d * %d = %d\n", soma, subtrai, multplica);
}