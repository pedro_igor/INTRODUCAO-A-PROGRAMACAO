#include <stdio.h>

int main()
{
    int vet[10];

    int nro;

    for(int i = 0; i < 10; i ++ )
    {
        printf("Digite um valor: ");
        scanf("%d", &nro);

        vet[i] = nro;
    }

    int soma = 0;

    for(int i = 0; i < 10; i++)
    {
        if(vet[i] % 2 != 0)
        {
            soma += vet[i];
        }
    }

    printf("\nSoma dos numeros impares do vetor: %d\n", soma);

    return 0;
}