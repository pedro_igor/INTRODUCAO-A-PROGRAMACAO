#include <stdio.h>

#define TAM 5

int main()
{
    int vet[TAM];
    int vet2[TAM];

    int nro;
    int maior = 0;
    //lendo numeros
    for (int i = 0; i < TAM; i++)
    {
        printf("Insira um valor %d: ", i +1);
        scanf("%d", &nro);

        vet[i] = nro;
    }

    for (int i = 0; i < TAM; i++)
    {
        if (vet[i] > maior)
        {
            maior = vet[i];
        }
    }

    printf("\nMaior: %d\n", maior);

    for (int i = 0; i < TAM; i++)
    {
        vet2[i] = vet[i] * maior;

        printf("%d ", vet2[i]);
    }

    printf("\n");
    return 0;
    
}