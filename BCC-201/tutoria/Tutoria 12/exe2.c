#include <stdio.h>

#define MAX 10

int main()
{
    int n, nro;
    int vet[MAX];

    printf("Tamanho do Vetor: ");
    scanf("%d", &n);

    for (int i = 0; i < n; i++)
    {
        printf("Número na posição %d: ", i + 1);
        scanf("%d", &nro);

        while (nro < 0)
        {
            printf("Digite um valor inteiro\n");
            printf("Número na posição %d: ", i + 1);
            scanf("%d", &nro);
        }

        vet[i] = i;
    }

    return 0;
}