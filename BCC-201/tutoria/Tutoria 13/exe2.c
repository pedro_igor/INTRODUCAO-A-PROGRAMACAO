#include <stdio.h>
#include <stdlib.h>

#define M 2
#define N 3

int main()
{
    double vet_A[N][M];
    double vet_B[M][N];

    //ler as matriz A
    system("clear");
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            printf("Digite o valor da matriz A [%d][%d]: ", i + 1, j + 1);
            scanf("%lf", &vet_A[i][j]);
        }
    }

    printf("\n");

    //ler a matriz B
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            printf("Digite o valor da matriz B [%d][%d]: ", i + 1, j + 1);
            scanf("%lf", &vet_B[i][j]);
        }
    }

    double vet_C[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            vet_C[i][j] = 0;
            for (int k = 0; k < M; k++)
            {
                vet_C[i][j] += vet_A[i][k] * vet_B[k][j];
            }
        }
    }

    printf("\n");
    printf("\n");
    printf("Matriz C: \n");

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            printf("%4.0lf ", vet_C[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    return 0;
}