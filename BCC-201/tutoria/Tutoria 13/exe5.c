#include <stdio.h>
#include <stdlib.h>

#define MAX 4

int main()
{
    int vet[MAX];
    int nro;

    system("reset");

    printf("Digite um numro: ");
    scanf("%d", &nro);

    while (nro < 999 || nro > 9999)
    {
        printf("Digite um numro: ");
        scanf("%d", &nro);
    }

    int numero = nro;

    vet[0] = nro / 1000;
    nro -= vet[0] * 1000;
    vet[1] = nro / 100;
    nro -= vet[1] * 100;
    vet[2] = nro / 10;
    nro -= vet[2] * 10;
    vet[3] = nro;

    int final = MAX - 1;

    for(int i = 0; i < MAX; i++)
    {
        printf("%d ", vet[i]);
    }
    printf("\n");

    for (int i = 0; i < (MAX / 2); i++, final--)
    {
        if (vet[i] != vet[final])
        {   
            printf("%d não é palindromo\n", numero);
            return 0;
        }
    }

    printf("%d é palindromo\n", numero);

    return 0;
}