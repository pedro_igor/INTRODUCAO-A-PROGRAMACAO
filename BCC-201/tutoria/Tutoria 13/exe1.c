#include <stdio.h>
#include <stdlib.h>

#define LIN 2
#define COL 3

int main()
{
    double matrix_1[LIN][COL];
    double matrix_2[LIN][COL];
    double matrix_SOMA[LIN][COL];

    system("reset");

    //ler matriz 1
    for (int i = 0; i < LIN; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            printf("Digite a matriz na posição [%d][%d]: ", i + 1, j + 1);
            scanf("%lf", &matrix_1[i][j]);
        }
    }

    //ler a matriz 2
    for (int i = 0; i < LIN; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            printf("Digite a matriz na posição [%d][%d]: ", i + 1, j + 1);
            scanf("%lf", &matrix_2[i][j]);
        }
    }

    //faz a soma
    for (int i = 0; i < LIN; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            matrix_SOMA[i][j] = matrix_1[i][j] + matrix_2[i][j];
        }
    }

    system("reset");

    printf("\n");
    printf("\n");

    //imprime as matrizes
    printf("Matriz 1: \n");
    for (int i = 0; i < LIN; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            printf("%3.1lf ", matrix_1[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriz 2: \n");
    for (int i = 0; i < LIN; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            printf("%3.1lf ", matrix_2[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriz soma: \n");
    for (int i = 0; i < LIN; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            printf("%3.1lf ", matrix_SOMA[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    return 0;
}