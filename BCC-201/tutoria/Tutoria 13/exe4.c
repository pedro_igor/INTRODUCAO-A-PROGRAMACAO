#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int main()
{
    int matrix[MAX][MAX];
    system("reset");

    for (int i = 0; i < MAX; i++)
    {
        for (int j = 0; j < MAX; j++)
        {
            scanf("%d", &matrix[i][j]);
        }
    }

    //fazendo as operações

    for (int i = 0; i < MAX; i++)
    {
        for (int j = 0; j < MAX; j++)
        {
            if (i < j)
            {
                matrix[i][j] = (2 * 1) + (7 * j) - 2;
            }
            else if (i == j)
            {
                matrix[i][j] = (3 * 1 * 2) - 1;
            }
            else
            {
                matrix[i][j] = (4 * i * 3) - (5 * j * 2) + 1;
            }
        }
    }

    //imprimindo a matriz
    for (int i = 0; i < MAX; i++)
    {
        for(int j = 0; j < MAX; j++)
        {
            printf("%3d ", matrix[i][j]);
        }
        printf("\n");
    }

    return 0;
}