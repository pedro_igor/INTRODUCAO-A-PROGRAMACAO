#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define TAM 4

int main()
{
    int matriz[TAM][TAM];

    int maior = INT_MIN;
    int linha, coluna;

    system("reset");

    for (int i = 0; i < TAM; i++)
    {
        for (int j = 0; j < TAM; j++)
        {
            printf("Digite o valor de matriz na posição [%d][%d]: ", i + 1, j + 1);
            scanf("%d", &matriz[i][j]);
        }
    }

    printf("\n");
    for (int i = 0; i < TAM; i++)
    {
        for (int j = 0; j < TAM; j++)
        {
            printf("%2d ", matriz[i][j]);
        }
        printf("\n");
    }

    for (int i = 0; i < TAM; i++)
    {
        for (int j = 0; j < TAM; j++)
        {
            if (matriz[i][j] > maior)
            {
                maior = matriz[i][j];
                linha = i + 1;
                coluna = j +1;
            }
        }
    }

    printf("\n");

    printf("O maior valor é %d, foi encontrado na linha: %d e na coluna: %d \n", maior, linha, coluna);

    return 0;
}