#include <stdio.h>

int main()
{
    int cont = 0, i = 0;
    int idade;

    do
    {

        printf("%dª Digite a sua idade: \n", i + 1);
        scanf("%d", &idade);

        if (idade >= 18)
            cont++;

        i++;

    } while (i < 10);

    printf("\n%d tem idade maior ou igual a 18 anos\n", cont);

    return 0;
}