#include <stdio.h>

int main()
{
    char op;
    double saldo = 0.00, saque, deposito;

    do
    {
        printf("\nCaixa eletronico:\n");
        printf("(a) Consutar saldo\n");
        printf("(b) Saque\n");
        printf("(c) Depósito\n");
        printf("(d) sair\n");

        scanf("\n%c", &op);

        switch (op)
        {
        case 'a':

            printf("\nSaldo\n");
            printf("R$ %.2lf\n", saldo);
            break;

        case 'b':

            printf("\nSaque\n");
            printf("Digite o valor que voce que sacar:\nR$");
            scanf("%lf", &saque);

            if (saldo <= 0)
            {
                printf("Não é possivel realizar a operação\n");
                printf("R$ %.2lf\n", saldo);
            }
            else if ((saldo - saque) <= 0)
            {
                printf("Não é possivel realizar a operação\n");
                printf("R$ %.2lf\n", saldo);
            }
            else
            {
                saldo -= saque;
                printf("Operação faita com sucesso.\n");
                printf("R$ %.2lf\n", saldo);
            }

            break;

        case 'c':

            printf("\nDeposito\n");

            printf("Qual é o valor que deseja depositar:\nR$ ");
            scanf("%lf", &deposito);

            saldo += deposito;

            printf("Operação faita com sucesso.\n");
            printf("R$ %.2lf\n", saldo);

            break;

        case 'd':

            printf("Sair\n");
            break;

        default:

            printf("Erro\n\n");
            break;
        }

    } while (op != 'd');

    return 0;
}