#include <stdio.h>

int main()
{
    int nro_aluno;
    double nota, soma = 0;
    double media;
    int i = 0;

    do
    {
        i++;
        printf("Digite a nota do aluno %d (ou -1 para sair): ", i);
        scanf("%lf", &nota);
        if (nota >= 0)
            soma += nota;

    } while (nota != -1);

    nro_aluno = i;

    media = soma / nro_aluno;

    printf("A media da nota da turma = %.2lf\n", media);

    return 0;
}