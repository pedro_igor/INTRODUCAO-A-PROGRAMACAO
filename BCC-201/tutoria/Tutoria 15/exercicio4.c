#include <stdio.h>
#include <string.h>
#include <math.h>

#define TAM 10

int main()
{
    int vet[TAM];
    int soma = 0;
    double media;

    for (int i = 0; i < TAM; i++)
    {
        printf("Digite o valor na posição [%d]: ", i + 1);
        scanf("%d", &vet[i]);

        soma += vet[i];
    }

    media = soma / TAM;

    double somatorio = 0;

    for (int i = 0; i < TAM; i++)
    {
        somatorio += pow((vet[i] - media), 2);
    }

    double desvio_pad = sqrt((1 / (TAM - 1.0)) * somatorio);

    printf("O desvio padrão é: %.2lf\n", desvio_pad);

    return 0;
}