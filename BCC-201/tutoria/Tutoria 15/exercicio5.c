#include <stdio.h>
#include <string.h>

#define TAM 48

char inverte(char *palavra);
void readStr(char *palavra);

int main()
{
    char palavra[TAM], palavra2[TAM], palavra3[TAM];
    char cpypalavra[TAM], cpypalavra2[TAM], cpypalavra3[TAM];

    readStr(palavra);
    readStr(palavra2);
    readStr(palavra3);

    cpypalavra = inverte(palavra);


    return 0;
}

char inverte(char *palavra)
{
    int tamanho = strlen(palavra) - 1;
    palavra[tamanho] = '\0';

    char palavra2[TAM];
    int j = tamanho;

    for(int i = 0; i <= tamanho; i++, j--)
    {
        palavra2[i] = palavra[j];
    }

    return palavra2;
}

void readStr(char *palavra)
{
    printf("Qual é a string:");
    fgets(palavra, TAM, stdin);
}