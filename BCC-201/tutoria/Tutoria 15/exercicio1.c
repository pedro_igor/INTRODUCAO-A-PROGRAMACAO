#include <stdio.h>
#include <string.h>

int main()
{
    char frase[100];
    char cesar[100];

    printf("Digite uma Frase: ");
    fgets(frase, 99, stdin);

    int tamanho = strlen(frase) - 1;
    frase[tamanho] = '\0';

    int j = 0;

    for (int i = 0; i < tamanho; i++, j++)
    {
        if (frase[j] == ' ')
        {
            cesar[i] = frase[j];
        }
        else
        {
            if (frase[j] >= 65 && frase[j] <= 90)
            {
                cesar[i] = frase[j] + 3;
            }
            else if (frase[j] >= 97 && frase[j] <= 122)
            {
                cesar[i] = (frase[j] + 3) - 32;
            }
            else
            {
                cesar[i] = frase[j];
            }
        }
    }

    printf("%s\n", cesar);

    return 0;
}