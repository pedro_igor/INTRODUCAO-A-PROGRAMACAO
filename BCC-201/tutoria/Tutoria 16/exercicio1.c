#include <stdio.h>

typedef struct
{
    char nome[100];
    int matricula;
    char diciplina[100];
    double nota1[3];
    double media;
} Aluno;

void ler_dados(Aluno *aluno)
{
    printf("Digite o nome do aluno: ");
    fgets((*aluno).nome, 100, stdin);

    printf("Digite a matricula do aluno: ");
    scanf("%d", &aluno->matricula);

    printf("Digite a diciplina: ");
    scanf("%s", &(*aluno).diciplina);

    printf("Digite as notas: ");
    for (int i = 0; i < 3; i++)
    {
        printf("Digite a %dª nota: ", i + 1);
        scanf("%lf", &(*aluno).nota1[i]);
    }
}

double media(Aluno aluno)
{
    aluno.media = 0;
    for (int i = 0; i < 3; i++)
    {
        aluno.media += aluno.nota1[i];
    }

    double media = aluno.media/3;

    return media;
}

void imprimir(Aluno aluno)
{
    printf("Nome: %s\n", aluno.nome);

    printf("Matricula: %d\n", aluno.matricula);

    printf("Diciplina: %s\n", aluno.diciplina);

    printf("Notas: ");
    for (int i = 0; i < 3; i++)
    {
        printf("%.2lf ", aluno.nota1[1]);
    }

    double media1 = media(aluno);
    printf("Media: ", &media1);
}

void editar(Aluno *aluno)
{
    printf("Qual nota que voce quer editar? ");
    puts("\n");
    printf("1. nome\n");
    printf("2. MAtricula\n");
    printf("3. Diciplina\n");
    printf("4. notas\n");

    int op;

    switch (op)
    {
        case 1:
            printf("Digite o nome do aluno: ");
            fgets((*aluno).nome, 100, stdin);
            break;

        case 2:
            printf("Digite a matricula do aluno: ");
            scanf("%d", &(*aluno).matricula);
            break;

        case 3:
            printf("Digite a diciplina: ");
            scanf("%s", &(*aluno).diciplina);
            break;

        case 4:
            printf("Digite as notas: ");
            for (int i = 0; i < 3; i++)
            {
                printf("Digite a %dª nota: ", i + 1);
                scanf("%lf", &(*aluno).nota1[i]);
            }
            break;

        default:
            printf("erro");
            break;
    }
}
