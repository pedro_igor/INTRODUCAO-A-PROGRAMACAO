#include <stdio.h>

typedef struct data
{
    int dia;
    int mes;
    int ano;
} Data;

int main()
{
    Data data;

    int duracao;

    printf("Qual é a data de inicio do evento no formato dd/mm/aaa: ");
    scanf("%d %d %d", &data.dia, &data.mes, &data.ano);

    if (data.mes < 1 || data.mes > 12)
    {
        while (data.mes < 1 || data.mes > 12)
        {
            printf("Digite o mes novamente: ");
            scanf("%d", &data.mes);
        }
    }

    if (data.mes == 1 || data.mes == 3 || data.mes == 5 || data.mes == 7 || data.mes == 8 || data.mes == 10 || data.mes == 12)
    {
        while (data.dia < 1 || data.dia > 30)
        {
            printf("Didite o dia novamente: ");
            scanf("%d", &data.dia);
        }
    }
    else if (data.mes == 4 || data.mes == 6 || data.mes == 9 || data.mes == 11)
    {
        while (data.dia < 1 || data.dia > 30)
        {
            printf("Didite oo dia novamente: ");
            scanf("%d", &data.dia);
        }
    }
    else if (data.mes == 2)
    {
        while (data.dia < 1 || data.dia > 30)
        {
            printf("Didite o dia novamente: ");
            scanf("%d", &data.dia);
        }
    }

    printf("Qual é a duração do evento: ");
    scanf("%d", &duracao);

    data.dia += duracao;

    while (data.dia >= 30)
    {
        //data.dia -= 30;
        if (data.mes == 12 && data.dia > 30)
        {
            data.ano++;
            data.mes = 1;
        }
        else
            data.mes++;

        data.dia -= 30;
    }

    printf("O evento durará até: %.2d/%.2d/%d.\n", data.dia, data.mes, data.ano);

    return 0;
}
