#include <stdio.h>
#include <math.h>

typedef struct
{
    int x, y;
} Ponto;

int main()
{
    Ponto ponto1, ponto2;

    printf("Digite os valores de x e y do ponto 1: ");
    scanf("%d %d", &ponto1.x, &ponto1.y);

    printf("Digite os valores de x e y do ponto 2: ");
    scanf("%d %d", &ponto2.x, &ponto2.y);

    double distancia = sqrt(pow((ponto2.x - ponto1.x), 2) + pow((ponto2.y - ponto1.y), 2));

    printf("A distancias entre dois pontos é: %.2lf.\n", distancia);
    return 0;
}
