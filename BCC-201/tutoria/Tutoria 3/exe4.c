//exercici 4
#include <stdio.h>

int menor(int nro1, int nro2);

int main()
{
    int x, y;

    printf("Digite dois numeros: ");
    scanf("%d %d", &x, &y);

    int menor1 = menor(x, y);

    printf("O menor nemero é: %d\n", menor1);

    return 0;
}

int menor(int nro1, int nro2)
{
    if (nro1 < nro2)
    {
        return nro1;
    }
    else if (nro2 < nro1)
    {
        return nro2;
    }
}