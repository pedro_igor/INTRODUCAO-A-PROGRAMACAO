//exercicio 1
#include <stdio.h>

void positive(int nro1);

int main()
{
    int x;

    printf("Digite um numero: \n");
    scanf("%d", &x);

    positive(x);

    return 0;
}

void positive(int nro1)
{
    if (nro1 > 0)
        printf("O numero é positivo\n");
    else
        printf("O numero não é positivo\n");
}