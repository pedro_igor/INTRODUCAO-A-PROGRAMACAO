//exercicio 5
//conversão de temperaturas
#include <stdio.h>
#include "exe5_t2c.h"
#include "exe5_c2f.h"

int main()
{
    char op;
    double temp;
    double temp_C, temp_F;
    
    printf("Para qual temperatura voce quer converter?\n");
    printf("c para Celsius ou f para farenheint: ");
    scanf("%c", &op);

    printf("Digite a Temperatura:");
    scanf("%lf", &temp);

    switch (op)
    {
        case 'c':
            temp_C = farenheint_to_celsius(temp);
            printf("A tempeatura equivalente de Farenheit em Celsius é: %.2lf\n", temp_C);

            break;

        case 'f':
            temp_F = celsius_to_farienheit(temp);
            printf("A temperatura equivalente de Celsius em Farenheit é: %.2lf\n", temp_F);

            break;
    
        default:
            printf("ERRO: Opção invalida\n");
            break;
    }

}