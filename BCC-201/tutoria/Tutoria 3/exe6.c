//exercicio  6
#include <stdio.h>

double media(double nt1, double nt2, double nt3);
double media2(double nt1, double nt2, double nt3);
double maior(double nt1, double nt2, double nt3);
double menor(double nt1, double nt2, double nt3);

int main()
{
    double nt1, nt2, nt3;
    double med2, med3, maior1, menor1;

    printf("Digite as tres notas: ");
    scanf("%lf %lf %lf", &nt1, &nt2, &nt3);

    med2 = media2(nt1, nt2, nt3);
    med3 = media(nt1, nt2, nt3);
    maior1 = maior(nt1, nt2, nt3);
    menor1 = menor(nt1, nt2, nt3);

    printf("A media das 3 notas: %.2lf\n", med3);
    printf("A media das 2 maiores notas: %.2lf\n", med2);
    printf("A maior nota: %.2lf \n", maior1);
    printf("A menor nota: %.2lf\n", menor1);

    return 0;
}

double media(double nt1, double nt2, double nt3)
{
    double med = (nt1 + nt2 + nt3) / 3;

    return med;
}

double media2(double nt1, double nt2, double nt3)
{
    if (nt1 > nt3 && nt2 > nt3)
    {
        return (nt1 + nt2) / 2;
    }
    else if (nt1 > nt2 && nt3 > nt2)
    {
        return (nt1 + nt3) / 2;
    }
    else if (nt2 > nt1 && nt3 > nt1)
    {
        return (nt2 + nt3) / 2;
    }
}

double maior(double nt1, double nt2, double nt3)
{
    if (nt1 > nt2 && nt1 > nt3)
    {
        return nt1;
    }
    else if (nt2 > nt1 && nt2 > nt3)
    {
        return nt2;
    }
    else if (nt3 > nt1 && nt3 > nt1)
    {
        return nt3;
    }
}

double menor(double nt1, double nt2, double nt3)
{
    if (nt1 < nt2 && nt1 < nt3)
    {
        return nt1;
    }
    else if (nt2 < nt1 && nt2 < nt3)
    {
        return nt2;
    }
    else if (nt3 < nt1 && nt3 < nt1)
    {
        return nt3;
    }
}