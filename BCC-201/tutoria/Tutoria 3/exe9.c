//exercicio 9
#include <stdio.h>

int soma(int nro1, int nro2);
int sub(int nro1, int nro2);
int mult(int nro1, int nro2);
int div(int nro1, int nro2);

int main()
{
    int x, y, resul;
    char op;

    printf("Digite os numeros: ");
    scanf("%d %d", &x, &y);

    printf("Qual é a operação: ");
    getchar();
    scanf("%c", &op);

    if (op == '+')
    {
        resul = soma(x, y);
    }
    else if (op == '-')
    {
        resul = sub(x, y);
    }
    else if (op == '*')
    {
        resul = mult(x, y);
    }
    else if (op == '/')
    {

        resul = div(x, y);
    }
    else
    {
        printf("ERRO\n");
    }

    if (op == '+' || op == '-' || op == '*' || op == '/')
    {
        printf("%d %c %d = %d\n", x, op, y, resul);
    }
}

int soma(int nro1, int nro2)
{
    return nro1 + nro2;
}

int sub(int nro1, int nro2)
{
    return nro1 - nro2;
}

int mult(int nro1, int nro2)
{
    return nro1 * nro2;
}

int div(int nro1, int nro2)
{
    return nro1 / nro2;
}