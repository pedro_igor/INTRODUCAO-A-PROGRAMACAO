//exercicio 1
#include <stdio.h>

int main()
{
    float cg, ca, v, t, rg /*rendimento usando gasolina*/, pg, pa, ra /*rendimento usando alcool*/;

    printf("CUSTO DE COMBUSTIVEL EM UMA VIAGEM\n");

    printf("VELOCIDADE MÉDIA (km/h): ");
    scanf("%f", &v);

    printf("TEMPO DO PERCUSO (h): ");
    scanf("%f", &t);

    printf("RENDIMENTO DA GASOLINA (KM/litro): ");
    scanf("%f", &rg);

    printf("PREÇO DO LITRO DE GASOLINA (R$): ");
    scanf("%f", &pg);

    printf("PREÇO DO LITRO DE ALCOOL (R$): ");
    scanf("%f", &pa);

    cg = ((v * t) / rg) * pg;
    ra = 0.7 * rg;
    ca = ((v * t) / ra) * pa;

    printf("\n");
    printf("CUSTO USANDO GASOLINA = R$ %.2f\n", cg);
    printf("CUSTO USANDO ALCOOL = R$ %.2f\n\n", ca);

    return 0;
}