//exercicio 2
#include <stdio.h>

int main()
{
    int a, b, c;

    printf("Valor de A: ");
    scanf("%d", &a);

    printf("Valor de B: ");
    scanf("%d", &b);

    if (a == b)
    {
        c = a + b;
    }
    else if (a != b)
    {
        c = a * b;
    }

    printf("C = %d\n", c);

    return 0;
}