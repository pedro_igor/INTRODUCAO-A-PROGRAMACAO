//exercicio 3
#include <stdio.h>

int main()
{
    int a, b, c;
    printf("Digite os lados do triangulo:");
    scanf("%d %d %d", &a, &b, &c);

    if ((a + b) > c && (a + c) > c && (b + c) > a)
    {
        printf("Respeita a Restrição\n");
    }
    else
    {
        printf("Não respeita a restrição\n");
    }

    return 0;
}