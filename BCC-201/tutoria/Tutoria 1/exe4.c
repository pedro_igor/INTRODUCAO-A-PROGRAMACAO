//exercicio 4
#include <stdio.h>
#include <math.h>

int main()
{
    int a, b, c, delta;

    scanf("%d %d %d", &a, &b, &c);

    delta = pow(b, 2) - (4 * a * c);

    if (delta < 0)
    {
        printf("A equação nao possui raizes reais\n");
    }
    else if (delta == 0)
    {
        printf("A equação possui uma raiz real\n");
    }
    else if (delta > 0)
    {
        printf("A equação possui duas raizes reais distintas\n");
    }

    return 0;
}