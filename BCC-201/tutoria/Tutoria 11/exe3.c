#include <stdio.h>

int main()
{
    double preco_final;
    int parcelas;
    double valor_das_parcelas;
    double preco_parcelado;
    double vista;

    printf("QUAL É O PREÇO FINAL DO CARRO: ");
    scanf("%lf", &preco_final);

    int i;
    double j;

    printf("\nQUANTIDADES\tPERCENTUAL DE ACRÉCIMO\n");
    printf("\nDE PARCELAS\tSOBRE O PREÇO FINAL\n\n");

    printf("Á vista\t");
    vista = preco_final - (preco_final * 0.20);
    printf("%6.2lf\n", vista);

    for (i = 6, j = 0.03; i <= 60; i += 6, j += 0.03)
    {

        printf("%6d\t", i);
        preco_parcelado = preco_final + (preco_final * j);

        printf("%.2lf\n", preco_parcelado / i);
    }

    return 0;
}