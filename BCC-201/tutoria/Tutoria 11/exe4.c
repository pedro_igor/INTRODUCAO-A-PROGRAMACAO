#include <stdio.h>

int main()
{
    int soma = 0;
    int cont = 0, i = 0;
    int idade;

    do
    {
        i++;
        printf("DIGITE A IDADE DA %d:\n", i);
        scanf("%d", &idade);
        if(idade > 0)
        {
            soma += idade;
            cont++;
        }
    } while (idade != 0 && idade > 0);

    double media = soma / cont;

    printf("A media da idade das %d é: %.2lf\n", cont, media);

    return 0;
}