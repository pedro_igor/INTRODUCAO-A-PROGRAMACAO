#include <stdio.h>

int main()
{
    int cont = 0;
    int n;

    for(int i = 0; i < 10; i++)
    {
        printf("Digite um numero: ");
        scanf("%d", &n);

        if(n >= 30 && n <= 90)
        {
            cont++;
        }
    }

    printf("Há %d numeros entre 30 e 90.\n", cont);

    return 0;
}