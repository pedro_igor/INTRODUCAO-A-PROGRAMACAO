#include <stdio.h>

int main()
{
    const  double taxas = 200.00;
    int ingr = 120;
    double lmax = 0;
    double lucro = 0;

    printf("Preço \t Ingresos \t Lucro\n");
    for(double i = 5; i >= 1; i -= 0.5)
    {
        lucro = (i * ingr) - taxas;

        if(lucro > lmax)
        {
            lmax = lucro;
        }
        printf("%.2lf \t %d \t %.2lf\n", i, ingr, lucro);

        ingr += 26;
    }
    printf("Lucro maximo: %.2lf\n", lmax);

    return 0;
}