#include <stdio.h>
#include <stdlib.h>

int main()
{
    double produtos[5] = {5, 15, 9, 20, 34};
    int nro_cliente;
    double **matriz;

    printf("Digite o numeros de cliente: ");
    scanf("%d", &nro_cliente);

    matriz = malloc(nro_cliente * sizeof(double*));
    
    for(int i = 0; i < nro_cliente; i++)
    {
        matriz[i] = malloc(5 * sizeof(double));
    }

    for (int i = 0; i < nro_cliente; i++)
    {
        for(int j = 0; j < 5; j++)
        {
            printf("Digite a quantida do %dº produto: ", j +1);
            scanf("%lf", &matriz[i][j]);
        }
        puts("\n");
    }

    double soma;
    for(int i = 0; i < nro_cliente; i++)
    {
        soma = 0;

        for(int j = 0; j < 5; j++)
        {
            soma += matriz[i][j] * produtos[j];
        }
        printf("valor total: %.2lf.\n", soma);
    }
    
    for(int i = 0; i < nro_cliente; i++)
    {
        free(matriz[i]);
    }

    free(matriz);
    return 0;
}