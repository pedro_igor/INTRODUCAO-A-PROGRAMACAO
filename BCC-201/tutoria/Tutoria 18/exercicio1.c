#include <stdio.h>
#include <stdlib.h>

typedef struct 
{
    char nome[100];
    int mat;//matricula
    double **notas;
}Dados;

Dados **aloca_matriz(int nro_prova, int nro_disciplina);
void media(Dados *aluno, int nro_prova, int nro_disciplina,int nro_alunos);

int main()
{
    Dados *aluno;
    int nro_diciplinas,nro_provas, nro_alunos;
    double media1;

    printf("Digite os numeros de alunos:");
    scanf("%d",&nro_alunos);

    aluno = malloc(nro_alunos * sizeof(Dados));

    printf("Digite o numeros de provas: ");
    scanf("%d",&nro_provas);

    printf("digite o numeros de diciplinas: ");
    scanf("%d,",&nro_diciplinas);

    for(int i = 0; i < nro_alunos; i++)
    {
        getchar();
        printf("Digite o nome do aluno: ");
        fgets(aluno[i].nome,101,stdin);
        printf("Digite a matricula: ");
        scanf("%d", &aluno[i].mat);
        aluno[i].notas = aloca_matriz(nro_provas, nro_diciplinas);
    }

    media(aluno,nro_provas,nro_diciplinas,nro_alunos);

    for(int i = 0; i < nro_alunos; i++)
    {
        for(int j = 0; j < nro_diciplinas; j++ )
        {
        free(aluno[i].notas[j]);
        }
        free(aluno[i].notas);
    }

    free(aluno);

    return 0;
}

Dados **aloca_matriz(int nro_prova, int nro_disciplina)
{
    Dados **matriz;//pxd

    matriz = malloc(nro_disciplina * sizeof(Dados*));

    if(matriz == NULL)
    {
        printf("Memoria insuficiente.\n");
        return 1;
    }

    for(int i = 0; i < nro_disciplina; i++)
    {
        matriz[i] = malloc(nro_prova * sizeof(Dados));
    }

    for(int i = 0; i < nro_disciplina; i++)
    {
        for(int j = 0; j < nro_prova; j++)
        {
            printf("Digite a nota do %d aluno: ",i +1);
            scanf("%lf", matriz[i][j]);
        }
    }

    return matriz;
}

void media(Dados *aluno, int nro_prova, int nro_disciplina,int nro_alunos)
{
    double med = 0.0;
    for(int k = 0; k < nro_alunos; k++)
    {
        for(int i = 0; i < nro_disciplina; i++)
        {
            for(int j = 0; j < nro_prova; j++)
            {
                med += aluno[k].notas[i][j];
            }
            printf("Media do aluno %s: %.2lf.\n", aluno[k].nome, med/nro_prova);
        }
    }

}