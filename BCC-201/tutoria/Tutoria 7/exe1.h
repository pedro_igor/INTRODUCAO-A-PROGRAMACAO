#ifndef EXE1_H
#define EXE1_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define PI 3.14159265359

double area_circulo();
double area_hex(void);
double area_triangulo(void);
double area_quadrado(void);
void menu();
void escolha(int *opcao);

#endif