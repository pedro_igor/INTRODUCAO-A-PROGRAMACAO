/*1 - Crie um programa que calcule a área de figuras. O programa dele inserir uma opção de figura: 
A- círculo, B- hexágono, C- triângulo, D- quadrado. Em seguida, o programa deve pedir ao usuário os dados necessários para o cálculo da área da figura pedida 
( A inserção dos dados para calcular a área devem ser feitas dentro da função de cálculo, pois para cada área os dados de entrada são diferentes...)*/
#include "exe1.h"

double area_circulo()
{
    double raio;

    printf("\nDigite o Raio do circulo: ");
    scanf("%lf", &raio);

    double area = PI * pow(raio, 2);
    return area;
}

double area_hex(void)
{
    double lado;

    printf("\nDigite o lado do hexagono: ");
    scanf("%lf", &lado);

    double area = (6 * pow(lado, 2) * sqrt(3)) / 4;
    return area;
}

double area_triangulo(void)
{
    double base, altura;

    printf("\nDigite a base do Triangulo: ");
    scanf("%lf", &base);

    printf("\nDigite a altura do triangulo: ");
    scanf("%lf", &altura);

    double area = ((base) * (altura)) / 2;
    return area;
}

double area_quadrado(void)
{
    double lado;

    printf("\nDigite o lado do quadrado: ");
    scanf("%lf", &lado);

    double area = pow(lado, 2);
    return area;
}

void menu()
{
    system("clear");

    printf("\n\tCALCULO DE ÁREA");
    printf("\n\n");
    printf("---------------------\n");
    printf("1 - Área do circulo  \n");
    printf("---------------------\n");
    printf("2 - Área do hexagono \n");
    printf("---------------------\n");
    printf("3 - Área do triangulo\n");
    printf("---------------------\n");
    printf("4 - Área do quadrado \n");
    printf("---------------------\n");
    printf("\n\n");

    int op;

    printf("Escolha uma opção:");
    scanf("%d", &op);

    escolha(&op);
}

void escolha(int *opcao)
{
    double area;

    if (*opcao > 4)
    {
        printf("ERRO: OPÇÃO INVALIDA\n");
        exit(0);
    }
    else
    {
        switch (*opcao)
        {
            case 1:

                printf("\nAREA DO CIRCULO\n");
                area = area_circulo();
                printf("\n A area do circulo é: %.2lf\n", area);

                break;

            case 2:

                printf("\nAREA DO HEAXGONO\n");
                area = area_hex();
                printf("\n A area do Hexagono é: %.2lf\n", area);

                break;

            case 3:

                printf("\nÁREA DO TRIANGULO\n");
                area = area_triangulo();
                printf("\n A area do triangulo é: %.2lf\n", area);

                break;

            case 4:

                printf("\nAREA DO QUADRADO\n");
                area = area_quadrado();
                printf("\n A area do Quadrado é: %.2lf\n", area);

                break;
        }//fim do switch
    }//fim do else
}