#include <stdio.h>
#include <stdlib.h>

int hour_to_seconds(int hour)
{
    int seconds = hour * 3600;
    return seconds;
}

int main()
{
    int horas, segundos;
    system("clear");

    printf("Entre com os horas: ");
    scanf("%d", &horas);

    segundos = hour_to_seconds(horas);

    printf("\n%d horas em segundos = %ds\n", horas, segundos);

    return 0;
}