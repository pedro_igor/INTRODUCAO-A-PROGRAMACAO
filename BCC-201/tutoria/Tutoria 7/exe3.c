#include <stdio.h>
#include <stdlib.h>

void  converter_min(int h1, int m1, int h2, int m2, int* minutos);

int main()
{
    int horas1, minutos1, horas2, minutos2, minutos;

    system("clear");

    printf("Digite horas e os minutos do inicio do jogo: ");
    scanf("%d %d", &horas1, &minutos1);

    printf("Digite horas e minutos do temino do jogo: ");
    scanf("%d %d", &horas2, &minutos2);

    converter_min(horas1, minutos1, horas2, minutos2, &minutos);

    printf("inicio: %d:%.2d\n", horas1, minutos1);
    printf("termino: %d:%.2d\n", horas2, minutos2);
    printf("Tempo decorrido em minutos: %d\n", minutos);
    

    return 0;
}

void converter_min(int h1, int m1, int h2, int m2, int* minutos)
{
    int tempo_inicio = (h1 * 60) + m1 ;
    int tempo_fim = (h2 * 60) + m2;

    if (tempo_inicio < tempo_fim)
    {
        *minutos = tempo_fim - tempo_inicio;
    }
    else
    {
        *minutos = tempo_inicio - 1440 - (tempo_fim); //1440min = 24h
    }

    if(*minutos < 0)
    {
        *minutos = *minutos *(-1);
    } 
}