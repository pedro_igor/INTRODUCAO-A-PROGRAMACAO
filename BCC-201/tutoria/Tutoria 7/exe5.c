#include <stdio.h>
#include <stdlib.h>

double meters_to_centimeters(double meters);
double centimeters_to_milometers(double centimeters);
void menu();
void escolha(int *opcao);

int main()
{
    menu();

    return 0;
}

double meters_to_centimeters(double meters)
{
    return meters * 100;
}

double centimeters_to_milometers(double centimeters)
{
    return centimeters * 10;
}

void menu()
{
    system("clear");

    printf("-------------------------------\n");
    printf("1 - Metros para cemtimetros    \n");
    printf("-------------------------------\n");
    printf("2 - Centimetros para milimetros\n");
    printf("-------------------------------\n");

    int op;
    escolha(&op);
}

void escolha(int *opcao)
{
    printf("\nDigite uma opção:\n");
    scanf("%d", opcao);

    double meters, centimeters, milometers;

    if (*opcao > 2)
    {
        printf("ERRO\n");
        exit(0);
    }
    else
    {
        switch (*opcao)
        {
            case 1:

                printf("Metros: ");
                scanf("%lf", &meters);
                centimeters = meters_to_centimeters(meters);

                printf("%.2lfm = %.2lfcm\n", meters, centimeters);

                break;

            case 2:

                printf("Centimetros: ");
                scanf("%lf", &centimeters);
                milometers = centimeters_to_milometers(centimeters);

                printf("%.2lfcm = %.2lfmm\n", centimeters, milometers);

                break;
        }
    }

    return 0;
}