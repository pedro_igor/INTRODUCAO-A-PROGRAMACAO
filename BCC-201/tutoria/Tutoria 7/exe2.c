#include <stdio.h>
#include <stdlib.h>

#define PINEAPPLE 1.30  //uni
#define BANANA 1.38     //kg
#define APPLE 1.98      //kg
#define WATERMELON 0.96 //uni
#define ORANGE 1.25     //kg

int menu();

int main()
{
    menu();

    double preco, kilo, din, troco;
    int op, unidade;
    printf("Digite uma opção: ");
    scanf("%d", &op);

    if(op > 6 || op < 1)
    {
        printf("ERRO\n");
        exit(0);
    }
    else
    {
        switch(op)
        {
            case 1:
                printf("Qual é a quantidade de abacaxi: ");
                scanf("%d", &unidade);

                preco = unidade * PINEAPPLE;

                break;

            case 2: 
                printf("Quantos quilos de banana: ");
                scanf("%lf", &kilo);

                preco = BANANA * kilo;

                break;

            case 3: 
                printf("Quantos quilos de maçãs: ");
                scanf("%lf", &kilo);

                preco = APPLE * kilo;

                break; 

            case 4:
                printf("Qual é a quantidade de melancia: ");
                scanf("%d", &unidade);

                preco = WATERMELON * unidade;

                break;

            case 5: 
                printf("Quantos quilos de laranja: ");
                scanf("%lf", &kilo);

                preco = ORANGE * kilo;

                break;
        }//fim do switch

        printf("quanto de dinheiro voce está: \n");
        scanf("%lf", &din);

        if (din > preco)
        {
            troco = din - preco;
            printf("TOTAL: %.2lf\n", preco);
            printf("TROCO: %.2lf\n", troco);
        }
        else
        {
           printf("TOTAL: %.2lf\n", preco); 
        }
        
        
    }

    return 0;
}

int menu()
{
    system("clear");

    printf("\n");
    printf("------------\n");
    printf("1 - ABACAXI \n");
    printf("------------\n");
    printf("2 - BANANA  \n");
    printf("------------\n");
    printf("3 - MAÇÃ    \n");
    printf("------------\n");
    printf("4 - MELANCIA\n");
    printf("------------\n");
    printf("5 - LARANJA \n");
    printf("------------\n\n");
}