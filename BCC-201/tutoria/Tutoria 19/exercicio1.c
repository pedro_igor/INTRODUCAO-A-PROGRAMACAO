#include <stdio.h>
#include <stdlib.h>

typedef struct 
{
    int dia, mes, ano;
}Data;

typedef struct
{
    char nome[30];
    float altura;
    Data nascimento;
}Pessoa;

int main()
{
    Pessoa povo[10];
    Pessoa p;
    int opcao, qtdPessoas = 0;
    Data dia;

    imprimeTaladeOpcoes();

    opcao = escolheOpcao();

    if(opcao == 1)
    {
        p = lePessoaDeTeclado();
        adicionaPessoa(povo, p, qtdPessoas);
    }
    if(opcao == 2)
    {
        imprimeTodasPessoas(povo, qtdPessoas);
    }
    if(opcao == 3)
    {
        dia = leDataDoTeclado();
        imprimeMaisVelhos(povo, qtdPessoas, dia);
    }

}