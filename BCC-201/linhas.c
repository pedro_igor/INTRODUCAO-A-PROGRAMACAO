//converter arquivo para maiúscula

#include <stdio.h>
#include <ctype.h>

int main()
{
	int c;
	char entrada[121]; //*amarzena nome do arquivo de entrada
	char saida[121];   //*amarzena nome do arquivo de saida
	FILE *e;            //*Ponteiro do arquivo de entrada
	FILE *s;            //*Ponteiro do arquivo de saida

	//pede ao usuairio os nomes do arquivo
	printf("Digite o nome do arquivo de entrada: ");
	scanf("%s",entrada);
	printf("Digite o nome do arquivo de saida: ");
	scanf("%s",saida);

	//abre os arquivos para leitura e escrita
	e = fopen(entrada,"r");
	if(e == NULL)
	{
		printf("Nao foi posivel abrir o arquivo  de entrada.\n");
		return 1;
	}
	s = fopen(saida,"w");
	if(s == NULL)
	{
		printf("Não foi possivel abrir o arquivo de saida.\n");
		return 1;
	}

	//Lê  da entrada e escreve na saida
	while((c = fgetc(e)) != EOF)
	{
		fputc(toupper(c),s);
	}

	//fecha os arquivos
	fclose(e);
	fclose(s);

	return 0;
}