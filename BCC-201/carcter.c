/*conta o numeros de linha de um arquivo*/
#include <stdio.h>

int main()
{
	int c;
	int nlinhas = 0; // contador de numeros de linhas
	FILE *fp;

	//abre o arquivo para leitura
	fp = fopen("entrada.txt", "rt");
	if(fp==NULL)
	{
		printf("Não é posivel abrir o arquivo.\n");
		return 1;
	}

	//ler caracter por caracter
	while((c = fgetc(fp)) != EOF)
	{
		if(c == '\n')
			nlinhas++;
	}

	//fecha o arquivo
	fclose(fp);

	//exibe o resultado na tela

	printf("Números de linhas = %d\n",nlinhas);

	return 0;
}