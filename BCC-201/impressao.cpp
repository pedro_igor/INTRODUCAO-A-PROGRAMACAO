#include <iostream>

#define TAM 9

using namespace std;

void impress(char **m)
{
	char b = 'I'; //char b = 'H'
	int a = 1; //int a = 1

	/*for (int i = 0; i < TAM; i++)
		for (int j = 0; j < TAM; j++)
			txt >> m[i][j];*/

	cout << "┌───┬";
	for(int i = 0; i < TAM; i++)
		cout << "───┬";
	cout << "───┐" << endl << "|   | ";

	for (int i = 0; i < TAM; i++, a++)
		cout << a << " | ";

	cout << "  |" << endl << "├───";

	for(int i = 0; i <= TAM; i++)
		cout << "┼───";
	cout << "┤";

	a -= TAM;

	for (int i = 0; i < TAM; i++, b--)
	{
		cout << endl
			 << "| " << b << " | ";

		for (int j = 0; j < TAM; j++)
			if (m[i][j] == '-')
				cout << " "
					 << " | ";
			else
				cout << m[i][j] << " | ";

		cout << b << " |" << endl;
		cout << "├───";
		for(int i = 0; i <= TAM; i++)
			cout << "┼───";

		cout << "┤";
	}

	cout << endl
		 << "|   | ";
	for (int i = 0; i < TAM; i++, a++)
		cout << a << " | ";

	cout << "  |" << endl << "└";

	for(int i = 0; i <= TAM; i++, a++)
		cout << "───┴";
	cout << "───┘" << endl;

}

int main ()
{
	char **matriz;

	matriz = new char*[TAM];
	for(int i = 0; i < TAM; i++)
		matriz[i] = new char[TAM];

	for(int i = 0; i < TAM; i++)
		for(int j = 0; j < TAM; j++)
			matriz[i][j] = ' ';

	impress(matriz);
}