#include <iostream>
#include <stdio.h>
#include <cstdio>
#include <cstring>
#include <fstream>
using namespace std;

int numerosFixos(char matriz[][9], int l, int c, int valor) // verifica se um numero é fixo na matriz base
{
	int referencia = 1; // retorno para quando haver numero fixo na coordenada

	if (matriz[l][c] == '0' || matriz[l][c] == '_')
	{					// compara se a coordenada tem zero ou underline
		referencia = 0; // não há numeros fixos
	}

	return referencia;
}

int ConfereLinha(char matriz[][9], int l, int valor) // para não repetir numeros nas linhas
{
	int referencia = 0; // retorno para quando não ha números repetidos

	for (int i = 0; i < 9; i++)
	{
		if (matriz[l][i] == valor)
		{
			referencia = 1; //retorno para quando há numeros repetidos
			break;
		}
	}

	return referencia;
}

int ConfereColuna(char matriz[][9], int c, int valor) // para não repetir numeros nas colunas
{

	int referencia = 0; // retorno para quando não ha números repetidos
	for (int i = 0; i < 9; i++)
	{
		if (matriz[i][c] == valor)
		{
			referencia = 1; // retorno para quando ha números repetidos
			break;
		}
	}
	return referencia;
}

int confereBlocos(char matriz[][9], int l, int c, int valor) // confere os valores nos blocos, que são identificados de acordo com as coordenadas
{
	int referencia = 0; // retorno para quando não ha numeros repetidos no bloco
	if (l >= 0 && l <= 2 && c >= 0 && c <= 2)
	{ // confere bloco 1 (identificado pela coordenada de entrada)

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
				if (matriz[i][j] == valor)
				{					// percorre o bloco e compara se ha numero repetido
					referencia = 1; // há numeros repetidos (retorna o numero do bloco em que ocorre o erro)
					break;
				}
		}
	}

	else if (l >= 0 && l <= 2 && c >= 3 && c <= 5)
	{ // confere bloco 2

		for (int i = 0; i <= 2; i++)
		{
			for (int j = 3; j <= 5; j++)
				if (matriz[i][j] == valor)
					referencia = 2; // há numeros repetidos no bloco 2
		}
	}

	else if (l >= 0 && l <= 2 && c >= 6 && c <= 8)
	{ // confere bloco 3

		for (int i = 0; i <= 2; i++)
		{
			for (int j = 6; j <= 8; j++)
				if (matriz[i][j] == valor)
					referencia = 3; // há numeros repetidos no bloco 3
		}
	}

	else if (l >= 3 && l <= 5 && c >= 0 && c <= 2)
	{ // confere bloco 4

		for (int i = 3; i <= 5; i++)
		{
			for (int j = 0; j <= 2; j++)
				if (matriz[i][j] == valor)
					referencia = 4; // há numeros repetidos no bloco 4
		}
	}

	else if (l >= 3 && l <= 5 && c >= 3 && c <= 5)
	{ // confere bloco 5

		for (int i = 3; i <= 5; i++)
		{
			for (int j = 3; j <= 5; j++)
				if (matriz[i][j] == valor)
					referencia = 5; // há numeros repetidos no bloco 5
		}
	}

	else if (l >= 3 && l <= 5 && c >= 6 && c <= 8)
	{ // confere bloco 6

		for (int i = 3; i <= 5; i++)
		{
			for (int j = 6; j <= 8; j++)
				if (matriz[i][j] == valor)
					referencia = 6; // há numeros repetidos no bloco 6
		}
	}

	else if (l >= 6 && l <= 8 && c >= 0 && c <= 2)
	{ // confere bloco 7

		for (int i = 6; i <= 8; i++)
		{
			for (int j = 0; j <= 2; j++)
				if (matriz[i][j] == valor)
					referencia = 7; // há numeros repetidos no bloco 7
		}
	}

	else if (l >= 6 && l <= 8 && c >= 3 && c <= 5)
	{ // confere bloco 8

		for (int i = 6; i <= 8; i++)
		{
			for (int j = 3; j <= 5; j++)
				if (matriz[i][j] == valor)
					referencia = 8; // há numeros repetidos no bloco 8
		}
	}

	else if (l >= 6 && l <= 8 && c >= 6 && c <= 8)
	{ // confere bloco 9

		for (int i = 6; i <= 8; i++)
		{
			for (int j = 6; j <= 8; j++)
				if (matriz[i][j] == valor)
					referencia = 9; // há numeros repetidos no bloco 9
		}
	}
	return referencia;
}

int converteCharParaInt(char comando[10], int p) // coverte os valores de entada para int
{

	if (comando[p] == '0')
		return 0;
	else if (comando[p] == '1')
		return 1;
	else if (comando[p] == '2')
		return 2;
	else if (comando[p] == '3')
		return 3;
	else if (comando[p] == '4')
		return 4;
	else if (comando[p] == '5')
		return 5;
	else if (comando[p] == '6')
		return 6;
	else if (comando[p] == '7')
		return 7;
	else if (comando[p] == '8')
		return 8;
	else if (comando[p] == '9')
		return 9;
	return 0;
}

void ImprimiMatriz(char matriz[][9]) // imprime matriz
{
	// para imprimir os quadrantes separados
	for (int i = 0; i < 9; i++)
	{
		if (i % 3 == 0) // separa as linhas de tres em tres
			cout << "" << endl;

		for (int j = 0; j < 9; j++)
		{
			if (j % 3 == 0) // separa as colunas de tres em tres
				cout << "  "
					 << " ";

			cout << " " << matriz[i][j]; // imprime a matriz com um espaço entre os numeros
		}
		cout << "\n";
	}
}

bool validacaoDeCoordenadas(int l, int c, char valor) // confere se são válidas as coordenadas digitadas
{
	if (l >= 1 && l <= 9 && c >= 0 && c <= 9 && valor != '_' && c != '\0' && l != '\0' && valor != '\0')
		return true; // valor de retorno se a coordenada for válida

	else
		return false; //valor de retorno se a coordenada não for válida
}

void underline(char matriz[][9]) // coloca underline no lugar do zero(espaco vazio)
{
	for (int i = 0; i < 9; i++)
		for (int j = 0; j < 9; j++)
			if (matriz[i][j] == '0')
				matriz[i][j] = '_';
}

void alocarNumeroNaMatriz(char matriz[][9], int l, int c, int valor) // aloca o valor na celula especificada
{
	matriz[l][c] = valor;
}

int novaBase(char matriz[][9]) // verifica se a matriz de entrada é nova ou se é uma continuação de jogo salvo pela verificação de underlie
{
	int referencia = 1;
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
			if (matriz[i][j] == '_')
			{
				referencia = 0; // retorno quando a matriz foi modificada (pois contem underline ) se for nova, ela não conterá
				break;
			}
	}
	return referencia;
}

int main()
{

	char matriz[9][9]; // matriz jogo
	char base[9][9];   // matriz base

	ifstream entrada; // entrada da matriz do usuario
	ofstream saida;   // arquivo de saida que sera salvo

	char nome[20]; // variavel para armazenar o nome do arquuivo de entrada

	cout << "Indique o arquivo texto contendo o jogo: ";
	cin >> nome;

	entrada.open(nome);

	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
			entrada >> matriz[i][j]; // preenche a matriz do jogo
	}
	entrada.close();

	if (novaBase(matriz) == 0) // quando o jogo já foi modificado e já tem base antiga
	{
		entrada.open("base_tp_william"); // abre arquivo anterior com o nome base

		for (int i = 0; i < 9; i++)
		{
			for (int j = 0; j < 9; j++)
				entrada >> base[i][j]; // base preenchida e pronta para usar
		}
		entrada.close();
	}

	else if (novaBase(matriz) == 1) // quando é inserido um novo arquivo de jogo (ele será o estado inicial e salvo como base)
	{

		entrada.open(nome);

		for (int i = 0; i < 9; i++)
		{
			for (int j = 0; j < 9; j++)
				entrada >> base[i][j]; // prenche a matriz base
		}
		entrada.close();

		saida.open("base_tp_william"); // cria um arquivo "base" para estado inicial do jogo inserido
		for (int i = 0; i < 9; i++)
		{
			for (int j = 0; j < 9; j++)
				saida << base[i][j] << endl; // salva a matriz "base" (inicial)
			saida << endl;
		}

		saida.close();
	}

	char comando[10];										// comando de entrada
	char salvar[10] = {'s', 'a', 'l', 'v', 'a', 'r', '\0'}; //para comparar o comando com a palavra salvar
	char sair[10] = {'s', 'a', 'i', 'r', '\0'};				// para comparar o comando com a palavra sair

	underline(matriz);	 // substitui os zeros por underline
	ImprimiMatriz(matriz); // imprime a matriz com underline

	do
	{

		cout << "\nDigite um comando ou indique a celula a alterar: ";
		cin >> comando;

		int l = converteCharParaInt(comando, 0); // linha (conversão para int do primeiro indice do comando de entrada)
		int c = converteCharParaInt(comando, 1); // coluna (conversão para int do primeiro indice do comando de entrada)
		char valor = comando[2];				 // valor a ser alocado (terceiro indice do comando de entrada)

		if (strcmp(comando, salvar) == 0) // condicao para salvar
		{

			saida.open(nome); // abre o arquivo de saida

			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 9; j++)
					saida << matriz[i][j] << endl; // subrescreve a matriz do jogo com os novos dados
			}

			saida.close(); // fecha o arquivo de saida
			cout << "Jogo foi salvo em " << nome << endl;
		}

		else if (validacaoDeCoordenadas(l, c, valor) == false && strcmp(comando, sair) != 0) // validaçaõ de coordenadas
		{
			cout << "As coordenadas são inválidas!" << endl;
		}

		else if (numerosFixos(base, l - 1, c - 1, valor) == 1 && strcmp(comando, sair) != 0)
		{
			cout << "A celula " << l << "," << c << " já posui valor!" << endl;
		}

		else if (confereBlocos(matriz, l - 1, c - 1, valor))
		{

			cout << "Erro! A região " << confereBlocos(matriz, l - 1, c - 1, valor) << " ja possui o valor " << valor << "." << endl;
		}

		else if (ConfereLinha(matriz, l - 1, valor))
		{
			cout << "Erro! A linha " << l << " ja possui o valor " << valor << "." << endl;
		}

		else if (ConfereColuna(matriz, c - 1, valor))
		{
			cout << "Erro! A coluna " << c << " ja possui o valor " << valor << "." << endl;
		}

		else if (strcmp(comando, sair) != 0)
		{
			alocarNumeroNaMatriz(matriz, l - 1, c - 1, valor); // aloca o numero digitado na matriz
			underline(matriz);
			ImprimiMatriz(matriz); // imprimi a matriz modificada
		}

	} // fecha o do

	while (strcmp(comando, sair) != 0); // enquanto o comando for diferente de sair o jogo continua

	cout << "Você abandonou o jogo!" << endl; // quando sair sem salvar

	return 0;
}