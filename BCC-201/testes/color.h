//corlor.h
#ifndef COLOR_H
#define COLOR_H

#define BLACK "\x001b[30m"   // cor preta para os textos
#define RED "\x001b[31m"     // cor vermelha para os textos
#define GREEN "\x001b[32m"   // cor verde para os textos
#define YELLOW "\x001b[33m"  // cor amarelo para os texto
#define BLUE "\x001b[34m"    // cor azul para os textos
#define MAGENTA "\x001b[35m" // cor magenta para os textos
#define CYAN "\x001b[36m"    // cor ciano para os textos
#define WHITE "\x001b[37m"   // cor branca para os textos

#define BRIGTH_BLACK "\x001b[30;1m"
#define BRIGTH_RED "\x001b[31;1m"
#define BRIGTH_GREEN "\x001b[32;1m"
#define BRIGTH_YELLOW "\x001b[33;1m"
#define BRIGTH_BLUE "\x001b[34;1m"
#define BRIGTH_MAGENTA "\x001b[35;1m"
#define BRIGTH_CYAN "\x001b[36;1m"
#define BRIGTH_WHITE "\x001b[37;1m"

#define BACKGROUND_BLACK "\x001b[40m"   // cor do fundo do texto preto
#define BACKGROUND_RED "\x001b[41m"     // cor do fundo do texto vermelho
#define BACKGROUND_GREEN "\x001b[42m"   // cor do fundo do texto verde
#define BACKGROUND_YELLOW "\x001b[43m"  // cor do fundo do texto amarelo
#define BACKGROUND_BLUE "\x001b[44m"    // cor do fundo do texto azul
#define BACKGROUND_MAGENTA "\x001b[45m" // cor do fundo do texto Magenta
#define BACKGROUND_CYAN "\x001b[46m"    // cor do fundo do texto ciano
#define BACKGROUND_WHITE "\x001b[47m"   // cor do fundo do texto branco

#define BACKGROUND_BRIGTH_BLACK "\x001b[40;1m"
#define BACKGROUND_BRIGTH_RED "\x001b[41;1m"
#define BACKGROUND_BRIGTH_GREEN "\x001b[42;1m"
#define BACKGROUND_BRIGTH_YELLOW "\x001b[43;1m"
#define BACKGROUND_BRIGTH_BLUE "\x001b[44;1m"
#define BACKGROUND_BRIGTH_MAGENTA "\x001b[45;1m"
#define BACKGROUND_BRIGTH_CYAN "\x001b[46;1m"
#define BACKGROUND_BRIGTH_WHITE "\x001b[47;1m"

#define RESET "\x001b[0m"     //apaga todas as operações antes deste comando
#define NEGRITO "\x001b[1m"   //deixa o texto em negrito
#define UNDERLINE "\x001b[4m" //põe um linha embaixo do texto
#define REVERSED "\x001b[7m"
#define CLEAR "\33[H\33[2J" //limpa a tela * faz a mesma coisa do que system("clear") ou system("cls");

#endif