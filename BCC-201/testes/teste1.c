#include <stdio.h>

int main()
{
  int x = 100;
  int* px = &x;
  printf("valor de x    = %d\n",x );
  printf("endereço de x = %p\n", &x);
  printf("endereço de x = %p\n", px);
  printf("o valor de x  = %d\n",*px);
  return 0;
}
