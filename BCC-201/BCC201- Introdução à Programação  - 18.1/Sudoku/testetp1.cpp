#include<iostream>
#include<fstream>
#define m 9
using namespace std;


int temp[m];
int sudoku[m][m];
int pista[m][m];

bool verifi(int y[]);
bool veriLinha(int s[m][m], int);
bool veriColuna(int x[m][m], int);
bool verifiRegioes(int x[m][m]);
bool verisolucao();
bool checaLinha(int, int);
bool checaColuna(int, int);
bool veriregiao(int, int, int);
void imprime();
void posicaoarmaz();
bool volta(int&, int&);
bool enconNume(int, int);
bool resolve();



int main(int argc, char*argv[])//quase ok
{
	fstream arquivo;

	if (argc == 2)
	{
		arquivo.open(argv[1], ios::in);

		if (arquivo.is_open())
		{
			for (int linha = 0; linha < m; linha++) {
				for (int coluna = 0; coluna < m; coluna++) {
					arquivo >> sudoku[linha][coluna];
					if (sudoku[linha][coluna] != 0)
						pista[linha][coluna] = 1;
				}
			}
			imprime();
		}
		else
			cout << "Nao foi possivel localizar o arquvo ' " << argv[1] << " '. Insira manualmente" << endl;
	}

	if (argc > 2)
		cout << "Mais um argumento. Insira manualmente\n";

	if (!arquivo.is_open()) {
		cout << "Insira 81 elementos(0s para as celulas sem pistas) : " << endl;

		for (int linha = 0; linha < m; linha++) {
			for (int coluna = 0; coluna < m; coluna++) {
				cin >> sudoku[linha][coluna];
				if (sudoku[linha][coluna] != 0)
					pista[linha][coluna] = 1;
			}
		}
		imprime();
	}
	posicaoarmaz();
	resolve();
	imprime();
	return 0;
}

//fun��o para checar 
bool verifi(int y[m])
{
	int lineartemp;
	for (int i = 0; i < m; i++) {
		lineartemp = y[i];
		for (int j = i + 1; j < m; j++) { //procura por duplica��es
			if (y[j] == lineartemp)
				return false;
		}
	}
	return true;
}

//fun��ao para checar linhas
bool veriLinha(int s[m][m], int linha)
{
	for (int j = 0; j < m; j++) {

		temp[j] = s[linha][j];

		if (verifi(temp) == 0)
			return false;

	}
	return true;
}

//procura duplica��es em cada coluna
bool veriColuna(int x[m][m], int coluna)
{

	for (int j = 0; j < m; j++)
		temp[j] = x[j][coluna];

	if (verifi(temp) == 0)
		return false;

	return true;
}

//fun��o para checar numeeros duplicados em todas as 9 sub-regioes
bool verifiRegioes(int x[m][m])
{
	int k, i, j;
	for (int coluna = 0; coluna <= 6; coluna += 3) {
		for (int linha = 0; linha <= 6; linha += 3) {
			for (k = 0, i = 0; i < 3; i++) {
				for (j = 0; j < 3; j++)
					temp[k++] = x[linha + i][coluna + j];
			}
			if (verifi(temp) == 0)
				return false;
		}
	}
	return true;
}

bool verisolucao()
{
	if (!veriLinha(sudoku) || !veriColuna(sudoku) || !verifiRegioes(sudoku))
		return false;
	else
		return true;
}

//esta fun��o checa quantas numeros repetidos � linha
bool checaLinha(int linha, int num)
{
	int numencon = 0;
	for (int i = 0; i < m; i++) {
		if (sudoku[linha][i] == num) {
			numencon = 1;
			break;
		}
	}
	if (numencon == 1)
		return false;
	else
		return true;
}

//ests fun��o checa se h� numeros repetido em cada coluna
bool checaColuna(int coluna, int num)
{
	int numencon = 0;
	for (int i = 0; i < m; i++) {
		if (sudoku[i][coluna] == num) {
			numencon = 1;
			break;
		}
	}
	if (numencon == 1)
		return false;
	else
		return true;
}

//esta fun��ao checa se h� bnumeros repetido na regiao 3x3
bool veriregiao(int linha, int coluna, int num)
{
	int numencon = 0;
	int indiceDaLinha = (linha / 3) * 3;
	int indeceDaColuna = (coluna / 3) * 3;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j<3; j++) {
			if (sudoku[indiceDaLinha + i][indeceDaColuna + j] == num) {
				numencon = 1;
				break;
			}
		}
	}
	if (numencon == 1)
		return false;
	else
		return true;
}

//imprime
void imprime(int matrix[m][m])
{
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < m; j++)
			cout << matrix[i][j] << " ";
		cout << endl;
	}
	cout << endl;

}

void posicaoarmaz()
{
	int templinha, tempcoluna;
	int prevpossicao[m][m][2];
	templinha = -1;
	tempcoluna = -1;

	for (int linha = 0; linha < m; linha++) {
		for (int coluna = 0; coluna < m; coluna++) {
			if (pista[linha][coluna] == 0) {
				prevpossicao[linha][coluna][0] = templinha;
				prevpossicao[linha][coluna][1] = tempcoluna;
				templinha = linha;
				tempcoluna = coluna;
			}
		}
	}
}

bool volta(int &linha, int &coluna)
{
	int tlinha, tcoluna;
	int prevpossicao[m][m][2];

	if (linha == 0 && coluna == 0)
		return false;
	sudoku[linha][coluna];

	tlinha = prevpossicao[linha][coluna][0];
	tcoluna = prevpossicao[linha][coluna][1];
	tcoluna -= 1;

	linha = tlinha;
	coluna = tcoluna;

	return true;
}

//encontrar numero
bool enconNume(int linha, int coluna)
{
	if (pista[linha][coluna] == 1)
		return true;

	for (int num = sudoku[linha][coluna] + 1; num <= m; num++) {
		if (veriLinha(linha, num) && veriColuna(coluna, num) && verifiRegioes(linha, coluna, num)) {
			sudoku[linha][coluna] = num;
			return true;
		}
	}

	sudoku[linha][coluna] = 0;

	return false;
}

//resolve
bool resolve()
{
	for (int linha = 0; linha < m; linha++) {
		for (int coluna = 0; coluna < m; coluna++) {
			if (!enconNume(linha, coluna)) {
				sudoku[linha][coluna] = 0;
				if (!volta(linha, coluna))
					return false;
			}
		}
	}
	return true;
}

