
#include <iostream>
#include <stdio.h>
#include <cstdio>
#include <cstring>
using namespace std;

// para imprimir os quadrantes separados
/*for (int i=0 ; i< 9; i++){
if( i % 3 == 0 )     // separa as linhas de tres em tres
cout << "" << endl;

for (int j=0; j <9; j++){
if( j%3 == 0 )   // separa as colunas de tres em tres
cout << "" << " ";


cout  << " " << matriz[i][j]; // imprime a matriz com um espa�o entre os numeros

}
cout << "\n";

}
*/

int ConfereLinha(char matriz[][9], int c, int valor) // para n�o repetir numeros nas linhas
{

	for (int i = 0; i<9; i++) {
		if (matriz[i][c] == valor)

			return 1; // h� numeros repetidos

		else
			return 0; // n�o h� numeros repetidos
	}
	return 0;
}

int converteCharParaInt(char comando[10], int p)
{


	if (comando[p] == '1')
		return 1;
	else if (comando[p] == '0')
		return 0;
	else if (comando[p] == '2')
		return 2;
	else if (comando[p] == '3')
		return 3;
	else if (comando[p] == '4')
		return 4;
	else if (comando[p] == '5')
		return 5;
	else if (comando[p] == '6')
		return 6;
	else if (comando[p] == '7')
		return 7;
	else if (comando[p] == '8')
		return 8;
	else if (comando[p] == '9')
		return 9;
	return 0;
}

int ConfereColuna(char matriz[][9], int l, int valor) // para n�o repetir numeros nas linhas
{

	for (int i = 0; i<9; i++) {
		if (matriz[l][i] == valor)

			return 1; // h� numeros repetidos

		else
			return 0; // n�o h� numeros repetidos
	}
	return 0;
}

int confereBlocos(char matriz[][9], int l, int c, int valor)
{
	if (l >= 0 && l <= 2 && c >= 0 && c <= 2) { // confere bloco 1

		for (int i = 0; i<3; i++) {
			for (int j = 0; j< 3; j++)
				if (matriz[i][j] == valor)
					return 1; // h� numeros repetidos
				else

					return 0; // n�o h� numeros repetidos
		}
	}

	else if (l >= 0 && l <= 2 && c >= 3 && c <= 5) { // confere bloco 2

		for (int i = 0; i <= 2; i++) {
			for (int j = 3; j <= 5; j++)
				if (matriz[i][j] == valor)
					return 2; // h� numeros repetidos // colocar valor de retorno de acordo com o bloco
				else

					return 0; // n�o h� numeros repetidos
		}
	}

	else if (l >= 0 && l <= 2 && c >= 6 && c <= 8) { // confere bloco 3

		for (int i = 0; i <= 2; i++) {
			for (int j = 6; j <= 8; j++)
				if (matriz[i][j] == valor)
					return 3; // h� numeros repetidos
				else

					return 0; // n�o h� numeros repetidos
		}
	}


	else if (l >= 3 && l <= 5 && c >= 0 && c <= 2) { // confere bloco 4

		for (int i = 3; i <= 5; i++) {
			for (int j = 0; j <= 2; j++)
				if (matriz[i][j] == valor)
					return 4; // h� numeros repetidos
				else

					return 0; // n�o h� numeros repetidos
		}
	}

	

	else if (l >= 3 && l <= 5 && c >= 3 && c <= 5) { // confere bloco 5

		for (int i = 3; i <= 5; i++) {
			for (int j = 3; j <= 5; j++)
				if (matriz[i][j] == valor)
					return 5; // h� numeros repetidos
				else

					return 0; // n�o h� numeros repetidos
		}
	}

	

	else if (l >= 3 && l <= 5 && c >= 6 && c <= 8) { // confere bloco 6

		for (int i = 3; i <= 5; i++) {
			for (int j = 6; j <= 8; j++)
				if (matriz[i][j] == valor)
					return 6; // h� numeros repetidos
				else

					return 0; // n�o h� numeros repetidos
		}
	}

	else if (l >= 6 && l <= 8 && c >= 0 && c <= 2) { // confere bloco 7

		for (int i = 6; i <= 8; i++) {
			for (int j = 0; j <= 2; j++)
				if (matriz[i][j] == valor)
					return 7; // h� numeros repetidos
				else

					return 0; // n�o h� numeros repetidos
		}
	}

	else if (l >= 6 && l <= 8 && c >= 3 && c <= 5) { // confere bloco 8

		for (int i = 6; i <= 8; i++) {
			for (int j = 3; j <= 5; j++)
				if (matriz[i][j] == valor)
					return 8; // h� numeros repetidos
				else

					return 0; // n�o h� numeros repetidos
		}
	}

	else if (l >= 6 && l <= 8 && c >= 6 && c <= 8) { // confere bloco 9

		for (int i = 6; i <= 8; i++) {
			for (int j = 6; j <= 8; j++)
				if (matriz[i][j] == valor)
					return 9; // h� numeros repetidos
				else

					return 0; // n�o h� numeros repetidos
		}
	}
	return 0;
}

int converteCharParaInt(char comando[10], int p)
{
	if (comando[p] == '1')
		return 1;
	else if (comando[p] == '2')
		return 2;
	else if (comando[p] == '3')
		return 3;
	else if (comando[p] == '4')
		return 4;
	else if (comando[p] == '5')
		return 5;
	else if (comando[p] == '6')
		return 6;
	else if (comando[p] == '7')
		return 7;
	else if (comando[p] == '8')
		return 8;
	else if (comando[p] == '9')
		return 9;
	return 0;
}

int NumerosFixos(char matriz[][9], int l, int c)
{
	if (matriz[l][c] == 0) {
		return 0; // quando o local esta vazio (com valor zero)
	}
	else
		return 1; // quando o local esta com um valor fixo (diferente de zero)
}

void ImprimiMatriz(char matriz[][9])
{
	// para imprimir os quadrantes separados
	for (int i = 0; i< 9; i++) {
		if (i % 3 == 0)     // separa as linhas de tres em tres
			cout << "" << endl;

		for (int j = 0; j <9; j++) {
			if (j % 3 == 0)   // separa as colunas de tres em tres
				cout << "" << " ";

			cout << " " << matriz[i][j]; // imprime a matriz com um espa�o entre os numeros
		}
		cout << "\n";
	}
}

int validacaoDeCoordenadas(int l, int c)
{
	if (l >= 0 && l <= 9 && c >= 0 && c <= 9)
		return 1; // valor de retorno se a coordenada for v�lida

	else
		return 0; //valor de retorno se a coordenada n�o for v�lida

}

int validacaoDeValor(int x)// valor da celula digitado
{
	if (x >= 0 && x <= 9)
		return 0; // para valor correto

	else
		return 1; // para valor incorreto

	return 0;
}

void underline(char matriz[][9]) // coloca underline no lugar do zero(espaco vazio)
{
	for (int i = 0; i < 9; i++)
		for (int j = 0; j < 9; j++)
			if (matriz[i][j] == '0')
				matriz[i][j] = '_';

}

void alocarNumeroNaMatriz(char matriz[][9], int l, int c, int valor)
{
	matriz[l][c] = valor;
}

int NumerosFixosLinhasColunas(char matriz[][9], int l, int c, int valor) // para nao sobrepor um numero fixo
{
	for (int i = 0; i <= 9; i++) { // verifica colunas
		if (matriz[l][i] == 0)
			return 1; // a coordenada esta vazia para jogada
		else
			return 0; // a coordenada n�o esta vazia para jogada
	}

	for (int j = 0; j <= 9; j++) { // verificar linhas
		if (matriz[j][c] == 0)
			return 1; // a coordenada esta vazia para jogada
		else
			return 0; // a coordenada n�o esta vazia para jogada
	}
	return 0;
}

int main()
{
	char matriz[9][9] = { { '7', '1', '6', '2', '0', '0', '0', '8', '4' }, // matriz base
						  { '0', '0', '8', '0', '7', '0', '0', '0', '0' },
						  { '0', '0', '4', '0', '1', '0', '5', '0', '7' },
						  { '8', '4', '0', '1', '6', '0', '0', '0', '2' },
						  { '2', '0', '0', '0', '0', '0', '0', '3', '5' },
						  { '6', '3', '0', '7', '5', '0', '0', '0', '0' },
	                      { '0', '0', '2', '0', '4', '7', '1', '0', '0' },
						  { '0', '0', '3', '0', '2', '8', '4', '0', '9' },
						  { '0', '5', '0', '0', '0', '1', '2', '0', '0' } };


	//char x[15]; // teste

	char comando[10]; // comando de entrada
	char salvar[10] = { 's','a','l','v','a','r','\0' }; //para comparar o comando com a palavra salvar
	char sair[10] = { 's','a','i','r','\0' }; // para comparar o comando com a palavra sair


											  //cout << "Indique o arquivo texto contendo o jogo :";
											  //cin >> arquivo.txt;// arquivo que conter� a matri
	ImprimiMatriz(matriz);

	do {
		underline(matriz);

		cout << "\nDigite um comando ou indique a celula a alterar: ";
		cin >> comando;

		int l = converteCharParaInt(comando, 0); // linha
		int c = converteCharParaInt(comando, 1); // coluna
		char valor = comando[2];



		if (strcmp(comando, salvar) == 0) // condicao para salvar
										  // comando para salvar
			cout << "o jogo foi salvo com sucesso! " << endl;

			else if (!ConfereColuna(matriz, l, c))
			{
				cout << "o numero " << valor << "j� existe na coluna " << c << endl;
			}
			else if (!ConfereLinha(matriz, l, c)
			{
				cout << "o numero " << valor << "j� existe na linha " << l << endl;
			}
			else if (!confereBlocos(matriz, l, c))
			{
				cout << "A regi�o " << confereBlocos(matriz, l, c) << "j� possui o valor " << valor << endl;
			}


			
			
			else
				{
						alocarNumeroNaMatriz(matriz, l, c, valor); // aloca o numero digitado na matriz
						ImprimiMatriz(matriz);
				}

	} 
	while (strcmp(comando, sair) != 0); // enquanto o comando for diferente de sair

	cout << "Voc� abandonou o jogo!" << endl;

	// colocar valor deretorno de acordo com o bloco

	return 0;
}