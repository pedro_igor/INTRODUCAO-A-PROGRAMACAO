//tp de prog  
#include<iostream>
#include<fstream>
using namespace std;

const int  m = 9;
int temp[m];
int verifi(int y[]);
int sudoku[m][m];

//fun��ao para checar linhas
int veriLinha(int s[m][m])
{
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < m; j++) {
			temp[j] = s[i][j];

			if (verifi(temp) == 0)
				return 0;
		}
	}
	return 1;
}

//fun��o para checar 
int verifi(int y[m])
{
	int lineartemp;
	for (int i = 0; i < m; i++) {
		lineartemp = y[i];
		for (int j = i + 1; j < m; j++) { //procura por duplica��es
			if (y[j] == lineartemp)
				return 0;
		}
	}
	return 1;
}

//fun��o para checar numeeros duplicados em todas as 9 sub-regioes
int verifiRegioes(int x[m][m])
{
	for (int coluna = 0; coluna <= 6; coluna += 3) {
		for (int linha = 0; linha <= 6; linha += 3) {
			for (int k = 0; int i = 0; i < 3; i++) {
				for (j = 0; j < 3; j++)
					temp[k++] = x[linha + i][coluna + j];
			}
			if (verifi(temp) == 0)
				return 0;
		}
	}
	return 1;
}

//procura duplica��es em cada coluna
int veriColuna(int x[m][m])
{
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < m; j++)
			temp[j] = x[j][i];

		if (verifi(temp) == 0)
			return 0;
	}
	return 1;
}

int verisolucao()
{
	if (!veriLinha(sudoku) || !veriColuna(sudoku) || !verifiRegioes(sudoku))
		return 0;
	else
		return 1;
}

//esta fun��o checa quantas numeros repetidos � linha
int checaLinha(int linha, int num)
{
	int numencon = 0;
	for (int i = 0; i < m; i++) {
		if (sudoku[linha][i] == num) {
			numencon = 1;
			break;
		}
	}
	if (numencon == 1)
		return 0;
	else
		return 1;
}

//ests fun��o checa se h� numeros repetido em cada coluna
int checaColuna(int coluna, int num)
{
	int numencon = 0;
	for (int i = 0; i < m; i++) {
		if (sudoku[i][coluna] == num) {
			numencon = 1;
			break;
		}
	}
	if (numencon == 1)
		return 0;
	else
		return 1;
}

//esta fun��ao checa se h� bnumeros repetido na regiao 3x3
int veriregiao(int linha, int coluna, int num)
{
	int numencon = 0;
	int indiceDaLinha = (linha / 3) * 3;
	int indeceDaColuna = (coluna / 3) * 3;
	for (int i = 0; i < 3; i++) {
		for(int j =0; j<3; j++){
			if (sudoku[indiceDaLinha + i][indeceDaColuna + j] == num) {
				numencon = 1;
				break;
			}
		}
	}
	if (numencon == 1)
		return 0;
	else
		return 1;
}

//imprime
void imprime(int matrix[m][m])
{
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < m; j += )
			cout << matrix[i][j] << " ";
		cout << endl;
	}
	cout << endl;
	return;
}

void posicaoarmaz()
{
	int templinha, tempcoluna;
	templinha = -1;
	tempcoluna = -1;

	for (int linha = 0; linha < m; linha++) {
		for (int coluna = 0; coluna < m < coluna++) {
			if (pista[linha][coluna] == 0) {
				prevpossicao[linha][coluna][0] = templinha;
				prevpossicao[linha][coluna][1] = tempcoluna;
				templinha = linha;
				tempcoluna = coluna;
			}
		}
	}
}

int volta(int &linha, int &coluna)
{
	int tlinha, tcoluna;

	if (linha == 0 && coluna == 0)
		return 0;
	sudoku[linha][coluna];

	tlinha = prevpossicao[linha][coluna][0];
	tcoluna = prevpossicao[linha][coluna][1];
	tcoluna -= 1;

	linha = tlinha;
	coluna = tcoluna;

	return 1;
}

//encontrar numero
int enconNume(int linha, int coluna)
{
	if (pista[linha][coluna] == 1)
		return 1;

	for (int num = sudoku[linha][coluna] + 1; num <= m; num++) {
		if (veriLinha(linha, num) && veriColuna(coluna, num) && verifiRegioes(linha, coluna, num)) {
			sudoku[linha][coluna] = num;
			return 1;
		}
	}

	sudoku[linha][coluna] = 0;

	return 0;
}

//resolve
int resolve()
{
	for (int linha = 0; linha < m; linha++) {
		for (int coluna = 0; coluna < m; coluna++) {
			if (!enconNume(linha, coluna)) {
				sudoku[linha][coluna] = 0;
				if (!volta(linha, coluna))
					return 0;
			}
		}
	}
	return 1;
}

int main(int argc, char*argv[])
{
	fstream arquivo;
	
	if (argc == 2)
	{
		arquivo.open(argv[1], ios::in);

		if (arquivo.is_open())
		{
			for (int linha = 0; linha < m; linha++) {
				for (int coluna = 0; coluna < m; coluna++) {
					arquivo >> sudoku[linha][coluna];
					if (sudoku[linha][coluna] != 0)
						pista[linha][coluna] = 1;
				}
			}
			imprime(sudoku);
		}
		else
			cout << "Nao foi possivel localizar o arquvo ' " << argv[1] << " '. Insira manualmente" << endl;
	}

	if (argc > 2)
		cout << "Mais um argumento. Insira manualmente\n";

	if (!arquivo.is_open()) {
		cout << "Insira 81 elementos(0s para as celulas sem pistas) : " << endl;

		for (int linha = 0; linha < m; linha++) {
			for (intcoluna = 0; coluna < m; coluna++) {
				cin >> sudoku[linha][coluna];
				if (sudoku[linha][coluna] != 0)
					pista[linha][coluna] = 1;
			}
		}
		imprime(sudoku);
	}
	posicaoarmaz();
	resolve();
	imprime(sudoku);
	return 0;
}