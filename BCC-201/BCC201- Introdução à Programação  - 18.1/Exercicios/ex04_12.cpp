//Exercicio 4.12: exe04_12.cpp
//o que que esse programa imprime 
#include<iostream>
using namespace std;

int main()
{
	int y;//declarara o y
	int x = 1;//inicializa o x
	int total = 0; //inicializa o total

	while( x <= 10 )//faz o loop 10 vezes
	{
		y = x * x;// realiza os calculos
		cout << y << endl;//gera a saidas dos resultados
		total += y;//adiciona y ao total
		x++;//incrementa o contador x
	}//fim de while

	cout << "Total é " << total << endl;//exibe o resultado
	return 0;
}