//exe04_13.cpp
#include<iostream>
using namespace std;

int main()
{
	double km, litros;
	
	while(km != -1)
	{
		cout << "Entre com a Quilometragem (-1 para sair): ";
		cin >> km;
		cout << "Entre com os litros: ";
		cin >> litros;
		cout << "Km/litros deste danque: " << km / litros << endl << endl;
	}

	return 0;
}