/*Faca um programa que apresenta a seguinte saída, perguntando ao usuário o
número máximo (no exemplo, 9). Este número deve ser sempre ímpar.
       1 2 3 4 5 6 7 8 9
         2 3 4 5 6 7 8  
           3 4 5 6 7 
             4 5 6 
               5
*/
#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
	int numero, j, i;
	cout << "\tDigite o numero maximo do Triangulo.\n";
	cout << "\n*O NUMERO DEVE SER IMPAR!*\n";
	cin >> numero;

	if (numero % 2 == 0)
	{
		cout << "\n NUMERO INVALIDO\n";
		return 1;
	}
	else
	{
		for (i = 0; i <= (numero / 2) + 1; i++)
		{
			for (j = i + 1; j <= numero - i; j++)
			{
				cout << j;
			}
			cout << "\n";

			for (j = 0; j < (i + 1) * 2; j++)
				cout << " " << endl;
		}
	}
	return 0;
}