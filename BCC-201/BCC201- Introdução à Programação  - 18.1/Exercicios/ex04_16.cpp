//ex04_16.cpp
#include<iostream>
using namespace std;

int main()
{
	double salarioBruto, valorHora;
	int hora;

	cout << "Entre com as horas trabalhadas ( -1 para Terminar): ";
	cin >> hora;

	while(hora != -1)
	{
		cout << "Entre com o valor por hora trabalhada ($00.00):  ";
		cin >> valorHora;

		if(hora <= 40)
		{
			salarioBruto = hora * valorHora;
			cout << "Salario: $" << salarioBruto << endl;
		}
		else if ( hora > 40)
		{
			salarioBruto = (40 * valorHora) + (((hora - 40) * valorHora ) * 0.5) + valorHora; 
			cout << "Salario: $" << salarioBruto << endl;
		}

		cout << "\nEntre com as horas Trabalhadas ( -1 para Terminar): ";
		cin >> hora;
	}

	return 0;
}