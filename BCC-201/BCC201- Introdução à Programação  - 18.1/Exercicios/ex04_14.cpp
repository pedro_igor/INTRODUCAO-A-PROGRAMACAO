//ex04_14.cpp
#include<iostream>
using namespace std;

int main()
{
	int conta;
	double saldoInicial, taxas, totalCreditos, limiteCredito;
	double novoSaldo;

	while(conta != -1)
	{
		cout << "Entre com o numero da conta (-1 para terminar): ";
		cin >> conta;

		cout << "Entre com o Saldo Inicial: ";
		cin >> saldoInicial;

		cout << "Entre com o total de Taxas: ";
		cin >> taxas;

		cout << "Entre com o total de Creditos: ";
		cin >> totalCreditos;

		cout << "Entre com o limite de Credito: ";
		cin >>limiteCredito;

		novoSaldo = (saldoInicial + taxas - totalCreditos);

		cout << "Novo Saldo: " << novoSaldo;

		if(novoSaldo > limiteCredito)
		{
			cout << "\nConta:\t" << conta;
			cout << "\nLimite de Credito: " << limiteCredito;
			cout << "\nSaldo:\t" << novoSaldo;
			cout << "\nLIMITE DE CREDITO ULTRAPASSADO." << endl<<endl;
		}
		else 
			cout << endl;

	}	
	return 0;
}