#include <iostream>
#include <cstdlib>
#include <fstream>
#include<clocale>
#define M 9
using namespace std;

void lerArquvo(char sudoku[][M]);
void sobrescreve(char sudoku[][M]);
void imprime(char sudoku[][M]);
void interact();
void entrada(char sudoku[][M]);
void edita (char sudoku[][M]);
void mostraOsValores();

/**********************************************************************
* Main: Basicamente um delegador. Apenas faz outras fun��es fazerem o seu
* trabalho sujo para isso. 
***********************************************************************/
int main()
{
	setlocale(LC_ALL, "portuguese");
	//Declara a matriz
	char sudoku[M][M];

	//Chamando outras fun��es / matriz de passagem
	lerArquvo(sudoku);
	interact();
	imprime(sudoku);

	return 0;
}


/**********************************************************************
* lerArquvo: pergunta ao usu�rio por um nome de arquivo, l� um gameboard do
* esse nome de arquivo e, em seguida, coloca-o em uma matriz.
***********************************************************************/
void lerArquvo(char sudoku[][M])
{
	//Declare o nome do arquivo
	char arquivoFonte[256];

	//Declarar entrada de arquivo
	ifstream fin;

	//Obter o nome do arquivo do usu�rio
	cout << "Digite o nome do arquivo:\n ";
	cin >> arquivoFonte;

	//Abrir arquivo com verifica��o de erro
	fin.open(arquivoFonte);
	if (fin.fail())
	{
		cout << "A abertura do arquivo falhou.\n";
		exit(1);
	}

	//Leia o arquivo na matriz
	for (int col = 0; col < M; col++)
	{
		for (int linha = 0; linha < M; linha++)
		{
			fin >> sudoku[linha][col];
			if (sudoku[linha][col] == '0')
			{
				sudoku[linha][col] = ' ';
			}
		}
	}

	//fecha o arquivo
	fin.close();
}

/***********************************************************************
* Exibe os resultado na tela
***********************************************************************/
void imprime(char sudoku[][M])
{
	//Declara a variavel
	char option;

	//imprime cabe�alho da coluna
	cout << "   1 2 3 4 5 6 7 8 9" << endl

		//linha 1
		<< "1  "
		<< sudoku[0][0]
		<< " " << sudoku[1][0]
		<< " " << sudoku[2][0]
		<< "|"
		<< sudoku[3][0]
		<< " " << sudoku[4][0]
		<< " " << sudoku[5][0]
		<< "|"
		<< sudoku[6][0]
		<< " " << sudoku[7][0]
		<< " " << sudoku[8][0]
		<< endl

		//linha 2
		<< "2  "
		<< sudoku[0][1]
		<< " " << sudoku[1][1]
		<< " " << sudoku[2][1]
		<< "|"
		<< sudoku[3][1]
		<< " " << sudoku[4][1]
		<< " " << sudoku[5][1]
		<< "|"
		<< sudoku[6][1]
		<< " " << sudoku[7][1]
		<< " " << sudoku[8][1]
		<< endl

		//linha 3
		<< "3  "
		<< sudoku[0][2]
		<< " " << sudoku[1][2]
		<< " " << sudoku[2][2]
		<< "|"
		<< sudoku[3][2]
		<< " " << sudoku[4][2]
		<< " " << sudoku[5][2]
		<< "|"
		<< sudoku[6][2]
		<< " " << sudoku[7][2]
		<< " " << sudoku[8][2]
		<< endl

		//Visual Separator
		<< "   -----+-----+-----" << endl

		//linha 4
		<< "4  "
		<< sudoku[0][3]
		<< " " << sudoku[1][3]
		<< " " << sudoku[2][3]
		<< "|"
		<< sudoku[3][3]
		<< " " << sudoku[4][3]
		<< " " << sudoku[5][3]
		<< "|"
		<< sudoku[6][3]
		<< " " << sudoku[7][3]
		<< " " << sudoku[8][3]
		<< endl

		//linha 5
		<< "5  "
		<< sudoku[0][4]
		<< " " << sudoku[1][4]
		<< " " << sudoku[2][4]
		<< "|"
		<< sudoku[3][4]
		<< " " << sudoku[4][4]
		<< " " << sudoku[5][4]
		<< "|"
		<< sudoku[6][4]
		<< " " << sudoku[7][4]
		<< " " << sudoku[8][4]
		<< endl

		//linha 6
		<< "6  "
		<< sudoku[0][5]
		<< " " << sudoku[1][5]
		<< " " << sudoku[2][5]
		<< "|"
		<< sudoku[3][5]
		<< " " << sudoku[4][5]
		<< " " << sudoku[5][5]
		<< "|"
		<< sudoku[6][5]
		<< " " << sudoku[7][5]
		<< " " << sudoku[8][5]
		<< endl

		//Visual Separator
		<< "   -----+-----+-----" << endl

		//linha 7
		<< "7  "
		<< sudoku[0][6]
		<< " " << sudoku[1][6]
		<< " " << sudoku[2][6]
		<< "|"
		<< sudoku[3][6]
		<< " " << sudoku[4][6]
		<< " " << sudoku[5][6]
		<< "|"
		<< sudoku[6][6]
		<< " " << sudoku[7][6]
		<< " " << sudoku[8][6]
		<< endl

		//linha 8
		<< "8  "
		<< sudoku[0][7]
		<< " " << sudoku[1][7]
		<< " " << sudoku[2][7]
		<< "|"
		<< sudoku[3][7]
		<< " " << sudoku[4][7]
		<< " " << sudoku[5][7]
		<< "|"
		<< sudoku[6][7]
		<< " " << sudoku[7][7]
		<< " " << sudoku[8][7]
		<< endl

		//linha 9
		<< "9  "
		<< sudoku[0][8]
		<< " " << sudoku[1][8]
		<< " " << sudoku[2][8]
		<< "|"
		<< sudoku[3][8]
		<< " " << sudoku[4][8]
		<< " " << sudoku[5][8]
		<< "|"
		<< sudoku[6][8]
		<< " " << sudoku[7][8]
		<< " " << sudoku[8][8]
		<< "\n"
		<< "\n";

	entrada(sudoku);
}


/*************************************************************************
* Interagir: Permite ao usu�rio interagir e manipular o tabuleiro do jogo.
*
************************************************************************/
void interact()
{
	cout << "Opcoes:\n"
		<< "   ?  Mostra as  instrucoes\n"
		<< "   D  Exibir quadrado\n"
		<< "   E  Edita um quadrado\n"
		<< "   S  Mostra os valores possiveis para um quadrado\n"
		<< "   Q  Salva e sai\n"
		<< "\n";

	return;
}


/*************************************************************************
* entrada: Obtem a entrada do usuario
*
************************************************************************/
void entrada(char sudoku[][M])
{
	char option;
	cout << "> ";
	cin >> option;

	if (option == 'e' || option == 'E')
		edita (sudoku);
	else if (option == '?')
		interact();
	else if (option == 'd' || option == 'D')
		imprime(sudoku);
	else if (option == 's' || option == 'S')
		mostraOsValores();
	else if (option == 'q' || option == 'Q')
		sobrescreve(sudoku);
	else
		cout << "ERROR: comanndo invalido";

	return;
}


/***********************************************************************
* edita : Edita um quadrado da tabela com base nas coordenadas
* inserido pelo usu�rio.
************************************************************************/
void edita (char sudoku[][M])
{
	//Declara as variaveis
	int lin;
	int col;
	int valor = 0;

	//Gets letter/number coordinates
	cout << "Quais sao as coordenada do quadado: ";
	cin >> lin >> col;

	

	//If square is full, imprime "read only" message
	if (sudoku[lin - 1][col - 1] != ' ')
	{
		cout << "ERROR: Quadrado \'" << lin << col << "\' eh somente leitura\n";
		cout << "\n";
		entrada(sudoku);
	}
	else
	{
		//Gets valor to place in specified coordinates
		cout << "Qual eh o valor \'" << lin << col << "\': ";
		cin >> valor;

		//Makes sure valor is within correct range
		if (valor < 1 || valor > M)
		{
			cout << "ERROR: Valor \'" << valor << "\' no quadrado \'" << lin << col << "\' eh invalido\n";
			cout << "\n";
			entrada(sudoku);
		}

		cout << "\n";
		sudoku[lin - 1][col - 1] = valor;
		entrada(sudoku);
	}

	return;
}

/******************************************************************************
* sobrescreve:Escreve o conte�do do quadro em um arquivo a ser coletado posteriormente.
*
*****************************************************************************/
void sobrescreve(char sudoku[][M])
{
	//Declare file output
	ofstream fout;
	char destinationFile[256];

	//Asking for user input
	cout << "Digite o nome do arquivo: ";
	cin >> destinationFile;

	//Open destination file & error checking
	fout.open(destinationFile);
	if (fout.fail())
	{
		cout << "A abertura do arquivo falhou.\n";
		exit(1);
	}
	else
		cout << "A abertura feita com sucesso";

	//Writes board to file
	for (int col = 0; col < M; col++)
	{
		for (int linha = 0; linha < M; linha++)
		{
			if (sudoku[linha][col] == '_')
			{
				sudoku[linha][col] = '0';
			}

			fout << sudoku[linha][col];

			//verifica se � um grade 9x9
			if (linha % M == 0)
			{
				cout << endl;
			}
		}
	}

	//fecha o arquivo
	fout.close();
}

/**************************************************************************
* mostraOsValores: Mostra todos os valores poss�veis para um determinado conjunto de coordenadas.
*
**************************************************************************/
void mostraOsValores()
{
	//declara variaveis
	int lin;
	int col;

	//recebe letra e numero como coordernada
	cout << "Quais s�o as coordenadas do quadrado: ";
	cin >> lin >> col;

	

	return;
}