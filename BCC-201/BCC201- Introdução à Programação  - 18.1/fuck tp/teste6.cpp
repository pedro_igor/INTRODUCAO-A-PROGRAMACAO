#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 9

void print_sudoku(char sudoku[size][size]) {
	char i, j;

	/* print board */
	for (i = 0; i < SIZE; i++) {
		if (!(i % 3))
			printf("+------+------+------+\n");

		for (j = 0; j < SIZE; j++) {
			if (!(j % 3))
				printf("|");
			printf("%2d", sudoku[i][j]);
		}
		printf("|\n");
	}
	printf("+------+------+------+\n");
}

void swap(char(*sudoku)[size][size], char x1, char y1, char x2, char y2) {
	char aux;

	/* swap */
	aux = (*sudoku)[x1][y1];
	(*sudoku)[x1][y1] = (*sudoku)[x2][y2];
	(*sudoku)[x2][y2] = aux;
}

int main(int argc, char ** argv) {
	char sudoku[size][size] = { { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
	{ 7, 8, 9, 1, 2, 3, 4, 5, 6 },
	{ 4, 5, 6, 7, 8, 9, 1, 2, 3 },
	{ 9, 1, 2, 3, 4, 5, 6, 7, 8 },
	{ 6, 7, 8, 9, 1, 2, 3, 4, 5 },
	{ 3, 4, 5, 6, 7, 8, 9, 1, 2 },
	{ 8, 9, 1, 2, 3, 4, 5, 6, 7 },
	{ 5, 6, 7, 8, 9, 1, 2, 3, 4 },
	{ 2, 3, 4, 5, 6, 7, 8, 9, 1 } };
	char i, j, k;
	unsigned int iter, steps, select, type, block, swap1, swap2;

	/* start random number generation */
	srand(time(NULL));

	/* select a number from 1000 to 1000000 */
	steps = (rand() % (1000000 - 1000)) + 1000;

	/* swap cycle */
	for (iter = 0; iter < steps; iter++) {
		select = rand();

		/* select action parameters from the random number */
		type = (select & 0xff) % 6;
		block = ((select >> 8) & 0xff) % 3;
		swap1 = ((select >> 16) & 0xff) % 3;
		swap2 = ((select >> 24) & 0xff) % 3;
		swap2 = swap2 == swap1 ? (swap2 + 1) % 3 : swap2;

		/* check for swap type */
		switch (type) {
		case 0:
			/* column swap cycle step */
			for (j = 0; j < SIZE; j++)
				swap(&sudoku, j, block * 3 + swap1, j, block * 3 + swap2);
			break;
		case 1:
			/* line swap cycle step */
			for (j = 0; j < SIZE; j++)
				swap(&sudoku, block * 3 + swap1, j, block * 3 + swap2, j);
			break;
		case 2:
			/* column block swap cycle step */
			for (k = 0; k < 3; k++)
				for (j = 0; j < SIZE; j++)
					swap(&sudoku, j, swap1 * 3 + k, j, swap2 * 3 + k);
			break;
		case 3:
			/* line block swap cycle step */
			for (k = 0; k < 3; k++)
				for (j = 0; j < SIZE; j++)
					swap(&sudoku, swap1 * 3 + k, j, swap2 * 3 + k, j);
			break;
		case 4:
			/* major diagonal swap cycle step */
			for (k = 1; k < SIZE; k++)
				for (j = 0; j < k; j++)
					swap(&sudoku, k, j, j, k);
			break;
		case 5:
			/* minor diagonal swap cycle step */
			for (k = 0; k < SIZE - 1; k++)
				for (j = 0; j < SIZE - k - 1; j++)
					swap(&sudoku, k, j, SIZE - j - 1, SIZE - k - 1);
			break;
		}
	}

	print_sudoku(sudoku);

	return 0;
}