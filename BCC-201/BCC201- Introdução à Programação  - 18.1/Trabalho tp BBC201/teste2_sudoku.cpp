#include <stdio.h>

//Autores: Gustavo Araujo Carvalho e Yuri Alessandro

/*
* Esta fun��o serve para checar se existem dois valores iguais na mesma linha,
* o que invalidaria a proposi��o da matriz ser um quadrado latino.
*/
int checkHorizontal(int matriz[9][9]){

    /*
    * As vari�veis i, j e k servem para itera��o.
    * A vari�vel lat serve como retorno e tem o valor padr�o de 1,
    * que considera que o quadrado j� � inicialmente latino.
    */
    int i, j, k, lat = 1;

    /*
    * O primeiro loop serve para percorrer as linhas da matriz, enquanto que
    * o segundo e o terceiro loop servem para comparar valores que ocupam a
    * mesma linha, se esses valores forem iguais, o quadrado n�o � latino e o
    * valor de lat vai a zero. H� tamb�m a condi��o de parada nos loops para
    * quando lat for falso (igual a 0), isso serve para quebrar os loops e evitar
    * processamento desnecess�rio.
    * H� o cuidado de se colocar j diferente de k como condi��o no if para
    * n�o comparar valores que s�o iguais.
    */
    for(i = 0; i < 9 && lat; i++)
        for(j = 0; j < 9 && lat; j++)
            for(k = 0; k < 9 && lat; k++)
                if((j != k) && (matriz[i][j] == matriz[i][k]))
                    lat = 0;

    /*
    O valor retornado � lat, que serve como uma vari�vel booleana
    quando se trata de dizer se o quadrado � ou n�o latino.
    */
    return lat;
}

/*
* Esta fun��o serve para checar se existem dois valores iguais na mesma coluna,
* o que invalidaria a proposi��o de ser quadrado latino.
*/
int checkVertical(int matriz[9][9]){

    /*
    * Essa fun��o tem o funcionamento exatamente igual ao da fun��o checkHorizontal,
    * com a diferen�a que procura valores iguais na mesma coluna ao inv�s de na
    * mesma linha. As vari�veis i, j, e k ainda servem para itera��o e lat como a
    * vari�vel booleana de resposta.
    */
    int i, j, k, lat = 1;

    for(j = 0; j < 9 && lat; j++)
        for(i = 0; i < 9 && lat; i++)
            for(k = 0; k < 9 && lat; k++)
                if((i != k) && (matriz[i][j] == matriz[k][j]))
                    lat = 0;

    /*
    * O return � o booleano lat, que diz se o quadrado � ou n�o latino
    * considerando somente as colunas.
    */
    return lat;
}

/*
* Esta fun��o serve para preencher a matriz com os valores inseridos pelo
* usu�rio. Como arranjos j� s�o por si s� um tipo de ponteiro, as mudan�as
* que acontecem nessa fun��o afetam a matriz declarada na fun��o main.
*/
void popularMatriz(int matriz[9][9]){

    /*
    * As vari�veis i e j servem para itera��o, os loops percorrer�o todos os
    * espa�os da matriz e pedir�o um valor para o usu�rio p�r em cada um deles.
    */
    int i, j;

    printf("Digite os valores do quadrado a ser testado:\n");

    /*
    * H� tamb�m um loop que impede o usu�rio de colocar valores acima de 9,
    * pois o jogo de Sudoku s� aceita n�meros de 1 a 9;
    */
    for(i = 0; i < 9; i++)
        for(j = 0; j < 9; j++){
            scanf("%i", &matriz[i][j]);
            while(matriz[i][j] < 1 || matriz[i][j] > 9){
                printf("Somente valores maiores que 0 e menores ou iguais a 9\n");
                printf("sao permitidos no jogo de sudoku, digite outro valor.\n");
                scanf("%i", &matriz[i][j]);
            }
        }
}

/*
* Esta fun��o serve para checar os setores menores do jogo de Sudoku, para
* fazer isso, recebe uma matriz 3x3, que tem o tamanho de um setor do jogo
* de Sudoku e verifica se esse mesmo � v�lido.
* A fun��o passa por todos os valores da matriz e conta a frequ�ncia dos
* n�meros de 1 a 9. J� que todos os quadrados s�o preenchidos e s� existem
* 9 quadrados em cada setor, ele deve ter valores �nicos de 1 a 9 , Se houver
* um d�gito repetido, significa que todos os 9 que deveriam estar presentes
* n�o est�o, e logo, o Sudoku � inv�lido.
*/
int checarMenor(int mini[3][3]){
    /*
    * As vari�veis i e j servem para iterar atrav�s de todas as posi��es da
    * matriz, enquanto que num serve para iterar de 1 a 9, os n�meros que
    * ser�o procurados. "existe" � a vari�vel que informa quantas vezes o n�mero
    * "num" foi encontrado na matriz.
    */
    int i, j, num, existe = 0;

    /*
    * Os loops passam por todos os valores da matriz e sempre que encontram o
    * n�mero "num" em um dos espa�os incrementam a vari�vel "existe" em mais 1.
    * Se "existe" for maior que 1, isto �, se ouver mais de um n�mero "num" no mesmo
    * setor, o setor � automaticamente inv�lido e retorna falso. Caso passe por
    * todos os loops corretamente, n�o h� problemas no setor e fun��o retorna
    * verdadeiro.
    */
    for(num = 1; num <= 9; num++){
        for(i = 0; i < 3; i++)
            for(j = 0; j < 3; j++)
                if(mini[i][j] == num) existe++;
        if(existe > 1) return 0;
        existe = 0;
    }

    return 1;
}
/*
* Esta fun��o serve para separar a matriz completa 9x9 em matrizes
* pequenas 3x3 e envi�-las para a fun��o checarMenor(), para assim serem
* retornadas como v�lidas ou n�o.
*/
int checarMenores(int matriz[9][9]){
    /*
    * As vari�veis i e j servem para iterar sobre a matriz 9x9 e identificar
    * os setores do Sudoku. Elas crescem de 3 em 3 para ter in�cio exatamente
    * sobre o primeiro quadrado de cada setor, isso serve de refer�ncia para
    * o come�o de onde o programa vai come�ar a pegar os valores para por na
    * matriz mini.
    * As vari�veis k e l servem para iterar sobre os elementos da matriz pequena
    * 3x3; e da matriz 9x9 tomando como in�cio os pontos i e j, para todos os
    * elementos da matriz grande serem alcan�ados.
    * A matriz mini 3x3, na sua vez, armazenar� os valores de cada um desses
    * setores para serem depois enviados para a fun��o checarMenor()
    */
    int i, j, k, l, mini[3][3];

    /*
    * Como j� dito, os loops de i e j v�o iterar sobre o come�o de cada setor e
    * os loops de k e l sobre esses mesmos setores. Quando uma matriz 3x3 for completa,
    * ela ser� enviada para a fun��o checarMenor() para verificar se � ou n�o v�lida.
    * Caso ela n�o seja v�lida, a fun��o checarMenor() retornar� falso, o que causar�
    * a fun��o atual retornar falso tamb�m, pois o Sudoku � inv�lido.
    * Caso todos os loops sejam completados sem problema, quer dizer que o programa
    * n�o achou nenhuma inconsist�ncia, e ent�o a fun��o retornar� verdadeiro.
    */
    for(i = 0; i < 9; i += 3)
        for(j = 0; j < 9; j += 3){
            for(k = 0; k < 3; k++)
                for(l = 0; l < 3; l++)
                    mini[k][l] = matriz[k+i][l+j];
            if(!checarMenor(mini))
                return 0;
        }

    return 1;
}
/*
* Este � o in�cio do programa.
* Primeiramente ele criar� uma matriz 9x9 e a enviar� para a fun��o popularMatriz().
* L� o usu�rio inserir� valores na matriz.
*/
int main(){
    int matriz[9][9];
    popularMatriz(matriz);

    /*
    * Agora uma estrutura condicional vai verificar se o matriz inserida pelo
    * usu�rio � ou n�o um Sudoku v�lido.
    * As fun��es checkHorizontal() e checkVertixal() v�o verificar se a matriz
    * � um quadrado latino 9x9, uma das condi��es para ser Sudoku, enquanto
    * checarMenores vai verificar, com a ajuda de checarMenor(), cada um dos
    * setores da Matriz, que devem ter valores �nicos de 1 a 9 dentro deles;
    * essa tamb�m � uma condi��o para Sudoku.
    *
    * Se as tr�s fun��es retornarem verdadeiro, significa que a matriz realmente
    * forma um Sudoku, e uma mensagem que expressa isso ser� impressa na tela.
    * Mas se ao menos um desses retornar falso, significa que a matriz n�o � Sudoku,
    * e assim uma mensagem ser� impressa na tela informando isso.
    */
    if(checkHorizontal(matriz) && checkVertical(matriz) && checarMenores(matriz))
        printf("A matriz inserida forma um Sudoku. Parab�ns!");
    else
        printf("A matriz inserida nao forma um Sudoku. Que pena!");

    /*
    * Fim do Programa.
    */
    return 0;
}