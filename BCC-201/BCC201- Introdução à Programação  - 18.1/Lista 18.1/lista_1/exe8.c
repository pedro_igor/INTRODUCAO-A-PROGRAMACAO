///exercicio 8
#include <stdio.h>
#include <stdlib.h>

int main()
{

    const double salary = 2000.00;
    const double commission = 500.00;
    int total_car;
    int registration;

    printf("Matricula do funcionario: ");
    scanf("%d", &registration);

    printf("quantos carros o funcinario vendeu este mes: ");
    scanf("%d", &total_car);

    double net_salary = salary + (commission * total_car);

    system("clear");
    printf("\n");
    printf("******************\n");
    printf("*MATRICULA: %d*\n", registration);
    printf("*SALARIO: %.2lf*\n", net_salary);
    printf("******************\n");

    return 0;
}