//exercicio 7
#include <stdio.h>
#include <stdlib.h>

int main()
{
    system("clear");
    int matricula, age;
    double salario;

    printf("Digite a matricula Do funcionario: ");
    scanf("%d", &matricula);

    printf("Digite a idade do funcionario: ");
    scanf("%d", &age);

    printf("Digite o Salario atual do funcinario: ");
    scanf("%lf", &salario);

    double netSalary = (salario + (salario * 0.38)) + (salario * 0.20) - (salario * 0.15);

    printf("\n");
    printf("|------------------------|\n");
    printf("|MATRICULA: %d          |\n", matricula);
    printf("|SALARIO BRUTO: %.2lf   |\n", salario);
    printf("|SALARI LIQUIDO: %.2lf |\n", netSalary);
    printf("|------------------------|\n");

    return 0;
}