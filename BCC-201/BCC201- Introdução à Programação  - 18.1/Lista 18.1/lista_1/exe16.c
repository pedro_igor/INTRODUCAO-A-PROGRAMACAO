//exercicio 16
#include <stdio.h>
#include <math.h>

int main()
{
    int a1, nro, q; //q = razão

    printf("ENTER A NUMBER: ");
    scanf("%d", &nro);

    printf("ENTER THE TERM OF FIRST P.G.: ");
    scanf("%d", &a1);

    printf("ENTER THE REASON OF P.G.: ");
    scanf("%d", &q);

    int an = a1 * pow(q, (nro - 1));

    printf("THE N-THIRD TERM OF THE P.G. IS: %d\n", an);

    return 0;
}