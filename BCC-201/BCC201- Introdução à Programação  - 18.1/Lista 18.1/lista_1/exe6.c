//exercicio 6
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    system("clear");
    int x1, x2, y1, y2;

    printf("Usuario digite as coordernadas do primeiro ponto: ");
    scanf("%d %d", &x1, &y1);

    printf("\nDigite as coordernadas do segundo ponto: ");
    scanf("%d %d", &x2, &y2);

    double dist = sqrt(pow(x2 - x1, 2) + (y2 - y1));

    printf("A distancia entre os dois pontos é: %.2lf\n", dist);

    return 0;
}