//exercicio 15
#include <stdio.h>

int main()
{
    int a1, razao, nro;

    printf("ENTER THE REASON OF P.A: ");
    scanf("%d", &razao);

    printf("ENTER THE FIRST TERM OF P.A: ");
    scanf("%d", &a1);

    printf("ENTER A NUMBER: ");
    scanf("%d", &nro);

    int an = a1 + (nro - 1) * razao;

    printf("THE N-THIRD TERM OF THE P.A. IS: %d\n", an);

    return 0;
}