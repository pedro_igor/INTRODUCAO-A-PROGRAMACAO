//exercicio 11
#include <stdio.h>

int main()
{
    double farenheit, celsius;

    printf("DIGITE A TEMPERATURA EM graus FARENHEIT: ");
    scanf("%lf", &farenheit);

    celsius = (5.0 / 9.0) * (farenheit - 32);

    printf("%lf em graus Celsius: %.2lfºC\n", farenheit, celsius);

    return 0;
}
