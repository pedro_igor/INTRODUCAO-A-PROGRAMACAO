//exercicio 10
#include <stdio.h>
#include <math.h>

int main()
{
    double l1, l2;

    printf("Digite os catetos do triangul0: ");
    scanf("%lf %lf", &l1, &l2);

    double h = sqrt(pow(l1, 2) + pow(l2, 2));

    printf("A hipotenusa = %lf\n", h);

    return 0;
}