//exercicio 12
#include <stdio.h>

int main()
{
    double base, height;
    char beep = '\a';
    double p, a;
    printf("DIGITE A BASE DO RETANGULO: ");
    scanf("%lf", &base);

    printf("DIGITE A ALTURA DO RETANGULO: ");
    scanf("%lf", &height);

    if (base <= 0 || height <= 0)
    {

        printf("\n %c Erro: A altura e a base tem que ser maior do que zero %c\n", beep, beep);
    }
    else
    {
        a = base * height;
        p = 2 * base + 2 * height;

        printf("A area do retangulo é: %.2lf \n", a);
        printf("O perimetro do retangulo é: %.2lf\n", p);
    }
    return 0;
}