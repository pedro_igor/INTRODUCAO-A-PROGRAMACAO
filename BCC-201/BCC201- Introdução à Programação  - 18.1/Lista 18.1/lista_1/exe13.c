//exercicio 13
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define PI 3.141592654

int main()
{
    system("clear");
    double raio;
    double c, a, v;

    printf("DIGITE O RAIO DO CIRCULO: ");
    scanf("%lf", &raio);

    if (raio <= 0)
    {
        printf("\nERRO: the radius must be above zero%c\n", 07);
    }
    else
    {
        c = 2 * PI * raio;
        a = PI * pow(raio, 2);
        v = (4.0 / 3.0) * PI * pow(raio, 3);

        printf("\nO COMPRIMENTO DA CIRCUFERENCIA: %.2lf", c);
        printf("\nA AREA DA CIRCUFERENCIA: %.2lf", a);
        printf("\nO VOLUME DA ESFERA: %.2lf\n", v);
    }

    return 0;
}