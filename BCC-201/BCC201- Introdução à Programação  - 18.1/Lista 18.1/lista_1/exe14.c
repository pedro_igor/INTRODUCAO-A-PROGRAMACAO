//exercicio 14
#include <stdio.h>

int main()
{
    double base, base2, height;
    char beep = '\a';
    double a;

    printf("DIGITE AS BASES DO TRAPEZIO: ");
    scanf("%lf %lf", &base, &base2);

    printf("DIGITE A ALTURA DO TRAPEZIO: ");
    scanf("%lf", &height);

    if (base <= 0 || height <= 0)
    {

        printf("\n %c Erro: A altura e as bases tem que ser maior do que zero %c\n", beep, beep);
    }
    else
    {
        a = ((base + base2) * height) / 2.0;

        printf("A area do Trapezio é: %.2lf \n", a);
    }

    return 0;
}