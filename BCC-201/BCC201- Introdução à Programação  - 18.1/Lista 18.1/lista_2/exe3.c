//exercicio 3
#include <stdio.h>

int main()
{
    int num1, num2;

    printf("Digite dois numeros: ");
    scanf("%d %d", &num1, &num2);

    if (num1 > num2)
    {
        printf("%d é maior do que %d\n", num1, num2);
    }
    else
    {
        printf("%d é maior do que %d\n", num2, num1);
    }

    return 0;
}