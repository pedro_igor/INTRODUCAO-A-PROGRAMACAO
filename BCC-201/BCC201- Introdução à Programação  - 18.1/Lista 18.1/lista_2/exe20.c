//exercicio 20
#include <stdio.h>
#include <stdlib.h>
#include "/home/pedro/Documentos/code/color.h"

#define HOTDOG 5.00
#define BAURUSIMPLES 6.00
#define BAURU_OVO 7.50
#define HABURGUER 9.00
#define CHEESEBURGUER 11.00
#define SODA 4.00

int main()
{
    system("clear");

    int cod, quan;
    double price;

    printf("%s%s",NEGRITO, YELLOW);
    printf("\n╔═══════════════════════════════════════╗");
    printf("\n║PRODUTO              CÓDIGO       PREÇO║");
    printf("\n╠═══════════════════════════════════════╣");
    printf("\n║Cachoro Quente        100        R$5,00║");
    printf("\n║Bauru Simples         101        R$6,00║");
    printf("\n║Bauru com Ovo         102        R$7,50║");
    printf("\n║Hamburguer            103        R$9,00║");
    printf("\n║Cheeseburguer         104       R$11,00║");
    printf("\n║Refrigerante          105        R$4,00║");
    printf("\n╚═══════════════════════════════════════╝");
    printf("%s", RESET);

    printf("\nDIGITE O CODIGO DO PRODUTO: ");
    scanf("%d", &cod);

    switch (cod)
    {
        case 100:
            printf("\nCACHORRO QUENTE\n");
            
            printf("QUAL É A  QUANTIDADE: ");
            scanf("%d", &quan);

            price = quan * HOTDOG;

            break;

        case 101:
            printf("\nBAURU SIMPLES\n");
           
            printf("QUAL É A  QUANTIDADE: ");
            scanf("%d", &quan);

            price = quan * BAURUSIMPLES;

            break;

        case 102:
            printf("\nBAURU COM OVO\n");
           
            printf("QUAL É A  QUANTIDADE: ");
            scanf("%d", &quan);

            price = quan * BAURU_OVO;

            break;

        case 103:
            printf("\nHAMBURGUER\n");

            printf("QUAL É A  QUANTIDADE: ");
            scanf("%d", &quan);

            price = quan * HABURGUER;

            break;

        case 104:
            printf("\nCHEESEBURGUER\n");

            printf("QUAL É A  QUANTIDADE: ");
            scanf("%d", &quan);

            price = quan * CHEESEBURGUER;

            break;

        case 105:
            printf("\nREFRIGERANTE\n");
            
            printf("QUAL É A  QUANTIDADE: ");
            scanf("%d", &quan);

            price = quan * SODA;

            break;
            
        default:

            printf("\n%s%sERRO:%sProduto invalido%c\n", BRIGTH_RED, NEGRITO, RESET, 07);
            
            break;
    }//fim do switch

    printf("\n%sO TOTAL DE SUA COMPRA É: R$%.2lf%s\n", NEGRITO, price, RESET);

    return 0;
}