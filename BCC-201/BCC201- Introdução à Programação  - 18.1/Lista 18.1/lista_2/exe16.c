//exercicio 16
#include <stdio.h>
#include <math.h>

int main()
{
    int a, b, c, delta;
    int x1, x2;

    printf("\nDIGITE OS COEFICIENTES DA EQUAÇÃO DO 2° GRAU: ");
    scanf("%d %d %d", &a, &b, &c);

    delta = pow(b, 2) - (4 * a * c);

    if (delta < 0)
    {
        printf("\nA EQUAÇÃO NÃO POSSUI RAIZES REAIS!\n");
    }
    else
    {
        x1 = (-b + sqrt(delta)) / (2 * a);
        x2 = (-b - sqrt(delta)) / (2 * a);

        printf("delta: %d\n", delta);
        printf("x1 = %d\n", x1);
        printf("x2 = %d\n", x2);
    }

    return 0;
}