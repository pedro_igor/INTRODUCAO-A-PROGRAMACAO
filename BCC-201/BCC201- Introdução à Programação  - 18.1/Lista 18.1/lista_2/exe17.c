//exercicio 17
#include <stdio.h>

int main()
{
    double a, b, c;
    printf("Digite os lados dos triangulo: ");
    scanf("%lf %lf %lf", &a, &b, &c);

    if ((a + b) > c && (a + c) > b && (b + c) > a)
    {
        if (a == b && a == c)
        {
            printf("\nO TRIANGULO É EQUILATERO!\n");
        }
        else if ((a == b && a != c) || (a == c && a != b) || (b == c && b != a))
        {
            printf("\nO TRIANGULO É ISOCELES!\n");
        }
        else if (a != b && a != c && b != c)
        {
            printf("\nO TRIANGULO É ESCALENO!\n");
        }
    }
    else
    {
        printf("\nNÃO É UM TRIANGULO\n");
    }

    return 0;
}