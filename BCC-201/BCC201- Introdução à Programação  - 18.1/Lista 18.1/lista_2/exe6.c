//exercicio 6
#include <stdio.h>

int main()
{
    int matricula;
    double salario, reajuste;
    char cargo;

    printf("DIGITE A MATRICULA DO FUNCIONARIO: ");
    scanf("%d", &matricula);

    printf("DIGITE O CARGO DO FUNCIONARIO: ");
    scanf("%c", &cargo);

    printf("DIGITE O SALARIO DO FUNCIONARIO: ");
    scanf("%lf", &salario);

    if (cargo == 'o' || cargo == 'O')
    {
        reajuste = salario + (salario * 0.2);
        printf("\nMATRICULA: %d", matricula);
        printf("\nSALARIO (NOVO): %.2lf", reajuste);
    }
    else if (cargo == 'p' || cargo == 'P')
    {
        reajuste = salario + (salario * 0.18);
        printf("\nMATRICULA: %d", matricula);
        printf("\nSALARIO (NOVO): %.2lf", reajuste);
    }
    else
    {
        printf ("CARGO INVALIDO\n");
    }

    return 0;
}