//exercicio 19
#include <stdio.h>

int main()
{

    int day, month, year;

    printf("Digite o dia: ");
    scanf("%d", &day);

    printf("\nDigite o mês: ");
    scanf("%d", &month);

    printf("\nDigite o ano: ");
    scanf("%d", &year);

    if (month > 12 || month < 1)
    {
        printf("\n%cMÊS INVALIDO!\n", 07);
    }
    else
    {
        switch (month)
        {
            case 1:
                printf("\nData: %d de janeiro de %d\n", day, year);
                break;

            case 2:
                printf("\nData: %d de fevereiro de %d\n", day, year);
                break;

            case 3:
                printf("\nData: %d de março de %d\n", day, year);
                break;

            case 4:
                printf("\nData: %d de abril de %d\n", day, year);
                break;

            case 5:
                printf("\nData: %d de maio de %d\n", day, year);
                break;

            case 6:
                printf("\nData: %d de junho de %d\n", day, year);
                break;

            case 7:
                printf("\nData: %d de julho de %d\n", day, year);
                break;

            case 8:
                printf("\nData: %d de agosto de %d\n", day, year);
                break;

            case 9:
                printf("\nData: %d de setembro de %d\n", day, year);
                break;

            case 10:
                printf("\nData: %d de outubro de %d\n", day, year);
                break;

            case 11:
                printf("\nData: %d de Novembro de %d\n", day, year);
                break;

            case 12:
                printf("\nData: %d de dezembro de %d\n", day, year);
                break;

            default:
                printf("\nERRO!\n");
                break;
        }
    }

    return 0;
}