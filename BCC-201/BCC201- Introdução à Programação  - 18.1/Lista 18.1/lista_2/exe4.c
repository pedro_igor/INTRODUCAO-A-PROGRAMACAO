//exercicio 4
#include <stdio.h>

int main()
{
    int mat1, mat2;
    double salary1, salary2;

    printf("Digite a matricula do funcionario 1: ");
    scanf("%d", &mat1);

    printf("Digite o salario deste funcionario: ");
    scanf("%lf", &salary1);

    printf("\nDigite a matricula do funcionario 2: ");
    scanf("%d", &mat2);

    printf("Digite o salario deste funcionario: ");
    scanf("%lf", &salary2);

    if (salary1 > salary2)
    {
        printf("\nMatricula: %d\n", mat1);
        printf("Salario: %.2lf\n", salary1);
    }
    else
    {
        printf("\nMatricula: %d\n", mat2);
        printf("Salario: %.2lf\n", salary2);
    }

    return 0;
}