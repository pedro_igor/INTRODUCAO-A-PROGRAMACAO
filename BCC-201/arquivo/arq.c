/*
 * Programa que que faz uma copia de um arquivo
 * de nome original.txt para um arquivo de nome copia.txt. O
 * arquivo copia.txt ficara no mesmo diretorio do arquivo
 * original.txt.
 *
 */
#include <stdio.h>
#include <stdlib.h>

int main()
{
  FILE *arq_o; /* associado ao arquivo original */
  FILE *arq_c; /* associado ao arquivo copia */
  int n; /* numero de elementos do arquivo original */
  float x;
  int i;

  /* 1. abra arquivo original.txt para leitura */
  arq_o = fopen("original.txt","r");
  if (arq_o == NULL)
    {
      printf("Erro na abertura do arquivo original.txt.\n");
      //system("pause"); /* para WINDOWNS */
      exit(-1);  /* abandona a execucao do programa */
    }

  /* 2. abra arquivo copia.txt para escrita */
  arq_c = fopen("copia.txt","w");
  if (arq_c == NULL)
    {
      printf("Erro na abertura do arquivo copia.txt.\n");
      //system("pause"); /* para WINDOWNS */
      exit(-1); /* abandona a execucao do programa */
    }

  /* 3. leia o tamanho da sequencia no arquivo original.txt */
  fscanf(arq_o,"%d", &n);

  /* 4. escreva o numero de elementos no arquivo copia.txt */
  fprintf(arq_c,"%d\n", n);

  /* 5. leia os numeros do arquivo original.txt e escreva em copia.txt*/
  for (i = 0; i < n; i++)
    {
      fscanf(arq_o,"%f", &x);
      fprintf(arq_c,"%f ", x);
      if (i == 5)
        { /* apenas cinco numeros por linha de arq_c */
          fprintf(arq_c,"\n");
        }
    }
 
 /* 6. feche o arquivo original.txt */
  fclose(arq_o);

  /* 7. feche o arquivo copia.txt */
  fclose(arq_c);
  return 0;
}