#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	FILE *pf;
	float pi = 3.1415;
	float pilido;

	if((pf = fopen("arquivo.bin", "wb")) == NULL) //abre arquivo binario para escrita
	{
		printf("Erro na abertura do arquivo.\n");
		exit(1);
	}

	if(fwrite(&pi, sizeof(float), 1, pf) != 1) // escreve a variavel pi
	{
		printf("Erro na escrita do arquivo.\n");
		fclose(pf);
	}

	if((pf = fopen("arquivo.bin", "rb")) == NULL) // abre o arquivo novamente para leitura
	{
		printf("Erro na arbertura do arquivo.\n");
		exit(1);
	}

	if(fread(&pilido, sizeof(float), 1, pf) != 1)
	{
		printf("Erro na leitura do arquivo.\n");
	}

	printf("\nO valor de PI, lido so arquivo é: %f\n", pilido);

	fclose(pf);
	return 0;
}