#include <stdio.h>
#include <stdlib.h>
int main()
{
   FILE *p;
   char str[80],c;
   printf("\n\n Entre com um nome para o arquivo:\n");       /* Le um nome para o arquivo a ser aberto: */
   gets(str);
   if (!(p = fopen(str,"w")))  		                     /* Caso ocorra algum erro na abertura do arquivo..*/ 
     {                           		             /* o programa aborta automaticamente */
        printf("Erro! Impossivel abrir o arquivo!\n");
        exit(1);
     }
   fprintf(p,"Este e um arquivo chamado:\n%s\n", str);
   fclose(p);                                                /* Se nao houve erro, imprime no arquivo, fecha ...*/
   p = fopen(str,"r");                                 /* abre novamente para a leitura  */
   while (!feof(p))
    {
       fscanf(p,"%c",&c);
       printf("%c",c);
    } 
   fclose(p);
   return 0;
}