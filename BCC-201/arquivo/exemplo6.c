#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
   FILE *p;
   char c, str[30], frase[80] = "Este e um arquivo chamado: ";
   int i;
   printf("\n\n Entre com um nome para o arquivo:\n");
   fgets(str, 31, stdin); 					/* Le um nome para o arquivo a ser aberto: */
   if (!(p = fopen(str,"w")))  	/* Caso ocorra algum erro na abertura do arquivo..*/
    {                           
      printf("Erro! Impossivel abrir o arquivo!\n");
      exit(1); 			/* o programa aborta automaticamente */
    }
   strcat(frase, str);
   for (i=0; frase[i]; i++)
     putc(frase[i],p);
   fclose(p); 				/* Se nao houve erro,imprime no arquivo e o fecha ...*/
   p = fopen(str,"r");			/* Abre novamente para  leitura  */
   c = getc(p);				/* Le o primeiro caracter */
   while (!feof(p))        		/* Enquanto não se chegar no final do arquivo */
    {    
     printf("%c",c); 		/*   Imprime o caracter na tela */
     c = getc(p);    		/* Le um novo caracter no arquivo */
    }
   fclose(p);              		/* Fecha o arquivo */
}