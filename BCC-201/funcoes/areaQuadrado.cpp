//area do quadrado
#include<iostream>
#include<cmath>
using namespace std;

double area(double lado);

int main()
{
	double lado;

	cout << "ENTRE COM O TAMANHO DO LADO DO QUADRADO: ";
	cin >> lado;

	area(lado);
	
	return 0;
}

//funçao que calcula a area do quadrado
double area(double lado)
{
	double area = pow(lado, 2);

	cout << "A AREA DO QUADRADO É: " << area << endl;

	return area;
}