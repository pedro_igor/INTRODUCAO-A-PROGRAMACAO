//
#include<iostream>
using namespace std;

double soma(double x, double y);
double subtracao(double x, double y);
double mutiplicacao(double x, double y);
double divisao(double x, double y);

int main()
{
	double a, b;
	char op;

	cout << "Digite dois Numeros: ";
	cin >> a >> b;

	cout << "Qual operação deseja realizar entre eles: ";
	cin >> op;

	switch(op)
	{
		case '+':
			soma(a, b);
			break;

		case '-':
			subtracao(a, b);
			break;

		case '*':
			mutiplicacao(a, b);
			break;

		case '/':
			divisao(a,b);
			break;

		default:
			cout << "OPERAÇÃO INVALIDA!";
	}

	
	cout << endl;

	return 0;
}

//função de soma
double soma(double x, double y)
{
	double soma = x + y;

	cout << x << " + " << y << " = " << soma << endl;

	return soma;
}

//funçao de subtração 
double subtracao(double x, double y)
{
	double subtracao = x - y;

	cout << x << " - " << y << " = " << subtracao << endl;

	return subtracao;
}

//funçao de multiplicação 
double mutiplicacao(double x, double y)
{
	double multiplicacao = x * y;

	cout << x << " * " << y << " = " << multiplicacao << endl;

	return multiplicacao;
}

//funçao de divisão 
double divisao(double x, double y)
{
	double divisao = x / y;

	cout << x << " / " << y << " = " << divisao << endl;

	return divisao;
}