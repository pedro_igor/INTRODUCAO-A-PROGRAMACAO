//ex04_15.cpp
#include<iostream>
using namespace std;

int main()
{
	double valorVendas, salario;
	const double tx = 200.00;

	cout << "Entre com as vendas em dolar (-1 para terminar): ";
	cin >> valorVendas;

	while(valorVendas != -1)
	{
		salario = valorVendas * 0.09 + tx;

		cout << "Salario: $" << salario << endl;

		cout << "\nEntre com as vendas em dolar (-1 para terminar): ";
		cin >> valorVendas;
	} 
	return 0;
}