//
#include<iostream>
using namespace std;

double bruto(double salAtual, double reajuste);
double liquido(double gratifica, double desconto, double salAtual, double reajuste);

int main()
{
	double salarioAtual,reajuste1, desconto1, gratificacao1;

	cout << "Digite o Salario do funcionario: ";
	cin >> salarioAtual;

	cout << "Entre com a porcentagem do reajuste do salario do funcionario: ";
	cin >> reajuste1;

	cout << "Entre com a porcentagem da gratificação do funcionario: ";
	cin >> gratificacao1;

	cout << "Entre com a porcentagem que será descontaddo do salario do funcionario: ";
	cin >> desconto1;

	double desconto = desconto1 / 100;//transforma o numero inteiro em um numero real
	double reajuste = reajuste1 / 100;
	double gratificacao = gratificacao1 /100;

	cout << "O Salario bruto do funcionario reajustado é:" << bruto(salarioAtual, reajuste) << endl;
	liquido(gratificacao, desconto,salarioAtual, reajuste);

	cout << endl;

	return 0;
}

//salario bruto
double bruto(double salAtual, double reajuste)
{
	double salBruto = salAtual + (salAtual * reajuste);

	return salBruto;
}

//salario liquido
double liquido(double gratifica, double desconto, double salAtual, double reajuste)
{
	double salLiquido = bruto(salAtual, reajuste) + bruto(salAtual, reajuste) * gratifica - bruto(salAtual, reajuste) * desconto;
	cout << "O salario liquido do funcionario é de: " << salLiquido << endl;

	return salLiquido;	
}