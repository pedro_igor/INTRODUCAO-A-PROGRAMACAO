//perimetro do retangulo
#include<iostream>
using namespace std;

double perimetro(double a, double b);

int main()
{
	double a, b;

	cout << "DIGITE OS LADOS DO RETANGULO: ";
	cin >> a >> b;

	perimetro(a, b);

	cout << endl; 
	
	return 0;
}

//função para calcular o perimetro
double perimetro(double a, double b)
{
	double perimetro = 2 * a + 2 * b;

	cout << "O PERIMETRO DO RETANGULO É: " << perimetro << endl;

	return perimetro;
}