//raio
#include<iostream>
#include<cmath>
#define PI 3.141492654
using namespace std;

double comprimento(double raio);
double area(double raio);
double volume(double raio);

int main()
{
	double raio;

	cout << "ENTRE COM O RAIO: ";
	cin >> raio;

	comprimento(raio);
	area(raio);
	volume(raio);
	
	cout << endl;
	return 0;
}

//comprimento da circuferencia
double comprimento(double raio)
{
	double comprimento = 2 * raio * PI;

	cout << "O comprimento da circuferencia é: " << comprimento << endl;

	return comprimento;
}

//area do circulo
double area(double raio)
{
	double area = PI * pow(raio,2);

	cout << "A area do circulo é: " << area << endl;

	return area;
}

//volume da esfera
double volume(double raio)
{
	double volume = (4 / 3) * PI * pow(raio,3);

	cout << "O volume da esfera é: " << volume << endl;

	return volume;
}