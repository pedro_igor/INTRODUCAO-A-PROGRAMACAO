#include <stdio.h>
#include <stdlib.h>

int bisex(int year)
{
    if ((year % 4 == 0) && (year % 400 == 0) && (year % 100 != 0))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int main()
{
    system("clear");

    int year;
    int bisexto;

    printf("Digite o Ano: ");
    scanf("%d", &year);

    bisexto = bisex(year);

    if (bisexto == 0)
    {
        printf("O ano não é bissexto\n\n");
    }
    else
    {
        printf("%d é Bissexto.\n\n", year);
    }

    return 0;
}