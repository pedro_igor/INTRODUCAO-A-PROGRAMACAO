#include "exe10.h"

double soma(double x, double y)
{
    return x + y;
}

double pod(double x, double y)
{
    return x * pow(y, 2);
}

double quad(double x)
{
    return pow(x, 2);
}

double raiz(double x, double y)
{
    return sqrt((pow(x, 2) + pow(y, 2)));
}

double seno(double x, double y)
{
    double result = sin(x * PI / 180) - sin(y * PI / 180);

    return result;
}