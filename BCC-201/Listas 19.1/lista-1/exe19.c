#include <stdio.h>
#include <stdlib.h>

double salary_base(double salary, int productivity);
//void read_salary(double *salary);
void menu();

int main()
{
    double salary, final_salary;
    int produtividade;

    system("clear");

    printf("Digite o salario do funcionario: ");
    //read_salary(&salary);
    scanf("%lf", &salary);

    menu();
    printf("Digite a Produtividade do funcionario: ");
    scanf("%d", &produtividade);

    final_salary = salary_base(salary, produtividade);

    printf("\nFINAL SALARIO: %.2lf\n\n", final_salary);

    return 0;
}

double salary_base(double salary, int productivity)
{
    double final_salary;

    if (productivity == 1)
    {
        final_salary = salary + (salary * 0.85);
        return final_salary;
    }
    else if (productivity == 2)
    {
        final_salary = salary + (salary * 0.35);
        return final_salary;
    }
    else if (productivity == 3)
    {
        final_salary = salary - (salary * 0.12);
        return final_salary;
    }
}

void menu()
{
    printf("\n");
    printf("--------------------------------------\n");
    printf("1 Exelente       +65%% do salário base\n");
    printf("--------------------------------------\n");
    printf("2 Intermediário  +35%% do salário base\n");
    printf("--------------------------------------\n");
    printf("3 Regular        -12%% do salário base\n");
    printf("--------------------------------------\n");
    printf("\n\n");
}

/*void read_salary(double *salary)
{
    scanf("%lf", *salary);
}*/