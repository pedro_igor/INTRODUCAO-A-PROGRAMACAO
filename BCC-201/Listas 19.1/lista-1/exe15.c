#include <stdio.h>

void troca(int *a, int *b, int *c);

int main()
{
    int a, b, c;

    printf("Entre com os valores de a,  b, c: ");
    scanf("%d %d %d", &a, &b, &c);

    troca(&a, &b, &c);

    printf("a = %d\n", a);
    printf("b = %d\n", b);
    printf("c = %d\n\n", c);

    return 0;
}
void troca(int *a, int *b, int *c)
{
    int aux;
    aux = *a;
    *a = *b;
    *b = *c;
    *c = aux;
}