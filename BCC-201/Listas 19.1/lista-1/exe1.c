#include <stdio.h>

int main()
{
    int a, b, c, d, e;
    int maior;

    printf("Digite 5 Numeros: ");
    scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);

    if( a > b && a > c && a > d && a > e)
        maior = a;
    else if(b > a && b > c && b > d && b > e )
        maior = b;
    else if(c > a && c > b && c >d && c > e)
        maior = c;
    else if(d >a && d > b && d > c && d > e)
        maior = d; 
    else 
        maior = e;

    printf("O maior numero = %d\n", maior);
    return 0;
}