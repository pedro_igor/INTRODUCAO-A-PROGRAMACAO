#include <stdio.h>

int main()
{
    char letra;

    printf("Digite um caracter: ");
    scanf("%c", &letra);

    if (letra >= 65 && letra <= 90)
    {
        printf("é uma letra maiúscula\n");
    }
    else if (letra >= 97 && letra <= 122)
    {
        printf("É uma letra minúscula\n");
    }
    else
    {
        printf("Não é uma letra!!\n");
    }

    return 0;
}