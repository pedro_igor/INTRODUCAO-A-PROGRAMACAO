#include <stdio.h>
#include <math.h>
#include <stdlib.h>

double cal_imc(double peso, double altura);

int main()
{
    double imc, peso, altura;

    system("clear");

    printf("\nEntre com o seu peso em Kg: ");
    scanf("%lf", &peso);

    printf("Entre com sua Altura em m: ");
    scanf("%lf", &altura);

    imc = cal_imc(peso, altura);

    printf("Peso (Kg) : %.2lf\n", peso);
    printf("Altura (m): %.2lf\n", altura);
    printf("IMC       : %.2lf\n", imc);

    return 0;
}

double cal_imc(double peso, double altura)
{
    double imc = peso / (pow(altura, 2));

    return imc;
}