#include <stdio.h>
#include <stdlib.h>

int main()
{
    int idade;

    system("clear");

    printf("-----------------------------------------------------------\n");
    printf("    Classe                              Idade              \n");
    printf("-----------------------------------------------------------\n");
    printf(" Não-eleitor                    Abaixo de 16 anos          \n");
    printf(" Eleitor facutativo  Entre 16 e 18 anos e maior que 65 anos\n");
    printf(" Eleitor obrigatório            Entre 18 e 65 anos         \n");
    printf("-----------------------------------------------------------\n\n");

    printf("Digite a sua idade: ");
    scanf("%d", &idade);

    if (idade < 16)
    {
        printf("Não Eleitor\n");
    }
    else if (idade >= 16 && idade < 18 && idade > 65)
    {
        printf("Eleitor Facutativo\n");
    }
    else if (idade >= 18 && idade <= 65)
    {
        printf("Eleitor obrigatorio\n");
    }

    return 0;
}