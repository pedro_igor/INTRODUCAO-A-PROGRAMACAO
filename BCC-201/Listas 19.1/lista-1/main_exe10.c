#include <stdio.h>
#include <stdlib.h>
#include "exe10.h"

int main()
{
    double x, y;
    double sum, produto, quadrado, raiz1, sen;

    system("clear");

    printf("Digite dois numeros: ");
    scanf("%lf %lf", &x, &y);

    sum = soma(x, y);
    produto = pod(x, y);
    quadrado = quad(x);
    raiz1 = raiz(x, y);
    sen = seno(x, y);

    printf("x = %.2lf\n", x);
    printf("y = %.2lf\n", y);
    printf("A soma dos numeros = %.2lf\n", sum);
    printf("O produto do primeiro pelo quadrado do segundo = %.2lf\n", pod);
    printf("O quadrado do primero numero = %.2lf\n", quad);
    printf("A raiz quadrada da soma dos quadrados = %.2lf\n", raiz);
    printf("O seno da diferença do primeiro pelo segundo (em graus) = %.2lf\n\n", seno);

    return 0;
}