#include <stdio.h>

int main()
{
    double preco;
    double lucro;

    printf("Digite o preço do produto: ");
    scanf("%lf", &preco);

    if (preco < 0)
    {
        printf("Nao existe preço negativo\n");
    }
    else
    {
        if (preco < 20.00)
        {
            lucro = preco * 0.45;
        }
        else
        {
            lucro = preco * 0.30;
        }

        printf("O Lucro em cima do produto foi de: %.2lf\n", lucro);
    }

    return 0;
}