#ifndef EXE10_H
#define EXE10_H

#include <math.h>
#define PI 3.14159265359

double soma(double x, double y);
double pod(double x, double y);
double quad(double x);
double raiz(double x, double y);
double seno(double x, double y);

#endif