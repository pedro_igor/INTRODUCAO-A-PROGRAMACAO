#include <stdio.h>
#include <math.h>

int main()
{
    double nro1, nro2;
    double quad, raiz;

    printf("Digite dois numeros: ");
    scanf("%lf %lf", &nro1, &nro2);

    if (nro1 == nro2)
    {
        printf("OS NUMEROS NAO PODE SER IGUAIS\n");
    }
    else
    {
        if (nro1 > nro2)
        {
            quad = pow(nro2, 2);
            raiz = sqrt(nro1);
        }
        else
        {
            quad = pow(nro1, 2);
            raiz = sqrt(nro2);
        }
        
        printf("O Quadrado do menor numero é: %.2lf\n", quad);
        printf("A raiz do maior numero: %.2lf\n", raiz);
    }

    return 0;
}