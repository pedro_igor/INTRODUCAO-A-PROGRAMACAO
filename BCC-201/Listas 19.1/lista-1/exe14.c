#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int aposta, nroAleatorio;

    srand(time(NULL));

    system("clear");

    printf("Digite a sua aposta: ");
    scanf("%d", &aposta);

    if(aposta <= 0 || aposta >= 7)
    {
        printf("Numero invalido\n\n");
        exit(0);
    }

    nroAleatorio = (1 + rand() % 6);

    if (aposta == nroAleatorio)
    {
        printf("Aposta = %d\n", aposta);
        printf("Dado = %d\n\n", nroAleatorio);

        printf("Você acertou !!!!\n");
    }
    else
    {
        printf("Aposta = %d\n", aposta);
        printf("Dado = %d\n\n", nroAleatorio);

        printf("Voce Errou\n");
    }

    return 0;
}