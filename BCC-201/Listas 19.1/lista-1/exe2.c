#include <stdio.h>

int main()
{
    double a, b, c;
    double maior1, maior2, maior3;

    printf("DIGITE OS TRES NUMEROS: ");
    scanf("%lf %lf %lf", &a, &b, &c);

    if (a == b || a == c || b == c)
    {
        printf("ERRO: os números não podem ser iguais\n");
    }
    else
    {
        if (a > b && b > c)
        {
            maior1 = a;
            maior2 = b;
            maior3 = c;
        }
        else if (a > c && c > b)
        {
            maior1 = a;
            maior2 = c;
            maior3 = b;
        }
        else if (b > a && a > c)
        {
            maior1 = b;
            maior2 = a;
            maior3 = c;
        }
        else if (b > c && c > a)
        {
            maior1 = b;
            maior2 = c;
            maior3 = a;
        }
        else if (c > b && b > a)
        {
            maior1 = c;
            maior2 = b;
            maior3 = a;
        }
        else if (c > a && a > b)
        {
            maior1 = c;
            maior2 = a;
            maior3 = b;
        }

        printf("Maior 1: %.1lf\n", maior1);
        printf("Maior 2: %.1lf\n", maior2);
        printf("Maior 3: %.1lf\n\n", maior3);
    }

    return 0;
}