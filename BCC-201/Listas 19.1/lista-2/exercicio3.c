#include <stdio.h>

double conversao(double farenheint);

int main()
{
    double celsius;
    for(double i = 50; i <= 150; i++)
    {
        celsius = conversao(i);

        printf("%3.1lf f° equivale à %.1lf c°\n", i, celsius);
    }

    return 0;
}

double conversao(double farenheint)
{
    double celsius = (5.0/9.0) * (farenheint - 32);

    return celsius;
}