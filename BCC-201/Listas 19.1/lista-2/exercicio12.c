#include <stdio.h>
#include <math.h>

int main()
{
    int x = 8;
    int y = 9;
    double f;

    printf(" X  Y  f(x)\n");
    for (int i = 2; i <= x; i += 2)
    {
        for (int j = 1; j <= y; j += 2)
        {
            if (j == i + 1)
            {
                f = ((pow(x, 4)) + (3 * x * y) + (pow(x, 3))) /
                    ((2 * x * y) + (3 * x) + (4 * y) + (2));

                printf(" %d  %d  %.2lf", i, j, f);
            }
                }

        printf("\n");
    }

    printf("\n");

    return 0;
}