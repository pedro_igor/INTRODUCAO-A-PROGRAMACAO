#include <stdio.h>

int main()
{
    double salary;
    double soma_salary = 0;
    double media_salary;
    int nro_filhos;
    int soma_filhos = 0;
    double media_filhos;

    int cont_salary = 0;//para saber a media das pesssoas com salario ate 1080,00
    double maior_salary = 0.00;

    int pop = 0; // população

    do
    {
        printf("Pessoa %d qual é o seu salario: ", pop + 1);
        scanf("%lf", &salary);

        if(salary < 0)
        {
            break;
        }
        else
        {
            if(salary <= 1080.00)
            {
                cont_salary++;
            }

            if(salary > maior_salary)
            {
                maior_salary = salary;
            }
            soma_salary += salary;
        }

        printf("Qual é o numeros de filhos: ");
        scanf("%d", &nro_filhos);

        soma_filhos += nro_filhos;

        pop++;
    } while (salary > 0.00);

    printf("A media do salario da população é: ");
    media_salary = soma_salary / pop;
    printf("R$%.2lf\n", media_salary);

    media_filhos = soma_filhos / pop;
    printf("A media de fillhos da população é: %.2lf\n", media_filhos);

    printf("O maior salario é: R$ %.2lf\n", maior_salary);

    printf("O percentual de pessoas com salário até R$ 1080,00: %d%%\n\n", (cont_salary / pop) * 100);
    
    return 0;   
}