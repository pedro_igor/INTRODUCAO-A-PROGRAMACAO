#include <stdio.h>

int main()
{
    int funcionarios = 30;
    double salario;
    double new_salary;

    for(int i = 0; i < funcionarios; i++)
    {
        printf("Digite o salario do funcionario %d: ", i + 1);
        scanf("%lf", &salario);

        if(salario < 1000.00)
        {
            new_salary = salario + (salario * 0.15);
        }
        else if(salario >= 1000.00 && salario < 2000.00)
        {
            new_salary = salario + (salario * 0.10);
        }
        else if(salario >= 2000.00)
        {
            new_salary = salario + (salario * 0.05);
        }

        printf("O novo salario do funcionario %d é de: R$%.2lf\n", i+1, new_salary);
        printf("------------------------------------------------\n\n");
    }

    return 0;
}