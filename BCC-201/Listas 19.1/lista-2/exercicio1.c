#include <stdio.h>

int main()
{
    int idade;
    int nro_aluno = 50;
    int matricula;
    int cont_idade = 0;

    for(int i = 0; i < nro_aluno; i++)
    {
        printf("Digite a idade do aluno %d: ", i + 1);
        scanf("%d", &idade);

        printf("Matricula: ");
        scanf("%d", &matricula);

        if(idade > 20)
        {
            cont_idade++;
        }
    }

    printf("Há %d alunos com idade acima dos 20 anos.\n", cont_idade);

    return 0;
}