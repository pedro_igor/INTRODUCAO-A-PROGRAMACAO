#include <stdio.h>

#define TAM 10

int main()
{
    int vetA[TAM];
    int vetB[TAM];
    int vetC[TAM];

    for (int i = 0; i < TAM; i++)
    {
        printf("Digite o valor do vator A na posição [%d]: ", i);
        scanf("%d", &vetA[i]);
    }

    for (int i = 0; i < TAM; i++)
    {
        printf("Digite o valor do vator B na posição [%d]: ", i);
        scanf("%d", &vetB[i]);
    }

    for (int i = 0; i < TAM; i++)
    {
        vetC[i] = vetA[i] + vetB[i];
    }

    printf("VETOR C: ");
    for (int i = 0; i < TAM; i++)
    {
        printf("%d ", vetC[i]);
    }

    printf("\n");

    return 0;
}