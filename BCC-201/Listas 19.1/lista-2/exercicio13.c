#include <stdio.h>

#define TAM 100

int main()
{
    int vet[TAM];
    int i, j;

    vet[0] = 30;
    for(i = 1; i < TAM; i++)
    {
        vet[i] = i;
    }

    for(j = 0; j < TAM; j++)
    {
        printf("%d ", vet[j]);
    }

    printf("\n");

    return 0;
}