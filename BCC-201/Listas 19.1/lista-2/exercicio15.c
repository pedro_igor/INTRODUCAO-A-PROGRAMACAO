#include <stdio.h>

#define TAM 50

int main()
{
    int vet[TAM];
    int soma = 0;

    for (int i = 0; i < TAM; i++)
    {
        printf("Digite o valor do vetor na posição [%d]: ", i);
        scanf("%d", &vet[i]);

        soma += vet[i];
    }

    printf("A soma entre todos os elemntos do vetor = %d\n", soma);

    return 0;
}