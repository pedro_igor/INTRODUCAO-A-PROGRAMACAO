#include <stdio.h>
#include <limits.h>

#define TAM 100

int main()
{
    int vet[TAM];

    int maior = INT_MIN;
    int menor = INT_MAX;

    int nro;

    printf("Qual é tamanho do vetor: ");
    scanf("%d", &nro);

    for (int i = 0; i < nro; i++)
    {
        printf("Digite o valor do vetor na posição [%d]: ", i);
        scanf("%d", &vet[i]);

        if (vet[i] > maior)
        {
            maior = vet[i];
        }
        else if (vet[i] < menor)
        {
            menor = vet[i];
        }
    }

    for (int i = 0; i < nro; i++)
    {
        if (vet[i] > maior)
        {
            maior = vet[i];
        }
        else if (vet[i] < menor)
        {
            menor = vet[i];
        }
    }

    printf("\nO maior Numero contido neste vetor = %d", maior);
    printf("\nO menor numero contido neste vetor = %d", menor);
    printf("\n");

    return 0;
}