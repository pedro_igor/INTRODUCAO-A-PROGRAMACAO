#include <stdio.h>

int main()
{
    int nro;
    double soma = 0;

    printf("Digite o numeros de termos: ");
    scanf("%d", &nro);

    while (nro <= 0)
    {
        printf("Digite o numeros de termos: ");
        scanf("%d", &nro);
    }

    for(int i = 1; i <= nro; i++)
    {
        if(i % 2 == 0)
        {
            soma += (1.0/nro);
        }
        else
        {
            soma -= (1.0/nro);
        }
    }

    printf("H = %.2lf\n", soma);

    return 0;
}