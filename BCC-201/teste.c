#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

int main()
{
	printf("INT_MAX: %d\n", INT_MAX );
	printf("INT_MIN: %d\n", INT_MIN);
	//printf("unsigned int: %ld\n", UINT_MAX);
	//printf("ULLONG_MAX: %lld\n",ULLONG_MAX);
	puts("\n");

	printf("char: %ld\n", sizeof(char));
	printf("short: %ld\n", sizeof(short));
	printf("int: %ld\n", sizeof(int));
	printf("float: %ld\n", sizeof(float));
	printf("double: %ld\n", sizeof(long double));
	printf("long int: %ld\n", sizeof(unsigned int));
	return 0;
}