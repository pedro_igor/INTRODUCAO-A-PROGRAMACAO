#include <stdio.h>


#define TAM 50

int main()
{
    char matriz[TAM][TAM];

    for(int i = 0; i < TAM; i++)
    {
        for(int j = 0; j < TAM; j++)
        {
            matriz[i][j] = " ";
        }
    }

    for (int i = 0; i < TAM; i++)
    {
        for (int j = 0; j < TAM; j++)
        {
            if (i % 2 != 0)
            {
                printf("%s",matriz[i][j]);
            }
            else
            {
                printf("%s",matriz[i][j]);
            }
        }
        printf("\n");
    }

    return 0;
}