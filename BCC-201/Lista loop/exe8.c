#include <stdio.h>

int main()
{
    int cont_pos = 0, cont_neg = 0;
    double x;

    while (x != 0)
    {
        printf("DIGITE UM NÚMERO REAL (PARAR = 0): ");
        scanf("%lf", &x);

        if (x > 0)
            cont_pos++;
        else if (x < 0)
            cont_neg++;
    }

    printf("\n");
    printf("QUANTIDADE DE POSITIVOS DIGITADOS: %d\n", cont_pos);
    printf("QUANTIDADE DE NEGATIVOS DIGITADOS: %d\n", cont_neg);

    return 0;
}