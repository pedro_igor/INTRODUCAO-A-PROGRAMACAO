#include <stdio.h>
#include <math.h>

int main()
{
    int i;

    for (i = 1; i <= 20; i++)
        printf("%.0lf ", pow(i, 2));

    printf("\n");

    return 0;
}