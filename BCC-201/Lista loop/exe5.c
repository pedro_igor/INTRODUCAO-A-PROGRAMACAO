#include <stdio.h>

int main()
{
    int n;

    printf("Digite o limite: ");
    scanf("%d", &n);

    for(int i = 3; i <= n; i+=6)
    {
        if(i % 3 == 0)
            printf("%d ",  i);
        
    }

    printf("\n");

    return 0;
}