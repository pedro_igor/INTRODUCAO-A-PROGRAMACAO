#include <stdio.h>

int main()
{
    int idade;
    int soma = 0;
    double media = 0;

    int i = 1;
    int cont = 0;

    do
    {
        printf("Digite a idade da pessoa %d: ", i);
        scanf("%d ", &idade);

        if (idade > 0)
        {
            soma += idade;
            cont++;
        }

        i++;

    } while (idade > 0);

    media = soma / cont;

    printf("A media da %d = %.1lf\n", cont, media);

    return 0;
}