#include <stdio.h>

int main()
{
    int i = 100;

    for (i; i >= 1; i--)
        printf("%d ", i);

    printf("\n");

    return 0;
}